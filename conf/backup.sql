PGDMP     6    
                v           nielsenMusic    10.1    10.0 +              0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false                       0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false                       1262    66732    nielsenMusic    DATABASE     l   CREATE DATABASE "nielsenMusic" WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'C' LC_CTYPE = 'C';
    DROP DATABASE "nielsenMusic";
             postgres    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false            	           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    3                        3079    13241    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false            
           0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    1            �            1259    67627    address    TABLE     �  CREATE TABLE address (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    is_default boolean DEFAULT false,
    user_id integer,
    first_name character varying,
    last_name character varying,
    phone character varying,
    address character varying,
    address2 character varying,
    city character varying,
    state character varying,
    country character varying,
    zip character varying,
    label character varying
);
    DROP TABLE public.address;
       public         nielsend_db    false    3                       0    0    address    ACL     W   GRANT ALL ON TABLE address TO nielsendev;
GRANT ALL ON TABLE address TO nielsend_user;
            public       nielsend_db    false    196            �            1259    67636    address_id_seq    SEQUENCE     p   CREATE SEQUENCE address_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.address_id_seq;
       public       nielsend_db    false    3    196                       0    0    address_id_seq    SEQUENCE OWNED BY     3   ALTER SEQUENCE address_id_seq OWNED BY address.id;
            public       nielsend_db    false    197                       0    0    address_id_seq    ACL     �   GRANT ALL ON SEQUENCE address_id_seq TO PUBLIC;
GRANT ALL ON SEQUENCE address_id_seq TO postgres;
GRANT ALL ON SEQUENCE address_id_seq TO nielsendev;
GRANT ALL ON SEQUENCE address_id_seq TO nielsend_user;
            public       nielsend_db    false    197            �            1259    67638    admin_roles    TABLE     �   CREATE TABLE admin_roles (
    id integer NOT NULL,
    active smallint DEFAULT 1 NOT NULL,
    insert_time timestamp without time zone DEFAULT now() NOT NULL,
    admin_id integer NOT NULL,
    role_id integer NOT NULL
);
    DROP TABLE public.admin_roles;
       public         nielsend_db    false    3                       0    0    admin_roles    ACL     _   GRANT ALL ON TABLE admin_roles TO nielsendev;
GRANT ALL ON TABLE admin_roles TO nielsend_user;
            public       nielsend_db    false    198            �            1259    67643    admin_roles_id_seq    SEQUENCE     t   CREATE SEQUENCE admin_roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.admin_roles_id_seq;
       public       nielsend_db    false    3    198                       0    0    admin_roles_id_seq    SEQUENCE OWNED BY     ;   ALTER SEQUENCE admin_roles_id_seq OWNED BY admin_roles.id;
            public       nielsend_db    false    199                       0    0    admin_roles_id_seq    ACL     �   GRANT ALL ON SEQUENCE admin_roles_id_seq TO PUBLIC;
GRANT ALL ON SEQUENCE admin_roles_id_seq TO postgres;
GRANT ALL ON SEQUENCE admin_roles_id_seq TO nielsendev;
GRANT ALL ON SEQUENCE admin_roles_id_seq TO nielsend_user;
            public       nielsend_db    false    199            �            1259    67645    administrator    TABLE     �  CREATE TABLE administrator (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    first_name character varying,
    last_name character varying,
    email character varying,
    username character varying,
    password character varying,
    hash character varying,
    permissions character varying,
    signup_ip character varying,
    update_time character varying
);
 !   DROP TABLE public.administrator;
       public         nielsend_db    false    3                       0    0    administrator    ACL     c   GRANT ALL ON TABLE administrator TO nielsendev;
GRANT ALL ON TABLE administrator TO nielsend_user;
            public       nielsend_db    false    200            �            1259    67653    administrator_id_seq    SEQUENCE     v   CREATE SEQUENCE administrator_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.administrator_id_seq;
       public       nielsend_db    false    3    200                       0    0    administrator_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE administrator_id_seq OWNED BY administrator.id;
            public       nielsend_db    false    201                       0    0    administrator_id_seq    ACL     �   GRANT ALL ON SEQUENCE administrator_id_seq TO PUBLIC;
GRANT ALL ON SEQUENCE administrator_id_seq TO postgres;
GRANT ALL ON SEQUENCE administrator_id_seq TO nielsendev;
GRANT ALL ON SEQUENCE administrator_id_seq TO nielsend_user;
            public       nielsend_db    false    201            �            1259    67655    answer_id_seq    SEQUENCE     o   CREATE SEQUENCE answer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.answer_id_seq;
       public       nielsend_db    false    3                       0    0    answer_id_seq    ACL     i   GRANT ALL ON SEQUENCE answer_id_seq TO nielsendev;
GRANT ALL ON SEQUENCE answer_id_seq TO nielsend_user;
            public       nielsend_db    false    202            �            1259    67657    answer    TABLE     >  CREATE TABLE answer (
    id integer DEFAULT nextval('answer_id_seq'::regclass) NOT NULL,
    active smallint DEFAULT 1 NOT NULL,
    insert_time timestamp without time zone DEFAULT now() NOT NULL,
    text character varying,
    question_id integer,
    featured_image character varying,
    display_order integer
);
    DROP TABLE public.answer;
       public         nielsend_db    false    202    3                       0    0    answer    ACL     U   GRANT ALL ON TABLE answer TO nielsendev;
GRANT ALL ON TABLE answer TO nielsend_user;
            public       nielsend_db    false    203            �            1259    67666    banner    TABLE     �  CREATE TABLE banner (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    title character varying,
    image character varying,
    link character varying,
    date_modified character varying,
    is_deal boolean DEFAULT false,
    featured_id integer DEFAULT 0,
    url character varying,
    banner_order integer
);
    DROP TABLE public.banner;
       public         nielsend_db    false    3                       0    0    banner    ACL     U   GRANT ALL ON TABLE banner TO nielsendev;
GRANT ALL ON TABLE banner TO nielsend_user;
            public       nielsend_db    false    204            �            1259    67676    banner_id_seq    SEQUENCE     o   CREATE SEQUENCE banner_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.banner_id_seq;
       public       nielsend_db    false    204    3                       0    0    banner_id_seq    SEQUENCE OWNED BY     1   ALTER SEQUENCE banner_id_seq OWNED BY banner.id;
            public       nielsend_db    false    205                       0    0    banner_id_seq    ACL     �   GRANT ALL ON SEQUENCE banner_id_seq TO PUBLIC;
GRANT ALL ON SEQUENCE banner_id_seq TO postgres;
GRANT ALL ON SEQUENCE banner_id_seq TO nielsendev;
GRANT ALL ON SEQUENCE banner_id_seq TO nielsend_user;
            public       nielsend_db    false    205            �            1259    67678    category    TABLE     �  CREATE TABLE category (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    name character varying,
    subtitle character varying,
    parent_category integer,
    description character varying,
    slug character varying,
    meta_title character varying,
    meta_keywords character varying,
    meta_description character varying,
    display_order integer,
    in_menu integer,
    image character varying
);
    DROP TABLE public.category;
       public         nielsend_db    false    3                       0    0    category    ACL     Y   GRANT ALL ON TABLE category TO nielsendev;
GRANT ALL ON TABLE category TO nielsend_user;
            public       nielsend_db    false    206            �            1259    67686    category_id_seq    SEQUENCE     q   CREATE SEQUENCE category_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.category_id_seq;
       public       nielsend_db    false    3    206                       0    0    category_id_seq    SEQUENCE OWNED BY     5   ALTER SEQUENCE category_id_seq OWNED BY category.id;
            public       nielsend_db    false    207                       0    0    category_id_seq    ACL     �   GRANT ALL ON SEQUENCE category_id_seq TO PUBLIC;
GRANT ALL ON SEQUENCE category_id_seq TO postgres;
GRANT ALL ON SEQUENCE category_id_seq TO nielsendev;
GRANT ALL ON SEQUENCE category_id_seq TO nielsend_user;
            public       nielsend_db    false    207            �            1259    67688    config    TABLE     �   CREATE TABLE config (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    name character varying,
    value character varying
);
    DROP TABLE public.config;
       public         nielsend_db    false    3                       0    0    config    ACL     U   GRANT ALL ON TABLE config TO nielsendev;
GRANT ALL ON TABLE config TO nielsend_user;
            public       nielsend_db    false    208            �            1259    67696    config_id_seq    SEQUENCE     o   CREATE SEQUENCE config_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.config_id_seq;
       public       nielsend_db    false    3    208                       0    0    config_id_seq    SEQUENCE OWNED BY     1   ALTER SEQUENCE config_id_seq OWNED BY config.id;
            public       nielsend_db    false    209                       0    0    config_id_seq    ACL     �   GRANT ALL ON SEQUENCE config_id_seq TO PUBLIC;
GRANT ALL ON SEQUENCE config_id_seq TO postgres;
GRANT ALL ON SEQUENCE config_id_seq TO nielsendev;
GRANT ALL ON SEQUENCE config_id_seq TO nielsend_user;
            public       nielsend_db    false    209            �            1259    67698    contact    TABLE     �   CREATE TABLE contact (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    name character varying,
    email character varying,
    message text,
    company character varying
);
    DROP TABLE public.contact;
       public         nielsend_db    false    3                       0    0    contact    ACL     W   GRANT ALL ON TABLE contact TO nielsendev;
GRANT ALL ON TABLE contact TO nielsend_user;
            public       nielsend_db    false    210            �            1259    67706    contact_id_seq    SEQUENCE     p   CREATE SEQUENCE contact_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.contact_id_seq;
       public       nielsend_db    false    210    3                        0    0    contact_id_seq    SEQUENCE OWNED BY     3   ALTER SEQUENCE contact_id_seq OWNED BY contact.id;
            public       nielsend_db    false    211            !           0    0    contact_id_seq    ACL     k   GRANT ALL ON SEQUENCE contact_id_seq TO nielsendev;
GRANT ALL ON SEQUENCE contact_id_seq TO nielsend_user;
            public       nielsend_db    false    211            �            1259    67708    coupon    TABLE     �  CREATE TABLE coupon (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    name character varying,
    code character varying,
    discount_type character varying,
    discount_amount character varying,
    min_amount character varying,
    num_uses_all character varying,
    uses_per_user character varying,
    start_time character varying,
    end_time character varying
);
    DROP TABLE public.coupon;
       public         nielsend_db    false    3            "           0    0    coupon    ACL     U   GRANT ALL ON TABLE coupon TO nielsendev;
GRANT ALL ON TABLE coupon TO nielsend_user;
            public       nielsend_db    false    212            �            1259    67716    coupon_id_seq    SEQUENCE     o   CREATE SEQUENCE coupon_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.coupon_id_seq;
       public       nielsend_db    false    3    212            #           0    0    coupon_id_seq    SEQUENCE OWNED BY     1   ALTER SEQUENCE coupon_id_seq OWNED BY coupon.id;
            public       nielsend_db    false    213            $           0    0    coupon_id_seq    ACL     �   GRANT ALL ON SEQUENCE coupon_id_seq TO PUBLIC;
GRANT ALL ON SEQUENCE coupon_id_seq TO postgres;
GRANT ALL ON SEQUENCE coupon_id_seq TO nielsendev;
GRANT ALL ON SEQUENCE coupon_id_seq TO nielsend_user;
            public       nielsend_db    false    213            �            1259    67718    credit    TABLE       CREATE TABLE credit (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    name character varying,
    description character varying,
    price character varying,
    value character varying
);
    DROP TABLE public.credit;
       public         nielsend_db    false    3            %           0    0    credit    ACL     U   GRANT ALL ON TABLE credit TO nielsendev;
GRANT ALL ON TABLE credit TO nielsend_user;
            public       nielsend_db    false    214            �            1259    67726    credit_id_seq    SEQUENCE     o   CREATE SEQUENCE credit_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.credit_id_seq;
       public       nielsend_db    false    3    214            &           0    0    credit_id_seq    SEQUENCE OWNED BY     1   ALTER SEQUENCE credit_id_seq OWNED BY credit.id;
            public       nielsend_db    false    215            '           0    0    credit_id_seq    ACL     �   GRANT ALL ON SEQUENCE credit_id_seq TO PUBLIC;
GRANT ALL ON SEQUENCE credit_id_seq TO postgres;
GRANT ALL ON SEQUENCE credit_id_seq TO nielsendev;
GRANT ALL ON SEQUENCE credit_id_seq TO nielsend_user;
            public       nielsend_db    false    215            �            1259    67728    jewelry    TABLE     z  CREATE TABLE jewelry (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    name character varying,
    description character varying,
    price numeric(10,2),
    currency_code character varying,
    quantity integer,
    tags character varying,
    materials character varying,
    images text,
    variation text,
    meta_title character varying,
    meta_keywords character varying,
    meta_description character varying,
    metal_colors text DEFAULT '{"14kt_gold":{"White":0,"Yellow":0,"Rose":0},"18kt_gold":{"White":0,"Yellow":0,"Rose":0},"Platinum":{"White":0,"Yellow":0,"Rose":0}}'::text,
    metals character varying DEFAULT '14kt_gold, 18kt_gold, Platinum'::character varying,
    total_14kt_price numeric(10,2) DEFAULT 0,
    total_18kt_price numeric(10,2) DEFAULT 0,
    total_plat_price numeric(10,2) DEFAULT 0,
    slug character varying,
    semi_mount numeric(5,2),
    stone_breakdown character varying,
    stone_count integer,
    weight numeric(10,2),
    diamond_cwwt character varying,
    diamond_color character varying,
    diamond_quality character varying,
    _14kt numeric(10,2),
    _18kt numeric(10,2),
    platinum numeric(10,2),
    setting integer,
    polish integer,
    melee numeric(10,2),
    ship_from_cast numeric(10,2),
    ship_to_cust numeric(10,2),
    memo_duration integer DEFAULT 0
);
    DROP TABLE public.jewelry;
       public         nielsend_db    false    3            (           0    0    jewelry    ACL     �   GRANT ALL ON TABLE jewelry TO postgres;
GRANT ALL ON TABLE jewelry TO PUBLIC;
GRANT ALL ON TABLE jewelry TO nielsendev;
GRANT ALL ON TABLE jewelry TO nielsend_user;
            public       nielsend_db    false    216            �            1259    67742    jewelry_id_seq    SEQUENCE     p   CREATE SEQUENCE jewelry_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.jewelry_id_seq;
       public       nielsend_db    false    216    3            )           0    0    jewelry_id_seq    SEQUENCE OWNED BY     3   ALTER SEQUENCE jewelry_id_seq OWNED BY jewelry.id;
            public       nielsend_db    false    217            *           0    0    jewelry_id_seq    ACL     �   GRANT ALL ON SEQUENCE jewelry_id_seq TO PUBLIC;
GRANT ALL ON SEQUENCE jewelry_id_seq TO postgres;
GRANT ALL ON SEQUENCE jewelry_id_seq TO nielsendev;
GRANT ALL ON SEQUENCE jewelry_id_seq TO nielsend_user;
            public       nielsend_db    false    217            �            1259    67744    order    TABLE     �  CREATE TABLE "order" (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    bill_first_name character varying,
    bill_last_name character varying,
    bill_address character varying,
    bill_address2 character varying,
    bill_city character varying,
    bill_state character varying,
    bill_country character varying,
    bill_zip character varying,
    ship_first_name character varying,
    ship_last_name character varying,
    ship_address character varying,
    ship_address2 character varying,
    ship_city character varying,
    ship_state character varying,
    ship_country character varying,
    ship_zip character varying,
    cc_number character varying,
    cc_expiration_month character varying,
    cc_expiration_year character varying,
    cc_ccv character varying,
    tax character varying,
    tax_rate character varying,
    status character varying,
    tracking_number character varying,
    shipping_method character varying,
    user_id integer,
    coupon_code character varying,
    gift_card character varying,
    email character varying,
    shipping_cost real,
    payment_method integer,
    subtotal real,
    total real,
    viewed boolean,
    phone character varying,
    coupon_type integer,
    coupon_amount numeric,
    note character varying,
    user_role character varying,
    ref_num character varying,
    order_type integer,
    signature character varying
);
    DROP TABLE public."order";
       public         nielsend_db    false    3            +           0    0    order    ACL     W   GRANT ALL ON TABLE "order" TO nielsendev;
GRANT ALL ON TABLE "order" TO nielsend_user;
            public       nielsend_db    false    218            �            1259    67752    order_id_seq    SEQUENCE     n   CREATE SEQUENCE order_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.order_id_seq;
       public       nielsend_db    false    3    218            ,           0    0    order_id_seq    SEQUENCE OWNED BY     1   ALTER SEQUENCE order_id_seq OWNED BY "order".id;
            public       nielsend_db    false    219            -           0    0    order_id_seq    ACL     g   GRANT ALL ON SEQUENCE order_id_seq TO nielsendev;
GRANT ALL ON SEQUENCE order_id_seq TO nielsend_user;
            public       nielsend_db    false    219            �            1259    67754    order_product_option    TABLE     �   CREATE TABLE order_product_option (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    order_product_id integer,
    data text,
    price numeric(10,2) DEFAULT 0
);
 (   DROP TABLE public.order_product_option;
       public         nielsend_db    false    3            .           0    0    order_product_option    ACL     �   GRANT ALL ON TABLE order_product_option TO postgres;
GRANT ALL ON TABLE order_product_option TO PUBLIC;
GRANT ALL ON TABLE order_product_option TO nielsendev;
GRANT ALL ON TABLE order_product_option TO nielsend_user;
            public       nielsend_db    false    220            �            1259    67763    order_product_option_id_seq    SEQUENCE     }   CREATE SEQUENCE order_product_option_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 2   DROP SEQUENCE public.order_product_option_id_seq;
       public       nielsend_db    false    3    220            /           0    0    order_product_option_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE order_product_option_id_seq OWNED BY order_product_option.id;
            public       nielsend_db    false    221            0           0    0    order_product_option_id_seq    ACL       GRANT ALL ON SEQUENCE order_product_option_id_seq TO PUBLIC;
GRANT ALL ON SEQUENCE order_product_option_id_seq TO postgres;
GRANT ALL ON SEQUENCE order_product_option_id_seq TO nielsendev;
GRANT ALL ON SEQUENCE order_product_option_id_seq TO nielsend_user;
            public       nielsend_db    false    221            �            1259    67765    order_products    TABLE     �   CREATE TABLE order_products (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    order_id integer,
    quantity integer,
    unit_price real,
    product_map_id integer NOT NULL
);
 "   DROP TABLE public.order_products;
       public         nielsend_db    false    3            1           0    0    order_products    ACL     e   GRANT ALL ON TABLE order_products TO nielsendev;
GRANT ALL ON TABLE order_products TO nielsend_user;
            public       nielsend_db    false    222            �            1259    67770    order_products_id_seq    SEQUENCE     w   CREATE SEQUENCE order_products_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.order_products_id_seq;
       public       nielsend_db    false    222    3            2           0    0    order_products_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE order_products_id_seq OWNED BY order_products.id;
            public       nielsend_db    false    223            3           0    0    order_products_id_seq    ACL     y   GRANT ALL ON SEQUENCE order_products_id_seq TO nielsendev;
GRANT ALL ON SEQUENCE order_products_id_seq TO nielsend_user;
            public       nielsend_db    false    223            �            1259    67772    page    TABLE     �  CREATE TABLE page (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    title character varying,
    slug character varying,
    description character varying,
    featured_image character varying,
    meta_title character varying,
    meta_keywords character varying,
    meta_description character varying,
    date_modified character varying,
    content text
);
    DROP TABLE public.page;
       public         nielsend_db    false    3            4           0    0    page    ACL     Q   GRANT ALL ON TABLE page TO nielsendev;
GRANT ALL ON TABLE page TO nielsend_user;
            public       nielsend_db    false    224            �            1259    67780    page_id_seq    SEQUENCE     m   CREATE SEQUENCE page_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 "   DROP SEQUENCE public.page_id_seq;
       public       nielsend_db    false    3    224            5           0    0    page_id_seq    SEQUENCE OWNED BY     -   ALTER SEQUENCE page_id_seq OWNED BY page.id;
            public       nielsend_db    false    225            6           0    0    page_id_seq    ACL     �   GRANT ALL ON SEQUENCE page_id_seq TO PUBLIC;
GRANT ALL ON SEQUENCE page_id_seq TO postgres;
GRANT ALL ON SEQUENCE page_id_seq TO nielsendev;
GRANT ALL ON SEQUENCE page_id_seq TO nielsend_user;
            public       nielsend_db    false    225            �            1259    67782    payment_profiles    TABLE     m  CREATE TABLE payment_profiles (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    user_id integer,
    first_name character varying,
    last_name character varying,
    phone character varying,
    address character varying,
    address2 character varying,
    city character varying,
    state character varying,
    country character varying,
    zip character varying,
    cc_number character varying,
    cc_expiration_month character varying,
    cc_expiration_year character varying,
    cc_ccv character varying,
    label character varying
);
 $   DROP TABLE public.payment_profiles;
       public         nielsend_db    false    3            7           0    0    payment_profiles    ACL     i   GRANT ALL ON TABLE payment_profiles TO nielsendev;
GRANT ALL ON TABLE payment_profiles TO nielsend_user;
            public       nielsend_db    false    226            �            1259    67790    payment_profiles_id_seq    SEQUENCE     y   CREATE SEQUENCE payment_profiles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.payment_profiles_id_seq;
       public       nielsend_db    false    3    226            8           0    0    payment_profiles_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE payment_profiles_id_seq OWNED BY payment_profiles.id;
            public       nielsend_db    false    227            9           0    0    payment_profiles_id_seq    ACL     �   GRANT ALL ON SEQUENCE payment_profiles_id_seq TO PUBLIC;
GRANT ALL ON SEQUENCE payment_profiles_id_seq TO postgres;
GRANT ALL ON SEQUENCE payment_profiles_id_seq TO nielsendev;
GRANT ALL ON SEQUENCE payment_profiles_id_seq TO nielsend_user;
            public       nielsend_db    false    227            �            1259    67792    product    TABLE     g  CREATE TABLE product (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    name character varying,
    sku character varying,
    price character varying,
    meta_title character varying,
    meta_keywords character varying,
    meta_description character varying,
    featured_image character varying,
    colors character varying,
    featured smallint,
    shape character varying,
    clarity character varying,
    weight character varying,
    lab character varying,
    cut_grade character varying,
    polish character varying,
    symmetry character varying,
    fluor character varying,
    rapaport_price character varying,
    total character varying,
    certificate character varying,
    length character varying,
    width character varying,
    depth character varying,
    depth_percent character varying,
    table_percent character varying,
    girdle character varying,
    culet character varying,
    description character varying,
    origin character varying,
    memo_status character varying,
    inscription character varying,
    certificate_file character varying,
    slug character varying,
    wholesale_price numeric(10,2) DEFAULT 0,
    video_link character varying,
    quantity integer,
    memo_duration integer DEFAULT 0,
    vendor character varying,
    discount integer
);
    DROP TABLE public.product;
       public         nielsend_db    false    3            :           0    0    product    ACL     �   GRANT ALL ON TABLE product TO postgres WITH GRANT OPTION;
GRANT ALL ON TABLE product TO nielsendev;
GRANT ALL ON TABLE product TO nielsend_user;
            public       nielsend_db    false    228            �            1259    67802    product_categories    TABLE     �   CREATE TABLE product_categories (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    category_id integer,
    product_map_id integer NOT NULL
);
 &   DROP TABLE public.product_categories;
       public         nielsend_db    false    3            ;           0    0    product_categories    ACL     m   GRANT ALL ON TABLE product_categories TO nielsendev;
GRANT ALL ON TABLE product_categories TO nielsend_user;
            public       nielsend_db    false    229            �            1259    67807    product_categories_id_seq    SEQUENCE     {   CREATE SEQUENCE product_categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE public.product_categories_id_seq;
       public       nielsend_db    false    3    229            <           0    0    product_categories_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE product_categories_id_seq OWNED BY product_categories.id;
            public       nielsend_db    false    230            =           0    0    product_categories_id_seq    ACL     �   GRANT ALL ON SEQUENCE product_categories_id_seq TO nielsendev;
GRANT ALL ON SEQUENCE product_categories_id_seq TO nielsend_user;
            public       nielsend_db    false    230            �            1259    67809    product_id_seq    SEQUENCE     p   CREATE SEQUENCE product_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.product_id_seq;
       public       nielsend_db    false    3    228            >           0    0    product_id_seq    SEQUENCE OWNED BY     3   ALTER SEQUENCE product_id_seq OWNED BY product.id;
            public       nielsend_db    false    231            ?           0    0    product_id_seq    ACL     �   GRANT ALL ON SEQUENCE product_id_seq TO PUBLIC;
GRANT ALL ON SEQUENCE product_id_seq TO postgres;
GRANT ALL ON SEQUENCE product_id_seq TO nielsendev;
GRANT ALL ON SEQUENCE product_id_seq TO nielsend_user;
            public       nielsend_db    false    231            �            1259    67811    product_images    TABLE     �   CREATE TABLE product_images (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    image character varying,
    display_order integer,
    product_map_id integer NOT NULL
);
 "   DROP TABLE public.product_images;
       public         nielsend_db    false    3            @           0    0    product_images    ACL     e   GRANT ALL ON TABLE product_images TO nielsendev;
GRANT ALL ON TABLE product_images TO nielsend_user;
            public       nielsend_db    false    232            �            1259    67819    product_images_id_seq    SEQUENCE     w   CREATE SEQUENCE product_images_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.product_images_id_seq;
       public       nielsend_db    false    232    3            A           0    0    product_images_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE product_images_id_seq OWNED BY product_images.id;
            public       nielsend_db    false    233            B           0    0    product_images_id_seq    ACL     y   GRANT ALL ON SEQUENCE product_images_id_seq TO nielsendev;
GRANT ALL ON SEQUENCE product_images_id_seq TO nielsend_user;
            public       nielsend_db    false    233            �            1259    67821    product_map    TABLE     �   CREATE TABLE product_map (
    id integer NOT NULL,
    active integer DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    product_id integer,
    product_type_id integer
);
    DROP TABLE public.product_map;
       public         nielsend_db    false    3            C           0    0    product_map    ACL     _   GRANT ALL ON TABLE product_map TO nielsendev;
GRANT ALL ON TABLE product_map TO nielsend_user;
            public       nielsend_db    false    234            �            1259    67826    product_map_id_seq    SEQUENCE     t   CREATE SEQUENCE product_map_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.product_map_id_seq;
       public       nielsend_db    false    234    3            D           0    0    product_map_id_seq    SEQUENCE OWNED BY     ;   ALTER SEQUENCE product_map_id_seq OWNED BY product_map.id;
            public       nielsend_db    false    235            E           0    0    product_map_id_seq    ACL     �   GRANT ALL ON SEQUENCE product_map_id_seq TO PUBLIC;
GRANT ALL ON SEQUENCE product_map_id_seq TO postgres;
GRANT ALL ON SEQUENCE product_map_id_seq TO nielsendev;
GRANT ALL ON SEQUENCE product_map_id_seq TO nielsend_user;
            public       nielsend_db    false    235            �            1259    67828    question_id_seq    SEQUENCE     q   CREATE SEQUENCE question_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.question_id_seq;
       public       nielsend_db    false    3            F           0    0    question_id_seq    ACL     m   GRANT ALL ON SEQUENCE question_id_seq TO nielsendev;
GRANT ALL ON SEQUENCE question_id_seq TO nielsend_user;
            public       nielsend_db    false    236            �            1259    67830    question    TABLE     c  CREATE TABLE question (
    id integer DEFAULT nextval('question_id_seq'::regclass) NOT NULL,
    active smallint DEFAULT 1 NOT NULL,
    insert_time timestamp without time zone DEFAULT now() NOT NULL,
    text character varying,
    failure_text character varying,
    image character varying,
    display_order integer,
    correct_answer_id integer
);
    DROP TABLE public.question;
       public         nielsend_db    false    236    3            G           0    0    question    ACL     Y   GRANT ALL ON TABLE question TO nielsendev;
GRANT ALL ON TABLE question TO nielsend_user;
            public       nielsend_db    false    237            �            1259    67839    ring    TABLE     
  CREATE TABLE ring (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    name character varying,
    price numeric(10,2),
    default_metal character varying,
    default_color character varying,
    dwt character varying,
    semi_mount_weight character varying,
    stone_breakdown character varying,
    center_stone_weight character varying,
    center_stone_shape character varying,
    total_stones character varying,
    semi_mount_diamond_weight character varying,
    diamond_color character varying,
    diamond_quality character varying,
    cost_per_dwt character varying,
    fourteenkt_gold_price character varying,
    eighteenkt_gold_price character varying,
    platinum_price character varying,
    website_fourteenkt_gold_price character varying,
    website_eighteenkt_gold_price character varying,
    website_platinum_price character varying,
    slug character varying,
    min_carat numeric(5,2) DEFAULT 0.0,
    max_carat numeric(5,2) DEFAULT 100,
    shape character varying,
    video_link character varying,
    metals character varying DEFAULT '14kt_gold, 18kt_gold, Platinum'::character varying,
    colors character varying DEFAULT 'White,Yellow,Rose'::character varying,
    metal_colors text DEFAULT '{"14kt_gold":{"White":1,"Yellow":1,"Rose":1},"18kt_gold":{"White":1,"Yellow":1,"Rose":0},"Platinum":{"White":1,"Yellow":0,"Rose":0}}'::text,
    setting_labor numeric(5,2),
    center_stone_setting_labor numeric(5,2),
    polish_labor numeric(5,2),
    head_to_add numeric(5,2),
    melee_cost numeric(5,2),
    shipping_from_cast numeric(5,2),
    shipping_to_customer numeric(5,2),
    total_14kt_cost numeric(10,2),
    total_18kt_cost numeric(10,2),
    total_plat_cost numeric(10,2),
    setting_style character varying,
    options text,
    alias character varying,
    meta_title character varying,
    meta_keywords character varying,
    meta_description character varying,
    description character varying,
    memo_duration integer DEFAULT 0
);
    DROP TABLE public.ring;
       public         nielsend_db    false    3            H           0    0    ring    ACL     Q   GRANT ALL ON TABLE ring TO nielsendev;
GRANT ALL ON TABLE ring TO nielsend_user;
            public       nielsend_db    false    238            �            1259    67853    ring_id_seq    SEQUENCE     m   CREATE SEQUENCE ring_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 "   DROP SEQUENCE public.ring_id_seq;
       public       nielsend_db    false    238    3            I           0    0    ring_id_seq    SEQUENCE OWNED BY     -   ALTER SEQUENCE ring_id_seq OWNED BY ring.id;
            public       nielsend_db    false    239            J           0    0    ring_id_seq    ACL     �   GRANT ALL ON SEQUENCE ring_id_seq TO PUBLIC;
GRANT ALL ON SEQUENCE ring_id_seq TO postgres;
GRANT ALL ON SEQUENCE ring_id_seq TO nielsendev;
GRANT ALL ON SEQUENCE ring_id_seq TO nielsend_user;
            public       nielsend_db    false    239            �            1259    67855    ring_material    TABLE     {  CREATE TABLE ring_material (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    name character varying,
    price numeric(10,2) DEFAULT 0,
    min_size numeric(10,2) DEFAULT 4.00,
    max_size numeric(10,2) DEFAULT 9.00,
    size_step numeric(10,2) DEFAULT 0.25,
    size_step_price numeric(10,2) DEFAULT 10
);
 !   DROP TABLE public.ring_material;
       public         nielsend_db    false    3            K           0    0    ring_material    ACL     c   GRANT ALL ON TABLE ring_material TO nielsendev;
GRANT ALL ON TABLE ring_material TO nielsend_user;
            public       nielsend_db    false    240            �            1259    67868    ring_material_id_seq    SEQUENCE     v   CREATE SEQUENCE ring_material_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.ring_material_id_seq;
       public       nielsend_db    false    240    3            L           0    0    ring_material_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE ring_material_id_seq OWNED BY ring_material.id;
            public       nielsend_db    false    241            M           0    0    ring_material_id_seq    ACL     w   GRANT ALL ON SEQUENCE ring_material_id_seq TO nielsendev;
GRANT ALL ON SEQUENCE ring_material_id_seq TO nielsend_user;
            public       nielsend_db    false    241            �            1259    67870    role    TABLE     �   CREATE TABLE role (
    id integer NOT NULL,
    active smallint DEFAULT 1 NOT NULL,
    insert_time timestamp without time zone DEFAULT now() NOT NULL,
    name character varying NOT NULL
);
    DROP TABLE public.role;
       public         nielsend_db    false    3            N           0    0    role    ACL     Q   GRANT ALL ON TABLE role TO nielsendev;
GRANT ALL ON TABLE role TO nielsend_user;
            public       nielsend_db    false    242            �            1259    67878    role_id_seq    SEQUENCE     m   CREATE SEQUENCE role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 "   DROP SEQUENCE public.role_id_seq;
       public       nielsend_db    false    3    242            O           0    0    role_id_seq    SEQUENCE OWNED BY     -   ALTER SEQUENCE role_id_seq OWNED BY role.id;
            public       nielsend_db    false    243            P           0    0    role_id_seq    ACL     e   GRANT ALL ON SEQUENCE role_id_seq TO nielsendev;
GRANT ALL ON SEQUENCE role_id_seq TO nielsend_user;
            public       nielsend_db    false    243            �            1259    67880    shipping_method    TABLE       CREATE TABLE shipping_method (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    is_default boolean DEFAULT false,
    name character varying,
    cart_subtotal_range_min numeric(10,0),
    cost numeric(10,0)
);
 #   DROP TABLE public.shipping_method;
       public         nielsend_db    false    3            Q           0    0    shipping_method    ACL     g   GRANT ALL ON TABLE shipping_method TO nielsendev;
GRANT ALL ON TABLE shipping_method TO nielsend_user;
            public       nielsend_db    false    244            �            1259    67889    shipping_method_id_seq    SEQUENCE     x   CREATE SEQUENCE shipping_method_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.shipping_method_id_seq;
       public       nielsend_db    false    244    3            R           0    0    shipping_method_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE shipping_method_id_seq OWNED BY shipping_method.id;
            public       nielsend_db    false    245            S           0    0    shipping_method_id_seq    ACL     �   GRANT ALL ON SEQUENCE shipping_method_id_seq TO PUBLIC;
GRANT ALL ON SEQUENCE shipping_method_id_seq TO postgres;
GRANT ALL ON SEQUENCE shipping_method_id_seq TO nielsendev;
GRANT ALL ON SEQUENCE shipping_method_id_seq TO nielsend_user;
            public       nielsend_db    false    245            �            1259    67891    transaction    TABLE     �   CREATE TABLE transaction (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    order_id integer,
    authorize_net_data text,
    ref_trans_id character varying
);
    DROP TABLE public.transaction;
       public         nielsend_db    false    3            T           0    0    transaction    ACL     _   GRANT ALL ON TABLE transaction TO nielsendev;
GRANT ALL ON TABLE transaction TO nielsend_user;
            public       nielsend_db    false    246            �            1259    67899    transaction_id_seq    SEQUENCE     t   CREATE SEQUENCE transaction_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.transaction_id_seq;
       public       nielsend_db    false    3    246            U           0    0    transaction_id_seq    SEQUENCE OWNED BY     ;   ALTER SEQUENCE transaction_id_seq OWNED BY transaction.id;
            public       nielsend_db    false    247            V           0    0    transaction_id_seq    ACL     �   GRANT ALL ON SEQUENCE transaction_id_seq TO PUBLIC;
GRANT ALL ON SEQUENCE transaction_id_seq TO postgres;
GRANT ALL ON SEQUENCE transaction_id_seq TO nielsendev;
GRANT ALL ON SEQUENCE transaction_id_seq TO nielsend_user;
            public       nielsend_db    false    247            �            1259    67901    user    TABLE     �  CREATE TABLE "user" (
    id integer NOT NULL,
    active smallint DEFAULT 1 NOT NULL,
    insert_time timestamp without time zone DEFAULT now() NOT NULL,
    email character varying NOT NULL,
    password character varying,
    hash character varying,
    first_name character varying,
    last_name character varying,
    phone character varying,
    photo character varying,
    username character varying
);
    DROP TABLE public."user";
       public         nielsend_db    false    3            W           0    0    user    ACL     U   GRANT ALL ON TABLE "user" TO nielsendev;
GRANT ALL ON TABLE "user" TO nielsend_user;
            public       nielsend_db    false    248            �            1259    67909    user_favorite    TABLE     �   CREATE TABLE user_favorite (
    id integer NOT NULL,
    active smallint DEFAULT 1 NOT NULL,
    insert_time timestamp without time zone DEFAULT now() NOT NULL,
    user_id integer,
    provider_id integer
);
 !   DROP TABLE public.user_favorite;
       public         nielsend_db    false    3            X           0    0    user_favorite    ACL     c   GRANT ALL ON TABLE user_favorite TO nielsendev;
GRANT ALL ON TABLE user_favorite TO nielsend_user;
            public       nielsend_db    false    249            �            1259    67914    user_favorite_id_seq    SEQUENCE     v   CREATE SEQUENCE user_favorite_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.user_favorite_id_seq;
       public       nielsend_db    false    249    3            Y           0    0    user_favorite_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE user_favorite_id_seq OWNED BY user_favorite.id;
            public       nielsend_db    false    250            Z           0    0    user_favorite_id_seq    ACL     w   GRANT ALL ON SEQUENCE user_favorite_id_seq TO nielsendev;
GRANT ALL ON SEQUENCE user_favorite_id_seq TO nielsend_user;
            public       nielsend_db    false    250            �            1259    67916    user_id_seq    SEQUENCE     m   CREATE SEQUENCE user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 "   DROP SEQUENCE public.user_id_seq;
       public       nielsend_db    false    3    248            [           0    0    user_id_seq    SEQUENCE OWNED BY     /   ALTER SEQUENCE user_id_seq OWNED BY "user".id;
            public       nielsend_db    false    251            \           0    0    user_id_seq    ACL     e   GRANT ALL ON SEQUENCE user_id_seq TO nielsendev;
GRANT ALL ON SEQUENCE user_id_seq TO nielsend_user;
            public       nielsend_db    false    251            �            1259    67918 
   user_roles    TABLE     �   CREATE TABLE user_roles (
    id integer NOT NULL,
    active smallint DEFAULT 1 NOT NULL,
    insert_time timestamp without time zone DEFAULT now() NOT NULL,
    user_id integer NOT NULL,
    role_id integer NOT NULL
);
    DROP TABLE public.user_roles;
       public         nielsend_db    false    3            ]           0    0 
   user_roles    ACL     ]   GRANT ALL ON TABLE user_roles TO nielsendev;
GRANT ALL ON TABLE user_roles TO nielsend_user;
            public       nielsend_db    false    252            ^           0    0    user_roles.id    ACL     -   GRANT ALL(id) ON TABLE user_roles TO PUBLIC;
            public       nielsend_db    false    252            �            1259    67923    user_roles_id_seq    SEQUENCE     s   CREATE SEQUENCE user_roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.user_roles_id_seq;
       public       nielsend_db    false    252    3            _           0    0    user_roles_id_seq    SEQUENCE OWNED BY     9   ALTER SEQUENCE user_roles_id_seq OWNED BY user_roles.id;
            public       nielsend_db    false    253            `           0    0    user_roles_id_seq    ACL     q   GRANT ALL ON SEQUENCE user_roles_id_seq TO nielsendev;
GRANT ALL ON SEQUENCE user_roles_id_seq TO nielsend_user;
            public       nielsend_db    false    253            �            1259    67925    wedding_band    TABLE     �  CREATE TABLE wedding_band (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    name character varying,
    slug character varying,
    price numeric(10,2) DEFAULT 0,
    category character varying,
    diamond_color character varying,
    diamond_quality character varying,
    video_link character varying,
    metals character varying DEFAULT '14kt_gold, 18kt_gold, Platinum'::character varying,
    colors character varying DEFAULT 'White,Yellow,Rose'::character varying,
    metal_colors text DEFAULT '{"14kt_gold":{"White":1,"Yellow":1,"Rose":1},"18kt_gold":{"White":1,"Yellow":1,"Rose":1},"Platinum":{"White":1,"Yellow":0,"Rose":0}}'::text,
    setting_labor numeric(5,2),
    polish_labor numeric(5,2),
    head_to_add numeric(5,2),
    melee_cost numeric(5,2),
    shipping_from_cast numeric(5,2),
    shipping_to_customer numeric(5,2),
    total_14kt_cost numeric(10,2),
    total_18kt_cost numeric(10,2),
    total_plat_cost numeric(10,2),
    options text,
    setting_style character varying,
    alias character varying,
    meta_title character varying,
    meta_keywords character varying,
    meta_description character varying,
    memo_duration integer DEFAULT 0
);
     DROP TABLE public.wedding_band;
       public         nielsend_db    false    3            a           0    0    wedding_band    ACL     a   GRANT ALL ON TABLE wedding_band TO nielsendev;
GRANT ALL ON TABLE wedding_band TO nielsend_user;
            public       nielsend_db    false    254            �            1259    67938    wedding_band_id_seq    SEQUENCE     u   CREATE SEQUENCE wedding_band_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.wedding_band_id_seq;
       public       nielsend_db    false    3    254            b           0    0    wedding_band_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE wedding_band_id_seq OWNED BY wedding_band.id;
            public       nielsend_db    false    255            c           0    0    wedding_band_id_seq    ACL     �   GRANT ALL ON SEQUENCE wedding_band_id_seq TO PUBLIC;
GRANT ALL ON SEQUENCE wedding_band_id_seq TO postgres;
GRANT ALL ON SEQUENCE wedding_band_id_seq TO nielsendev;
GRANT ALL ON SEQUENCE wedding_band_id_seq TO nielsend_user;
            public       nielsend_db    false    255                        1259    67940 	   wholesale    TABLE     m  CREATE TABLE wholesale (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    company character varying,
    job_title character varying,
    dba character varying,
    incorporation character varying,
    referrer character varying,
    phone character varying,
    alt_phone character varying,
    status integer DEFAULT 0,
    address character varying,
    address2 character varying,
    payment character varying,
    memo_duration integer DEFAULT 24,
    site character varying,
    logo character varying,
    discount text,
    tax_id integer
);
    DROP TABLE public.wholesale;
       public         nielsend_db    false    3            d           0    0 	   wholesale    ACL     [   GRANT ALL ON TABLE wholesale TO nielsendev;
GRANT ALL ON TABLE wholesale TO nielsend_user;
            public       nielsend_db    false    256                       1259    67950    wholesale_id_seq    SEQUENCE     r   CREATE SEQUENCE wholesale_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.wholesale_id_seq;
       public       nielsend_db    false    3    256            e           0    0    wholesale_id_seq    SEQUENCE OWNED BY     7   ALTER SEQUENCE wholesale_id_seq OWNED BY wholesale.id;
            public       nielsend_db    false    257            f           0    0    wholesale_id_seq    ACL     o   GRANT ALL ON SEQUENCE wholesale_id_seq TO nielsendev;
GRANT ALL ON SEQUENCE wholesale_id_seq TO nielsend_user;
            public       nielsend_db    false    257                       1259    67952    wholesale_items_id_seq    SEQUENCE     x   CREATE SEQUENCE wholesale_items_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.wholesale_items_id_seq;
       public       nielsend_db    false    3            g           0    0    wholesale_items_id_seq    ACL     {   GRANT ALL ON SEQUENCE wholesale_items_id_seq TO nielsendev;
GRANT ALL ON SEQUENCE wholesale_items_id_seq TO nielsend_user;
            public       nielsend_db    false    258                       1259    67954    wholesale_items    TABLE       CREATE TABLE wholesale_items (
    id integer DEFAULT nextval('wholesale_items_id_seq'::regclass) NOT NULL,
    active integer DEFAULT 1,
    insert_time timestamp without time zone,
    price character varying,
    wholesale_id integer,
    product_id integer
);
 #   DROP TABLE public.wholesale_items;
       public         nielsend_db    false    258    3            h           0    0    wholesale_items    ACL     g   GRANT ALL ON TABLE wholesale_items TO nielsendev;
GRANT ALL ON TABLE wholesale_items TO nielsend_user;
            public       nielsend_db    false    259                       1259    67962    wishlist    TABLE     �   CREATE TABLE wishlist (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    user_id integer,
    data text
);
    DROP TABLE public.wishlist;
       public         nielsend_db    false    3            i           0    0    wishlist    ACL     Y   GRANT ALL ON TABLE wishlist TO nielsendev;
GRANT ALL ON TABLE wishlist TO nielsend_user;
            public       nielsend_db    false    260                       1259    67970    wishlist_id_seq    SEQUENCE     q   CREATE SEQUENCE wishlist_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.wishlist_id_seq;
       public       nielsend_db    false    260    3            j           0    0    wishlist_id_seq    SEQUENCE OWNED BY     5   ALTER SEQUENCE wishlist_id_seq OWNED BY wishlist.id;
            public       nielsend_db    false    261            k           0    0    wishlist_id_seq    ACL     �   GRANT ALL ON SEQUENCE wishlist_id_seq TO PUBLIC;
GRANT ALL ON SEQUENCE wishlist_id_seq TO postgres;
GRANT ALL ON SEQUENCE wishlist_id_seq TO nielsendev;
GRANT ALL ON SEQUENCE wishlist_id_seq TO nielsend_user;
            public       nielsend_db    false    261            �           2604    67972 
   address id    DEFAULT     Z   ALTER TABLE ONLY address ALTER COLUMN id SET DEFAULT nextval('address_id_seq'::regclass);
 9   ALTER TABLE public.address ALTER COLUMN id DROP DEFAULT;
       public       nielsend_db    false    197    196            �           2604    67973    admin_roles id    DEFAULT     b   ALTER TABLE ONLY admin_roles ALTER COLUMN id SET DEFAULT nextval('admin_roles_id_seq'::regclass);
 =   ALTER TABLE public.admin_roles ALTER COLUMN id DROP DEFAULT;
       public       nielsend_db    false    199    198            �           2604    67974    administrator id    DEFAULT     f   ALTER TABLE ONLY administrator ALTER COLUMN id SET DEFAULT nextval('administrator_id_seq'::regclass);
 ?   ALTER TABLE public.administrator ALTER COLUMN id DROP DEFAULT;
       public       nielsend_db    false    201    200            �           2604    67975 	   banner id    DEFAULT     X   ALTER TABLE ONLY banner ALTER COLUMN id SET DEFAULT nextval('banner_id_seq'::regclass);
 8   ALTER TABLE public.banner ALTER COLUMN id DROP DEFAULT;
       public       nielsend_db    false    205    204            �           2604    67976    category id    DEFAULT     \   ALTER TABLE ONLY category ALTER COLUMN id SET DEFAULT nextval('category_id_seq'::regclass);
 :   ALTER TABLE public.category ALTER COLUMN id DROP DEFAULT;
       public       nielsend_db    false    207    206            �           2604    67977 	   config id    DEFAULT     X   ALTER TABLE ONLY config ALTER COLUMN id SET DEFAULT nextval('config_id_seq'::regclass);
 8   ALTER TABLE public.config ALTER COLUMN id DROP DEFAULT;
       public       nielsend_db    false    209    208            �           2604    67978 
   contact id    DEFAULT     Z   ALTER TABLE ONLY contact ALTER COLUMN id SET DEFAULT nextval('contact_id_seq'::regclass);
 9   ALTER TABLE public.contact ALTER COLUMN id DROP DEFAULT;
       public       nielsend_db    false    211    210            �           2604    67979 	   coupon id    DEFAULT     X   ALTER TABLE ONLY coupon ALTER COLUMN id SET DEFAULT nextval('coupon_id_seq'::regclass);
 8   ALTER TABLE public.coupon ALTER COLUMN id DROP DEFAULT;
       public       nielsend_db    false    213    212            �           2604    67980 	   credit id    DEFAULT     X   ALTER TABLE ONLY credit ALTER COLUMN id SET DEFAULT nextval('credit_id_seq'::regclass);
 8   ALTER TABLE public.credit ALTER COLUMN id DROP DEFAULT;
       public       nielsend_db    false    215    214            �           2604    67981 
   jewelry id    DEFAULT     Z   ALTER TABLE ONLY jewelry ALTER COLUMN id SET DEFAULT nextval('jewelry_id_seq'::regclass);
 9   ALTER TABLE public.jewelry ALTER COLUMN id DROP DEFAULT;
       public       nielsend_db    false    217    216            �           2604    67982    order id    DEFAULT     X   ALTER TABLE ONLY "order" ALTER COLUMN id SET DEFAULT nextval('order_id_seq'::regclass);
 9   ALTER TABLE public."order" ALTER COLUMN id DROP DEFAULT;
       public       nielsend_db    false    219    218            �           2604    67983    order_product_option id    DEFAULT     t   ALTER TABLE ONLY order_product_option ALTER COLUMN id SET DEFAULT nextval('order_product_option_id_seq'::regclass);
 F   ALTER TABLE public.order_product_option ALTER COLUMN id DROP DEFAULT;
       public       nielsend_db    false    221    220            �           2604    67984    order_products id    DEFAULT     h   ALTER TABLE ONLY order_products ALTER COLUMN id SET DEFAULT nextval('order_products_id_seq'::regclass);
 @   ALTER TABLE public.order_products ALTER COLUMN id DROP DEFAULT;
       public       nielsend_db    false    223    222            �           2604    67985    page id    DEFAULT     T   ALTER TABLE ONLY page ALTER COLUMN id SET DEFAULT nextval('page_id_seq'::regclass);
 6   ALTER TABLE public.page ALTER COLUMN id DROP DEFAULT;
       public       nielsend_db    false    225    224            �           2604    67986    payment_profiles id    DEFAULT     l   ALTER TABLE ONLY payment_profiles ALTER COLUMN id SET DEFAULT nextval('payment_profiles_id_seq'::regclass);
 B   ALTER TABLE public.payment_profiles ALTER COLUMN id DROP DEFAULT;
       public       nielsend_db    false    227    226            �           2604    67987 
   product id    DEFAULT     Z   ALTER TABLE ONLY product ALTER COLUMN id SET DEFAULT nextval('product_id_seq'::regclass);
 9   ALTER TABLE public.product ALTER COLUMN id DROP DEFAULT;
       public       nielsend_db    false    231    228            �           2604    67988    product_categories id    DEFAULT     p   ALTER TABLE ONLY product_categories ALTER COLUMN id SET DEFAULT nextval('product_categories_id_seq'::regclass);
 D   ALTER TABLE public.product_categories ALTER COLUMN id DROP DEFAULT;
       public       nielsend_db    false    230    229            �           2604    67989    product_images id    DEFAULT     h   ALTER TABLE ONLY product_images ALTER COLUMN id SET DEFAULT nextval('product_images_id_seq'::regclass);
 @   ALTER TABLE public.product_images ALTER COLUMN id DROP DEFAULT;
       public       nielsend_db    false    233    232            �           2604    67990    product_map id    DEFAULT     b   ALTER TABLE ONLY product_map ALTER COLUMN id SET DEFAULT nextval('product_map_id_seq'::regclass);
 =   ALTER TABLE public.product_map ALTER COLUMN id DROP DEFAULT;
       public       nielsend_db    false    235    234            �           2604    67991    ring id    DEFAULT     T   ALTER TABLE ONLY ring ALTER COLUMN id SET DEFAULT nextval('ring_id_seq'::regclass);
 6   ALTER TABLE public.ring ALTER COLUMN id DROP DEFAULT;
       public       nielsend_db    false    239    238            �           2604    67992    ring_material id    DEFAULT     f   ALTER TABLE ONLY ring_material ALTER COLUMN id SET DEFAULT nextval('ring_material_id_seq'::regclass);
 ?   ALTER TABLE public.ring_material ALTER COLUMN id DROP DEFAULT;
       public       nielsend_db    false    241    240            �           2604    67993    role id    DEFAULT     T   ALTER TABLE ONLY role ALTER COLUMN id SET DEFAULT nextval('role_id_seq'::regclass);
 6   ALTER TABLE public.role ALTER COLUMN id DROP DEFAULT;
       public       nielsend_db    false    243    242            �           2604    67994    shipping_method id    DEFAULT     j   ALTER TABLE ONLY shipping_method ALTER COLUMN id SET DEFAULT nextval('shipping_method_id_seq'::regclass);
 A   ALTER TABLE public.shipping_method ALTER COLUMN id DROP DEFAULT;
       public       nielsend_db    false    245    244            �           2604    67995    transaction id    DEFAULT     b   ALTER TABLE ONLY transaction ALTER COLUMN id SET DEFAULT nextval('transaction_id_seq'::regclass);
 =   ALTER TABLE public.transaction ALTER COLUMN id DROP DEFAULT;
       public       nielsend_db    false    247    246            �           2604    67996    user id    DEFAULT     V   ALTER TABLE ONLY "user" ALTER COLUMN id SET DEFAULT nextval('user_id_seq'::regclass);
 8   ALTER TABLE public."user" ALTER COLUMN id DROP DEFAULT;
       public       nielsend_db    false    251    248            �           2604    67997    user_favorite id    DEFAULT     f   ALTER TABLE ONLY user_favorite ALTER COLUMN id SET DEFAULT nextval('user_favorite_id_seq'::regclass);
 ?   ALTER TABLE public.user_favorite ALTER COLUMN id DROP DEFAULT;
       public       nielsend_db    false    250    249            �           2604    67998    user_roles id    DEFAULT     `   ALTER TABLE ONLY user_roles ALTER COLUMN id SET DEFAULT nextval('user_roles_id_seq'::regclass);
 <   ALTER TABLE public.user_roles ALTER COLUMN id DROP DEFAULT;
       public       nielsend_db    false    253    252            �           2604    67999    wedding_band id    DEFAULT     d   ALTER TABLE ONLY wedding_band ALTER COLUMN id SET DEFAULT nextval('wedding_band_id_seq'::regclass);
 >   ALTER TABLE public.wedding_band ALTER COLUMN id DROP DEFAULT;
       public       nielsend_db    false    255    254                       2604    68000    wholesale id    DEFAULT     ^   ALTER TABLE ONLY wholesale ALTER COLUMN id SET DEFAULT nextval('wholesale_id_seq'::regclass);
 ;   ALTER TABLE public.wholesale ALTER COLUMN id DROP DEFAULT;
       public       nielsend_db    false    257    256                       2604    68001    wishlist id    DEFAULT     \   ALTER TABLE ONLY wishlist ALTER COLUMN id SET DEFAULT nextval('wishlist_id_seq'::regclass);
 :   ALTER TABLE public.wishlist ALTER COLUMN id DROP DEFAULT;
       public       nielsend_db    false    261    260            �          0    67627    address 
   TABLE DATA               �   COPY address (id, active, insert_time, is_default, user_id, first_name, last_name, phone, address, address2, city, state, country, zip, label) FROM stdin;
    public       nielsend_db    false    196   �n      �          0    67638    admin_roles 
   TABLE DATA               J   COPY admin_roles (id, active, insert_time, admin_id, role_id) FROM stdin;
    public       nielsend_db    false    198   �n      �          0    67645    administrator 
   TABLE DATA               �   COPY administrator (id, active, insert_time, first_name, last_name, email, username, password, hash, permissions, signup_ip, update_time) FROM stdin;
    public       nielsend_db    false    200   )o      �          0    67657    answer 
   TABLE DATA               d   COPY answer (id, active, insert_time, text, question_id, featured_image, display_order) FROM stdin;
    public       nielsend_db    false    203   p      �          0    67666    banner 
   TABLE DATA               ~   COPY banner (id, active, insert_time, title, image, link, date_modified, is_deal, featured_id, url, banner_order) FROM stdin;
    public       nielsend_db    false    204   �s      �          0    67678    category 
   TABLE DATA               �   COPY category (id, active, insert_time, name, subtitle, parent_category, description, slug, meta_title, meta_keywords, meta_description, display_order, in_menu, image) FROM stdin;
    public       nielsend_db    false    206   �s      �          0    67688    config 
   TABLE DATA               ?   COPY config (id, active, insert_time, name, value) FROM stdin;
    public       nielsend_db    false    208   �s      �          0    67698    contact 
   TABLE DATA               R   COPY contact (id, active, insert_time, name, email, message, company) FROM stdin;
    public       nielsend_db    false    210   �t      �          0    67708    coupon 
   TABLE DATA               �   COPY coupon (id, active, insert_time, name, code, discount_type, discount_amount, min_amount, num_uses_all, uses_per_user, start_time, end_time) FROM stdin;
    public       nielsend_db    false    212   Ow      �          0    67718    credit 
   TABLE DATA               S   COPY credit (id, active, insert_time, name, description, price, value) FROM stdin;
    public       nielsend_db    false    214   lw      �          0    67728    jewelry 
   TABLE DATA               �  COPY jewelry (id, active, insert_time, name, description, price, currency_code, quantity, tags, materials, images, variation, meta_title, meta_keywords, meta_description, metal_colors, metals, total_14kt_price, total_18kt_price, total_plat_price, slug, semi_mount, stone_breakdown, stone_count, weight, diamond_cwwt, diamond_color, diamond_quality, _14kt, _18kt, platinum, setting, polish, melee, ship_from_cast, ship_to_cust, memo_duration) FROM stdin;
    public       nielsend_db    false    216   �w      �          0    67744    order 
   TABLE DATA               6  COPY "order" (id, active, insert_time, bill_first_name, bill_last_name, bill_address, bill_address2, bill_city, bill_state, bill_country, bill_zip, ship_first_name, ship_last_name, ship_address, ship_address2, ship_city, ship_state, ship_country, ship_zip, cc_number, cc_expiration_month, cc_expiration_year, cc_ccv, tax, tax_rate, status, tracking_number, shipping_method, user_id, coupon_code, gift_card, email, shipping_cost, payment_method, subtotal, total, viewed, phone, coupon_type, coupon_amount, note, user_role, ref_num, order_type, signature) FROM stdin;
    public       nielsend_db    false    218   �w      �          0    67754    order_product_option 
   TABLE DATA               _   COPY order_product_option (id, active, insert_time, order_product_id, data, price) FROM stdin;
    public       nielsend_db    false    220   �w      �          0    67765    order_products 
   TABLE DATA               j   COPY order_products (id, active, insert_time, order_id, quantity, unit_price, product_map_id) FROM stdin;
    public       nielsend_db    false    222   �w      �          0    67772    page 
   TABLE DATA               �   COPY page (id, active, insert_time, title, slug, description, featured_image, meta_title, meta_keywords, meta_description, date_modified, content) FROM stdin;
    public       nielsend_db    false    224   �w      �          0    67782    payment_profiles 
   TABLE DATA               �   COPY payment_profiles (id, active, insert_time, user_id, first_name, last_name, phone, address, address2, city, state, country, zip, cc_number, cc_expiration_month, cc_expiration_year, cc_ccv, label) FROM stdin;
    public       nielsend_db    false    226   x      �          0    67792    product 
   TABLE DATA               �  COPY product (id, active, insert_time, name, sku, price, meta_title, meta_keywords, meta_description, featured_image, colors, featured, shape, clarity, weight, lab, cut_grade, polish, symmetry, fluor, rapaport_price, total, certificate, length, width, depth, depth_percent, table_percent, girdle, culet, description, origin, memo_status, inscription, certificate_file, slug, wholesale_price, video_link, quantity, memo_duration, vendor, discount) FROM stdin;
    public       nielsend_db    false    228   7x      �          0    67802    product_categories 
   TABLE DATA               [   COPY product_categories (id, active, insert_time, category_id, product_map_id) FROM stdin;
    public       nielsend_db    false    229   Tx      �          0    67811    product_images 
   TABLE DATA               `   COPY product_images (id, active, insert_time, image, display_order, product_map_id) FROM stdin;
    public       nielsend_db    false    232   qx      �          0    67821    product_map 
   TABLE DATA               T   COPY product_map (id, active, insert_time, product_id, product_type_id) FROM stdin;
    public       nielsend_db    false    234   �x      �          0    67830    question 
   TABLE DATA               q   COPY question (id, active, insert_time, text, failure_text, image, display_order, correct_answer_id) FROM stdin;
    public       nielsend_db    false    237   �x      �          0    67839    ring 
   TABLE DATA               �  COPY ring (id, active, insert_time, name, price, default_metal, default_color, dwt, semi_mount_weight, stone_breakdown, center_stone_weight, center_stone_shape, total_stones, semi_mount_diamond_weight, diamond_color, diamond_quality, cost_per_dwt, fourteenkt_gold_price, eighteenkt_gold_price, platinum_price, website_fourteenkt_gold_price, website_eighteenkt_gold_price, website_platinum_price, slug, min_carat, max_carat, shape, video_link, metals, colors, metal_colors, setting_labor, center_stone_setting_labor, polish_labor, head_to_add, melee_cost, shipping_from_cast, shipping_to_customer, total_14kt_cost, total_18kt_cost, total_plat_cost, setting_style, options, alias, meta_title, meta_keywords, meta_description, description, memo_duration) FROM stdin;
    public       nielsend_db    false    238   �|      �          0    67855    ring_material 
   TABLE DATA               v   COPY ring_material (id, active, insert_time, name, price, min_size, max_size, size_step, size_step_price) FROM stdin;
    public       nielsend_db    false    240   �|      �          0    67870    role 
   TABLE DATA               6   COPY role (id, active, insert_time, name) FROM stdin;
    public       nielsend_db    false    242   �|      �          0    67880    shipping_method 
   TABLE DATA               l   COPY shipping_method (id, active, insert_time, is_default, name, cart_subtotal_range_min, cost) FROM stdin;
    public       nielsend_db    false    244   B}      �          0    67891    transaction 
   TABLE DATA               c   COPY transaction (id, active, insert_time, order_id, authorize_net_data, ref_trans_id) FROM stdin;
    public       nielsend_db    false    246   _}      �          0    67901    user 
   TABLE DATA               x   COPY "user" (id, active, insert_time, email, password, hash, first_name, last_name, phone, photo, username) FROM stdin;
    public       nielsend_db    false    248   |}      �          0    67909    user_favorite 
   TABLE DATA               O   COPY user_favorite (id, active, insert_time, user_id, provider_id) FROM stdin;
    public       nielsend_db    false    249   �~      �          0    67918 
   user_roles 
   TABLE DATA               H   COPY user_roles (id, active, insert_time, user_id, role_id) FROM stdin;
    public       nielsend_db    false    252   �~      �          0    67925    wedding_band 
   TABLE DATA               �  COPY wedding_band (id, active, insert_time, name, slug, price, category, diamond_color, diamond_quality, video_link, metals, colors, metal_colors, setting_labor, polish_labor, head_to_add, melee_cost, shipping_from_cast, shipping_to_customer, total_14kt_cost, total_18kt_cost, total_plat_cost, options, setting_style, alias, meta_title, meta_keywords, meta_description, memo_duration) FROM stdin;
    public       nielsend_db    false    254   ��      �          0    67940 	   wholesale 
   TABLE DATA               �   COPY wholesale (id, active, insert_time, company, job_title, dba, incorporation, referrer, phone, alt_phone, status, address, address2, payment, memo_duration, site, logo, discount, tax_id) FROM stdin;
    public       nielsend_db    false    256   ��                0    67954    wholesale_items 
   TABLE DATA               \   COPY wholesale_items (id, active, insert_time, price, wholesale_id, product_id) FROM stdin;
    public       nielsend_db    false    259   ՛                0    67962    wishlist 
   TABLE DATA               C   COPY wishlist (id, active, insert_time, user_id, data) FROM stdin;
    public       nielsend_db    false    260   �      l           0    0    address_id_seq    SEQUENCE SET     6   SELECT pg_catalog.setval('address_id_seq', 1, false);
            public       nielsend_db    false    197            m           0    0    admin_roles_id_seq    SEQUENCE SET     9   SELECT pg_catalog.setval('admin_roles_id_seq', 1, true);
            public       nielsend_db    false    199            n           0    0    administrator_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('administrator_id_seq', 1, true);
            public       nielsend_db    false    201            o           0    0    answer_id_seq    SEQUENCE SET     5   SELECT pg_catalog.setval('answer_id_seq', 31, true);
            public       nielsend_db    false    202            p           0    0    banner_id_seq    SEQUENCE SET     5   SELECT pg_catalog.setval('banner_id_seq', 1, false);
            public       nielsend_db    false    205            q           0    0    category_id_seq    SEQUENCE SET     7   SELECT pg_catalog.setval('category_id_seq', 1, false);
            public       nielsend_db    false    207            r           0    0    config_id_seq    SEQUENCE SET     5   SELECT pg_catalog.setval('config_id_seq', 1, false);
            public       nielsend_db    false    209            s           0    0    contact_id_seq    SEQUENCE SET     6   SELECT pg_catalog.setval('contact_id_seq', 25, true);
            public       nielsend_db    false    211            t           0    0    coupon_id_seq    SEQUENCE SET     5   SELECT pg_catalog.setval('coupon_id_seq', 1, false);
            public       nielsend_db    false    213            u           0    0    credit_id_seq    SEQUENCE SET     5   SELECT pg_catalog.setval('credit_id_seq', 1, false);
            public       nielsend_db    false    215            v           0    0    jewelry_id_seq    SEQUENCE SET     6   SELECT pg_catalog.setval('jewelry_id_seq', 1, false);
            public       nielsend_db    false    217            w           0    0    order_id_seq    SEQUENCE SET     4   SELECT pg_catalog.setval('order_id_seq', 1, false);
            public       nielsend_db    false    219            x           0    0    order_product_option_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('order_product_option_id_seq', 1, false);
            public       nielsend_db    false    221            y           0    0    order_products_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('order_products_id_seq', 1, false);
            public       nielsend_db    false    223            z           0    0    page_id_seq    SEQUENCE SET     3   SELECT pg_catalog.setval('page_id_seq', 1, false);
            public       nielsend_db    false    225            {           0    0    payment_profiles_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('payment_profiles_id_seq', 1, false);
            public       nielsend_db    false    227            |           0    0    product_categories_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('product_categories_id_seq', 1, false);
            public       nielsend_db    false    230            }           0    0    product_id_seq    SEQUENCE SET     6   SELECT pg_catalog.setval('product_id_seq', 1, false);
            public       nielsend_db    false    231            ~           0    0    product_images_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('product_images_id_seq', 1, false);
            public       nielsend_db    false    233                       0    0    product_map_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('product_map_id_seq', 1, false);
            public       nielsend_db    false    235            �           0    0    question_id_seq    SEQUENCE SET     7   SELECT pg_catalog.setval('question_id_seq', 10, true);
            public       nielsend_db    false    236            �           0    0    ring_id_seq    SEQUENCE SET     3   SELECT pg_catalog.setval('ring_id_seq', 1, false);
            public       nielsend_db    false    239            �           0    0    ring_material_id_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('ring_material_id_seq', 1, false);
            public       nielsend_db    false    241            �           0    0    role_id_seq    SEQUENCE SET     2   SELECT pg_catalog.setval('role_id_seq', 4, true);
            public       nielsend_db    false    243            �           0    0    shipping_method_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('shipping_method_id_seq', 1, false);
            public       nielsend_db    false    245            �           0    0    transaction_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('transaction_id_seq', 1, false);
            public       nielsend_db    false    247            �           0    0    user_favorite_id_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('user_favorite_id_seq', 1, false);
            public       nielsend_db    false    250            �           0    0    user_id_seq    SEQUENCE SET     2   SELECT pg_catalog.setval('user_id_seq', 3, true);
            public       nielsend_db    false    251            �           0    0    user_roles_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('user_roles_id_seq', 588, true);
            public       nielsend_db    false    253            �           0    0    wedding_band_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('wedding_band_id_seq', 1, false);
            public       nielsend_db    false    255            �           0    0    wholesale_id_seq    SEQUENCE SET     8   SELECT pg_catalog.setval('wholesale_id_seq', 1, false);
            public       nielsend_db    false    257            �           0    0    wholesale_items_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('wholesale_items_id_seq', 6, true);
            public       nielsend_db    false    258            �           0    0    wishlist_id_seq    SEQUENCE SET     7   SELECT pg_catalog.setval('wishlist_id_seq', 1, false);
            public       nielsend_db    false    261                       2606    68003    address address_pkey 
   CONSTRAINT     K   ALTER TABLE ONLY address
    ADD CONSTRAINT address_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.address DROP CONSTRAINT address_pkey;
       public         nielsend_db    false    196            
           2606    68005    admin_roles admin_roles_pkey 
   CONSTRAINT     S   ALTER TABLE ONLY admin_roles
    ADD CONSTRAINT admin_roles_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY public.admin_roles DROP CONSTRAINT admin_roles_pkey;
       public         nielsend_db    false    198                       2606    68007     administrator administrator_pkey 
   CONSTRAINT     W   ALTER TABLE ONLY administrator
    ADD CONSTRAINT administrator_pkey PRIMARY KEY (id);
 J   ALTER TABLE ONLY public.administrator DROP CONSTRAINT administrator_pkey;
       public         nielsend_db    false    200                       2606    68009    answer answer_pkey 
   CONSTRAINT     I   ALTER TABLE ONLY answer
    ADD CONSTRAINT answer_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.answer DROP CONSTRAINT answer_pkey;
       public         nielsend_db    false    203                       2606    68011    banner banner_pkey 
   CONSTRAINT     I   ALTER TABLE ONLY banner
    ADD CONSTRAINT banner_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.banner DROP CONSTRAINT banner_pkey;
       public         nielsend_db    false    204                       2606    68013    category category_pkey 
   CONSTRAINT     M   ALTER TABLE ONLY category
    ADD CONSTRAINT category_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.category DROP CONSTRAINT category_pkey;
       public         nielsend_db    false    206                       2606    68015    config config_pkey 
   CONSTRAINT     I   ALTER TABLE ONLY config
    ADD CONSTRAINT config_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.config DROP CONSTRAINT config_pkey;
       public         nielsend_db    false    208                       2606    68017    contact contact_pkey 
   CONSTRAINT     K   ALTER TABLE ONLY contact
    ADD CONSTRAINT contact_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.contact DROP CONSTRAINT contact_pkey;
       public         nielsend_db    false    210                       2606    68019    coupon coupon_pkey 
   CONSTRAINT     I   ALTER TABLE ONLY coupon
    ADD CONSTRAINT coupon_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.coupon DROP CONSTRAINT coupon_pkey;
       public         nielsend_db    false    212                       2606    68021    credit credit_pkey 
   CONSTRAINT     I   ALTER TABLE ONLY credit
    ADD CONSTRAINT credit_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.credit DROP CONSTRAINT credit_pkey;
       public         nielsend_db    false    214                       2606    68023    order order_pkey 
   CONSTRAINT     I   ALTER TABLE ONLY "order"
    ADD CONSTRAINT order_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY public."order" DROP CONSTRAINT order_pkey;
       public         nielsend_db    false    218                        2606    68025 .   order_product_option order_product_option_pkey 
   CONSTRAINT     e   ALTER TABLE ONLY order_product_option
    ADD CONSTRAINT order_product_option_pkey PRIMARY KEY (id);
 X   ALTER TABLE ONLY public.order_product_option DROP CONSTRAINT order_product_option_pkey;
       public         nielsend_db    false    220            "           2606    68027 "   order_products order_products_pkey 
   CONSTRAINT     Y   ALTER TABLE ONLY order_products
    ADD CONSTRAINT order_products_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY public.order_products DROP CONSTRAINT order_products_pkey;
       public         nielsend_db    false    222            $           2606    68029    page page_pkey 
   CONSTRAINT     E   ALTER TABLE ONLY page
    ADD CONSTRAINT page_pkey PRIMARY KEY (id);
 8   ALTER TABLE ONLY public.page DROP CONSTRAINT page_pkey;
       public         nielsend_db    false    224            &           2606    68031 &   payment_profiles payment_profiles_pkey 
   CONSTRAINT     ]   ALTER TABLE ONLY payment_profiles
    ADD CONSTRAINT payment_profiles_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY public.payment_profiles DROP CONSTRAINT payment_profiles_pkey;
       public         nielsend_db    false    226                       2606    68033    jewelry preset_pkey 
   CONSTRAINT     J   ALTER TABLE ONLY jewelry
    ADD CONSTRAINT preset_pkey PRIMARY KEY (id);
 =   ALTER TABLE ONLY public.jewelry DROP CONSTRAINT preset_pkey;
       public         nielsend_db    false    216            *           2606    68035 *   product_categories product_categories_pkey 
   CONSTRAINT     a   ALTER TABLE ONLY product_categories
    ADD CONSTRAINT product_categories_pkey PRIMARY KEY (id);
 T   ALTER TABLE ONLY public.product_categories DROP CONSTRAINT product_categories_pkey;
       public         nielsend_db    false    229            ,           2606    68037 "   product_images product_images_pkey 
   CONSTRAINT     Y   ALTER TABLE ONLY product_images
    ADD CONSTRAINT product_images_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY public.product_images DROP CONSTRAINT product_images_pkey;
       public         nielsend_db    false    232            .           2606    68039    product_map product_map_pkey 
   CONSTRAINT     S   ALTER TABLE ONLY product_map
    ADD CONSTRAINT product_map_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY public.product_map DROP CONSTRAINT product_map_pkey;
       public         nielsend_db    false    234            (           2606    68041    product product_pkey 
   CONSTRAINT     K   ALTER TABLE ONLY product
    ADD CONSTRAINT product_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.product DROP CONSTRAINT product_pkey;
       public         nielsend_db    false    228            0           2606    68043    question question_pkey 
   CONSTRAINT     M   ALTER TABLE ONLY question
    ADD CONSTRAINT question_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.question DROP CONSTRAINT question_pkey;
       public         nielsend_db    false    237            4           2606    68045     ring_material ring_material_pkey 
   CONSTRAINT     W   ALTER TABLE ONLY ring_material
    ADD CONSTRAINT ring_material_pkey PRIMARY KEY (id);
 J   ALTER TABLE ONLY public.ring_material DROP CONSTRAINT ring_material_pkey;
       public         nielsend_db    false    240            2           2606    68047    ring ring_pkey 
   CONSTRAINT     E   ALTER TABLE ONLY ring
    ADD CONSTRAINT ring_pkey PRIMARY KEY (id);
 8   ALTER TABLE ONLY public.ring DROP CONSTRAINT ring_pkey;
       public         nielsend_db    false    238            6           2606    68049    role role_pkey 
   CONSTRAINT     E   ALTER TABLE ONLY role
    ADD CONSTRAINT role_pkey PRIMARY KEY (id);
 8   ALTER TABLE ONLY public.role DROP CONSTRAINT role_pkey;
       public         nielsend_db    false    242            8           2606    68051 $   shipping_method shipping_method_pkey 
   CONSTRAINT     [   ALTER TABLE ONLY shipping_method
    ADD CONSTRAINT shipping_method_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY public.shipping_method DROP CONSTRAINT shipping_method_pkey;
       public         nielsend_db    false    244            :           2606    68053    transaction transaction_pkey 
   CONSTRAINT     S   ALTER TABLE ONLY transaction
    ADD CONSTRAINT transaction_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY public.transaction DROP CONSTRAINT transaction_pkey;
       public         nielsend_db    false    246            >           2606    68055     user_favorite user_favorite_pkey 
   CONSTRAINT     W   ALTER TABLE ONLY user_favorite
    ADD CONSTRAINT user_favorite_pkey PRIMARY KEY (id);
 J   ALTER TABLE ONLY public.user_favorite DROP CONSTRAINT user_favorite_pkey;
       public         nielsend_db    false    249            <           2606    68057    user user_pkey 
   CONSTRAINT     G   ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public."user" DROP CONSTRAINT user_pkey;
       public         nielsend_db    false    248            @           2606    68059    user_roles user_roles_pkey 
   CONSTRAINT     Q   ALTER TABLE ONLY user_roles
    ADD CONSTRAINT user_roles_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.user_roles DROP CONSTRAINT user_roles_pkey;
       public         nielsend_db    false    252            B           2606    68061    wedding_band wedding_band_pkey 
   CONSTRAINT     U   ALTER TABLE ONLY wedding_band
    ADD CONSTRAINT wedding_band_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.wedding_band DROP CONSTRAINT wedding_band_pkey;
       public         nielsend_db    false    254            F           2606    68063 $   wholesale_items wholesale_items_pkey 
   CONSTRAINT     [   ALTER TABLE ONLY wholesale_items
    ADD CONSTRAINT wholesale_items_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY public.wholesale_items DROP CONSTRAINT wholesale_items_pkey;
       public         nielsend_db    false    259            D           2606    68065    wholesale wholesale_pkey 
   CONSTRAINT     O   ALTER TABLE ONLY wholesale
    ADD CONSTRAINT wholesale_pkey PRIMARY KEY (id);
 B   ALTER TABLE ONLY public.wholesale DROP CONSTRAINT wholesale_pkey;
       public         nielsend_db    false    256            H           2606    68067    wishlist wishlist_pkey 
   CONSTRAINT     M   ALTER TABLE ONLY wishlist
    ADD CONSTRAINT wishlist_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.wishlist DROP CONSTRAINT wishlist_pkey;
       public         nielsend_db    false    260            �      x������ � �      �   ,   x�3�4�4204�5��52T04�21�22�3�4�r��qqq x�      �   �   x�%��j� ���)� n���=m�s����^4�A�(d,�o_C�g���|J(����N��\�t�x��h_���˕6��8.u�U��u�Z���I��)]BН�40!&uTځ z��]�s��}b9�t�d;,^kiT�|�+�|yԕ�~�A���Î��7q˵����қ<(�4>J�y�[.���[�Y�o=�}��K�Ny      �   �  x��U]��F}��󒪕�����-A�6�f��F�"Yc{ S��fwɯ�5`@�J���>��˹�b�Sfn���
37�t")��Ð5o�B��s��(�����s,�*M����ϱ��'�mw��o��f�	�N	��9�^c��f�¼��ưR��MS�nV��wU(�&ձh�=�a�D	"��Ρ�~�g��#�H��H-���i�����C��zq*�eS7�r"8�Y��>��]�Uh�_���%�!�௩Y�ߢ�����K,����W���eܬ���T��������pBQ��4�E�k�E�^�j��o���]��c(�e��ߗ�ʫ���fL�m���L��i�m�4R�(��s�y�m���$�gV�3[⬑���~Y��eb�����M�O;������L�ݏ.�-�^c��4v�3q8�׍� Z���e���XU�?t�P���NUl�4U��QGС���c�'�?%b8����]��\-�	(/�e{�'���HMȓo��^��|^�̫�IUx��#����iyg�瀕I�b�C��B=�B��qs�����t�;&X�T�R����H��uȳ�r�׵f��*���� �3\�O�	l�?'��l�_r3�_��1��1^H����r;<K�Q�R�������ر
M�s�X���p�<�g-�N奘K;�����
�D�9����Y\��_`w��`h;V�hn��Q�v�3��sN��I4mbہ�~L���tV�GK��B���v����6��.b�XG���j4�@��YK����\ۃY������ާ�8�$��������a%F�����ǽ]�\#��� �@��F�C^)a���*��CT�K��)8ጂ�p/�C�Ư��BZ��EƎ,��ܱ�/3����Bs�� Ⱥ��������j�      �      x������ � �      �      x������ � �      �   �   x����J1��S|��LF�=.�I�� ���4c$��=y{���U\p}���f����]°��;��ev�o5�RfhS�	�cR�T�*F/��N��Z�������*i�v����}�2�j�*#�"��~"#�.V���KWw��*Q����;��+gi_u["nI$�Q�w��OnQi�      �   ~  x�m�Mr�0���)�e�J�R�o6>�� �ƶHL0$��Gb��������0`(���_�]!�Az���
Y|�)�{��8>��V��AI
�.g����q/���ʜMΙ�� � >������э#����oڶb޿�p=�L@��$Ų�g��n�O��~�{N3��@R��s,N,��m��7�=�㉛G�WNB�Jh�<Ky㡸M��I�p�S7ő��Ơ�І��i�2KC����O]������.�
�BPF��]f_]�M�u�}�����
�:&��X7dv�s���i;�8���� IXO���n{�?��OV��n��qק����9�r#S�E������fs��p�B��J��%�by�������$�FM�߯�����Z�|��2h%�!��u�syv���]�b�_-\.}�i-�#�d '�ETP���U�`�m�ā�,�
 ���D=��7%f��:^�K�T
���Ys�`�v5+D����U��t��r���ڵ6?�(H����OMV�+�٢V�ǖ���&ۂ�3뉳J���c=:��P�%Z3���O��&�jI]�ϔ����m��YG9�'���/T�V���^<�q>/W���/�^����y���s�%�g.      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �   �  x�eU�n�8>SO1��)�J��_4@�bwS M��^h��XK�@Rq��}���>��PR�Āa@9���8aK�8�D�&�A\�w��6�8�K�6ukkƾ?̟ w��G�~[�EGi�>+����V����Fa*�� ��g�Ojg=��:���
�W���1z����#7_�����}�	x<����4���Q[���z������=?s��g,.�����o�k�GeT�O
1˪[:YVTNK���fjg�o�D��G��j��=��q2Uǭ�P�)��8VP�I�x��t���(�w�� ��<��L%��G��Y,ȕ��T����w �9K� ~�ػ��¨��h�>ϒ&)�]���^lM=W5*y�̀�!����E%w�3V��ٓ�t��=�)�G��tu��O�Ѱ�>x��C-V�O�r�-�7͉�
��Ċ}R��".�}��ʶ��֢�X�LYQ?��ڹA��G;1�Ox��È�R	{���j(P�[���F47�e�˙�Em��4H����m�YV�b� �͖�9�/������N��L�Z�E}��(�-�h�?����]�4� <N�
 �4���$yߣ��<�W�G�[ÇK�����zh�}���6�ʂ�J(N1�Z�̽1b�!���^&���� �(�4�NOF��6���'||Y�����C��P]�a�<S���٣���#�ɋ#r���l�X�O0L���\��$j�E^m����b�*;Vٵ>1�XQfE��>FOm��0"�f���o]hoq�Z���J"�M��9R��Lٛ�ñ��FQ���Q�h`_t%>���ң�莓�v�}���4��K�9D�>���8L�,�b�5�.��Y�4Z�nN����n��L��J)����cd7ZWi��q�h#Ǜq����lC��<(������Y�-a�it�k??4l�p�'�"S�� -��Έkl�Yww~��:���f֗WF[��7�B�3���%��{���g�      �      x������ � �      �      x������ � �      �   V   x�3�4�4204�5��52P04�25�24�333�02�LL����2¯(���$?7�����������:���3�sR�sR�b���� ($      �      x������ � �      �      x������ � �      �   +  x�m�An�1F��O�$�`�W������GY$��lz�q�j&��`�=>L9!d9�_rݩ���YR��v�;��8n�T��q]:�0/�-(�i�	Θ�T�sIfm�^!�"������,��}�zz�W��-'�+��%�N�Ӓ,\I��>>�~֬�$@uP7����Zc��V����9dqy�᰺�:��dk6p���vp�V�Pșs,���r���F�Y��&��HJ���D��ɥ��޺*�j�,�ЦJ.e���[�j��sĬR�?���|��������>=��~ܶ��I��      �      x������ � �      �      x�m�[��:�C��Fq&p2$>D)�r�?��Y�i�#��.��(�K���g�ؘ��8���g����^3���~Ӿ�9��y^�?WR��9�P�;�{�+l׶�=)�/�4��;��}�� �o��3�������F@��o�n��<_'�:��u�u�ֿ��Ow��8��7�{��4�����=�=�k��b��E���(����zD�5wF�Ϯ��]�[���n�k/s�}o���?#��Ó�y~��ȾE�{��kX���3pO˿D��s�b�|�<��p�W<E���ھ?s�?�ߢ��8嘆�k���?N��D����1p�ױe�|շHO�Qx��@�_�)
X���!8�u�"�����9Q����Z�y�M�|jL#^�������B0M�mp��"��!�����x�S5߉�����l�B0|=�Ps0�k=U����e���A�>�0xp�y�4�G���C5�T��8�u��gCኗ�'�㾿��a�w`�N���G~�����Mcl>����=���Ő�k��T�]�t��k���`N����Y�Q�B�R �ɱ��co��.����}��a���m���q��TUS!)��k�X�!�*T������9f;�c����4��P�	og2˜�T�+��6�"��|�=�Zp�{῜�s2����s�s*�/c���|��9͙�ux�ơ?qWa�1����A�Aơ?�TN�-q-� ��1��j*�"2w��Ã�Ȱ?uW!$��Z�F�Ɍa�;��3w_�Q-��V���5�+��K��3�e��4�}�װ-�|�62����|&���Q\17A��7�yx�_XTDY<d��/L�p\�M����fw1���E�uo�.�\��᯴8\T<t���}�8T����b@<u�o��-.�9_�#ۗd�.pٛ7,��#9Z��Ä�?d�Z8^5ߒ�&C8.����	�*؅t�b���C�a3-+51���
a�c<�aR�Se���j$��\M��2v0�"$�&�DGEeX�9�C���-N,N��݀xk��"��1!�����kq�`݀�Lc{'�r���Ԏ�wn���J��2T;|���-�,�UY��<��A0p��S�4e�u0��XK)9e�=MH��ӹ�(e�=M�B��� ��S21;�e�Hq{�u�8�/�w�y!Q�b�s�N��݅��X/0`j�\��B��@��]��%�� �m�Z���\��/�΃��6�Ғ�]HŸ�"����J� }���l1-������*�l�k,����q��Ps�[��t!B<c��Xr!�B'�U�P��jr!��a�$���&���8s#�.c�\r!���Ŕ9�[Ʉ�&H�[Cm�P�%����^�<��T�n�R���A�8�ĉ��x,2C<d�la"�-�������dB>M�lFv[i�!K&��0�c���%�iB8�fp�`˄|�@�BT�$P�xg>]`E=Q��!0�bޙ݅Ҥǀ�%�!��](=(�6JW��wfwAx�L��\兾3��z�>��&�� ���y�'��� ��9�H�Ԗ�� ���S��\X݅R
AB�.���Pܘ@�`ȖL��๺��w:��	�i�e"�7td�z�p��+�W,<���)�q#9��Ṟ&p�1�h���s=M�E�V$�)"��M،#TdG�fA�n��0 �3܄ų�	��@@甸xV�`�xf�\�
P`<�{���^��@d<���9����ɅTh<�����#����xV7���@���Lp<��pXFq&�G	8��Y�ɰ��w�u�9�kw$À$�΂!N�݄C �	\%�� v7�tP�d�)vw�h2D%��T�<ww����G�<ww���5�a�t�raw ;�n�d��S.��d�󨣮�Ʌ�t�X��0�6�'��t�TGa�Stj*��.$M�R��"�y�. ���	��%T����VyQS�<���Sp.p�D��<] p"	F\W���������xy��!Vk3o��y���z!���&b�����p�Y&�����|���肍��T�:��:ے5H�y�7�(�Hf�S��܏��̟2g�X�U���,�2�����_���9_�O�?��m��̴�&�2#�C�zʈ(}֢�bf�!c~�#�9tA�l��M�>���*2�Й�2�����G�l��m*#!�Y�r��B�fʸE��уT'h�͔-�4��5���:5sw̯-�JN>a�ul�,8��(T2�c3UL6d��uH�l�Y�:�X� 6[�f�&V��(=L�l�������JU=�f����M[�LF�f��LY�LEr È��s3d9X� ���>��͔M�ʨ�7[�fʌ��-0/���s3e���ʔ�O�l��+!�O�g���:6�_/u�u5�б�2��.�2eB�f�X�s�E{��f��̍����Le7a�ul�:4�5��n\I�l�)S��DО���:6SvG���%R�l�!�W��&���:6_2���3K&tj��������&j�N�F��J�Ȣ�
�f��LZKf�f�5[�f�/��LS^M&tj�ѦM��<��L� åuT�l�)+��R$�f��L�ᔇ�K����Ԝ�p�\=D5����_�S�%�\�6�������,��Y|;s�����|ɖ��a������'U�׽Ʉ;5_���)�n5۝�/���'�
A�ݡY*����^�����̗j)ˠh`L����̗l���3��-�������5Y�l�� �ȸ.e@1�e�`~:���P�l�=�L{V%��̖݃�ٵ�cN+1�e��}(^m�����{0�G�f0*!�	��0Ɉʊ����Z�k��E�l�{`�����W�l�{`�Ac
�5�Q�l�{��2f?a)��mu\�q�Q_��̶���X�Ph9\�l�{��x�^%�$d��=ȷ�87y�DT� �f���Bf��h|�{��=X^$-dg���ru��㵀7�M.f��.O'$�y���bf��B���4�bf��B�8J�Hϼ�\���V���dr���Ë��P�@&�����:��3WK�vwA=e�}X��%�mw6��}�WL����.50nco�w3��.�K!́�Ʌ�]8��AY�(P���]�:�7󆋙mw��0K��F亘�vw�|6aV0B�̶��S���vwA��.0}�����.e�^@�dr�t��>X�V���i.���NL�$���َ?e���)�O\�l'�������.f��Og��j
�\8�)[jb����&N=dbN����v�S��-`��bf;�)+����V���}���Z&Q��I�d�!����"y��G��	��-���42�x���1�	�lJ�4������LHBfO��[�1C"d��4�+8LpV�.b��� �n��o�dO2yk���>�S�h�9�.`��-�������gw`j�Ȑō�~/��\���SY��[0��2i��-/���6�ى	n�x�g���BD�0����ze< �<���6Uώ�T���mXU;��>�	�暃����n�cɎϖJ
0�u�%&_b>!U��٭������������٭��ڏg��9�.`v�.�vT�+x�O*`v�.�V$�3�W���]��,�
��Y���]��b���@1�uRy�)��0�"f��d:'1�w��1FW���MP�iw,���a/����X8p�����ݻɲ8�rfM]�[��v�G7Qe�7����N�Zs���D/�wRK�M.L+�{����41�+늗ݻ�"�Ź�e�{�@MD̗'�����u�
a�M��f�����=6��f�nB��(?�>��٣�����01.`��lL˙b�
"`�̔�/,}�����3�D���1�� ����+Y���G��?� 稒0{f�0H�OL�e�,X&���Y�T��������z�Љ�ڢ֑������3��OǰmL`1{'f�.ƣX1���3[�ƭ����y� �  �;1s�N��j�b�&b�N��P}a\"Y�yٮ�9��l�����2Am7i�u��;.�7X�@�4�,����6�vP'
\ޙ�ӲT�3��=������K�ɭ4n��N��qy"9�h�X�����K�������q��qy�8�jB����e���%+��Z��.��%pL��S�e���u�����ME�~�奓d�U����e���q�D�*�"Ѳ�=d<����9[ǡ�W�ٔM?DZp�?D�^�
��͑-{�C��n�r�;�^�F-�k�Y!X��H|�Q<6*X��\��_<��Ô�e���X�2�2���}m���,�����{H���	wX�t�V�+����e�òdq]C
���a�⮕��Q���,YL��x�ëɅ;,_2S�1���`��\BH���4G����wX.�8së�V�,���S���FW"�x���J�QN�`�Os�������!X�c��/�s����f��V�6�
*yp���fVX<�P�O>dv�."B��4��ԇ��)��H�����c�"�����ytn�;�!R��-�J3X[���1�[��u�Aٖ�[��q��a�*��l�Ä�p�	�ct$�Æ%��*��&l1p���U�v�ctt��(��	�ctΧ��g�B�����/��:��S��1�	G|��Y�r!V��MоO�-�%�P9f�@Gx@}���b���O�-C]�r�����a���P�X9f���q��<�/T�ռ�v`p�C�32��4�u�*�\O�]� ���Ge����G�Y��s���`�L�� �zy0�C�t�ar.�L�f�W�Bl���*�͇��C`"`]^��L�f@�b���Bg�ú	�M���b�*�u���ѵ9WB��]���QLGPɄ;)�On�P�)��)ǝ��_C
k<���&R�;)Kڀ(O�2�p'�����(㼯)ǝ�%#�l\m��>�r�IY�H����ѡ�r�Qy��}���	��r�QY��2��f�j-ǝ�%����,c���qg��� (Q�-Zb��nBj��Q��]B��=H5f�/Z��ߔ�=`Q�MѼV>�rx�@G8Q0�y�!T���~�oQ-��H9�{���恎�H9�{��N��y,�'�;F݂���&J	XPS�n��."�g�0h����un���"��89�9��G��}n%qrD���	;Z!N��O�� t"�89�<e�)�1������u��سqr�|�L/�E���'G6�K��(+�(�89�2�aw�K�i���0*�K�8922��lʗO)�	�M0��2<C��89��`�-������@��M���	��b%R��&8 �����Pg9V7���j���ʅ�]p&�\[?*Vw�?�v�^2�T����d�Ɇ�Q�r��^ �*���h%X��]�O���?*Vw!>�s�ԟ,��.�E�N|�r��B~�m�Y	�cuR/�'w�� �rTw!?[$#d!X��&\/)^�?_P,Gu�6(Y
���	�������J�rT7a�g��c#@R��MX���x�!j�h9��PB&��Y��-GuJ���6�1ݛL�n�u�����f�h9���?G�o��Ge��&\�1�I�#ȅ�]8\��5x^>˱�	v�-��2˱�!3@��R8˱�	�����o�d�·��,�2�q�����!�K�����2)X��L�.��bO"˱�SvtoH�(rS���p��a�E���'�q�C��Ԍ�&�|Ȣ�[�Xt �7�|T�9--�մT�wZ>b&_�+�������g���*;��.��ϧ'�CN�}Q����%���3Q�x9�|�^`��R�w^��z[t������q������x��<ƚ�����|�|��k�)^�;/5��㉑)^��\�:�S=�j��x9Gs��y��Ք"^��\jr	��W� ��AFsA��i?Y�P����^`@y�_��s�CF��� �ئ�9Gs�A������E�0�8��O�,Js�񐱯�5���@�捘�z�޸���F�\��%���<P��9X�捘!���Ʃ�#��)d�2�^����)�Ʌ2Sf����9o�lz� �l���9o�|��-�X4n��9o�L���)R`�=��yCf�����;a)d�2_�ԡ���gwSȜ�\ЉKq�/pr��7���E&��oO���9����U�@LsZ�@;�l���{
�ӺW۪ k���)bN�H����(�3�/�4L�,����ի߼�M�J޹�O�z�9o��Ol�s��<�3��r��˔]_�`�4�9o�|ɮ�z�@��sʁ0d�s|�"�Q)`�0_�� #���oҤ�9o�l�ÃC���eL
��̔�P����M��7`�wx���43��fX��捘)K|��[�������n����X�dr�F̐�]Ӿ�\|*E�y#fs�X<�(���SȜ7d�>ubj����"����M�z�t��<�M��1�������;��Ʌ3�~|�`��'!1�y�f[\�qo����^�oޘ�*mW��D{-4%n�ٵ�:_��,��{W7f6�BǊ	�\J��yc�LQ9}r'��ycfӶ�q��8���%��Ͷ?X�Zcq���A���1|��*i��9������6t㌹>��͒�g5�/�v��g��ɜ�T�<Z#�\�/h�d��K�oz���-����i��ـ����]_�,�[�����'�T��fȮ����wn�B��͒]/kO�_�p@4����iǛ�i��%h�/h�LQ9uL���\)h�us���6�s��9W�e�T���F�D\��9�ͅ�yG��G���J���i7��-4/����7Fs}�0���Q���k��9�n2uk��1�i.�o��������	�����v���>��"���fS�>Ŷ��T[
���.������E�)X��,o2�	;��Ϣl�־M����f�.��x��]v탠�X��i*Է	W�;�͞q�������	�g�;?���WK]�f��hh@��],Asֹ˦N��U��`�/As�o�:��(e��X���a�?h�?�u�/�,�.��9����S��UL�%h�?h��]_��ؔ]�����o��,Eك2p	����/ڴ�n�:�1?K_��?d����u1��e����CfN���u��.���h����U���� �������=3j      �      x������ � �      �      x������ � �            x������ � �            x������ � �     
<main class='festival_page'>
	<header>
		<a class='home_link' href="/"><img src="<?=FRONT_ASSETS?>img/home.png"></a>
		<p class='nielsen_text'>NIELSEN<br>MUSIC</p>
		<img class='banner' src="<?=FRONT_ASSETS?>img/banner.png">
	</header>
	<h2>TAP EACH ICON FOR A FESTIVAL FACT</h2>
	<div class='float_choices'>
		<img src="<?=FRONT_ASSETS?>img/audience_blue.png">
			<div class='popup'>
				<p class='close'>X</p>
				<img src="<?=FRONT_ASSETS?>img/audience_blue.png">
				<p>Almost half of festival-goers are repeat attendees - they say they tend to go to the same music festivals every year</p>
			</div>

		<img src="<?=FRONT_ASSETS?>img/cell_phone.png">
			<div class='popup'>
				<p class='close'>X</p>
				<img src="<?=FRONT_ASSETS?>img/cell_phone.png">
				<p>Top 3 most popular smartphone engagements while at a concert or live music event</p>
				<ul>
					<li> <strong>33%</strong> of festival goers share photos on social media</li>
					<li> <strong>28%</strong> of festival goers share videos on social media</li>
					<li> <strong>21%</strong> Write status updates or posts about the concert on social media</li>
				</ul>
			</div>

		<img src="<?=FRONT_ASSETS?>img/heart.png">
			<div class='popup'>
				<p class='close'>X</p>
				<img src="<?=FRONT_ASSETS?>img/heart.png">
				<p>Top 3 most popular brand activations:</p>
				<ul>
					<li> Free giveaways</li>
					<li> Freebies that meet practical needs</li>
					<li> Free access to music</li>
				</ul>
			</div>

		<img src="<?=FRONT_ASSETS?>img/music_note.png">
			<div class='popup'>
				<p class='close'>X</p>
				<img src="<?=FRONT_ASSETS?>img/music_note.png">
				<p><strong>11%</strong> of festival-goers use their smartphone to buy new music that they hear at a concert</p>
			</div>

		<img src="<?=FRONT_ASSETS?>img/hear_blue.png">
			<div class='popup'>
				<p class='close'>X</p>
				<img src="<?=FRONT_ASSETS?>img/hear_blue.png">
				<p><strong>47%</strong> of festival goers typically attend music festivals for multiple days in a row</p>
			</div>

		<img src="<?=FRONT_ASSETS?>img/laptop.png">
			<div class='popup'>
				<p class='close'>X</p>
				<img src="<?=FRONT_ASSETS?>img/laptop.png">
				<p>Ranking of factors taken into consideration when choosing a music festival to attend</p>
				<ul>
					<li> Lineup</li>
					<li> Headliner</li>
					<li> Proximity to school or home</li>
				</ul>
			</div>

		<img src="<?=FRONT_ASSETS?>img/media.png">
			<div class='popup'>
				<p class='close'>X</p>
				<img src="<?=FRONT_ASSETS?>img/media.png">
				<p><strong>58%</strong> of people who attend a music festival follow the event on social media</p>
			</div>
	</div>
</main>

<script type="text/javascript">
	$(document).ready(function(){
		$('.float_choices img').fadeIn(4000);
		$('.float_choices img').css('margin-top', '0');

		$('.float_choices img').click(function(){
			$(this).next('.popup').fadeIn();
			$(this).next('.popup').css('display', 'flex');
		});

		$('.popup').click(function(){
			$('.popup').fadeOut();
		});

		setTimeout(function(){
			$('.festival_page h2').fadeIn();
		}, 5000);
	});
</script>

	
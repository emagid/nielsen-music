<a href='/' class="button click_action back_home info_home">HOME<img src="<?=FRONT_ASSETS?>img/back_grey.png"></a>  
<img class='logo' src="<?=FRONT_ASSETS?>img/logo.png">
<section class="future_of_sport">
	
	<section class="info">
			<img id='scroll' src="<?=FRONT_ASSETS?>img/back_grey.png">

			<!-- heading text -->
			<div class="info_header">
		       <h1>NIELSEN SPORTS CONNECT</h1>
		       <h2>THE LEADING GLOBAL MEASUREMENT SOLUTION SHAPING THE FUTURE OF SPORTS SPONSORSHIP</h2>
		               <p><b>The answer to unified measurement is here.</b></span> Sports Connect delivers the data you need to help inform better business decisions in a best-in-class, user-friendly platform.  Covering metrics across media types, this is the first step in bringing all Nielsen Sports data sets from Media, Consumer Insights and Market Intelligence into a single platform. </p>
			</div>    

			<!-- laptop gif -->
			<div class="image_section">
				<img src="<?=FRONT_ASSETS?>img/screen.gif">
			</div>
	</section>

			<!-- icon section -->
			<div class="icon_section">
				<h2>WHAT WE MEASURE</h2>
				<div class="icons_holder">
					<div class="icon">
						<div class="icon_circle"><img src="<?=FRONT_ASSETS?>img/tv.png"></div>
						<h3>TV</h3>
					</div>
					<div class="icon">
						<div class="icon_circle"><img src="<?=FRONT_ASSETS?>img/online.png"></div>
						<h3>ONLINE</h3>
					</div>
					<div class="icon">
						<div class="icon_circle"><img src="<?=FRONT_ASSETS?>img/social.png"></div>
						<h3>SOCIAL</h3>
					</div>
					<div class="icon">
						<div class="icon_circle"><img src="<?=FRONT_ASSETS?>img/print.png"></div>
						<h3>PRINT</h3>
					</div>
				</div>
			</div>


			<!-- One Methodology section -->
			<div class="methodology_section">
		        <h2>ONE METHODOLOGY. ONE PLATFORM. TOTAL VALUE.</h2>
		        <div class="methodology_holder">
		        	<div class="method">
		        	<img src="<?=FRONT_ASSETS?>img/IPAD_1.png">
		        		<h4>HOLISTIC VALUATION</h4>
		        		<ul>
		        			<li>Combine metrics across media types and markets for unified measurement.</li>
		        			<li>Customizable dashboards for media valuation, media monitoring and social valuation.</li>
		        		</ul>
		        	</div>
		        	<div class="method">
		        	<img src="<?=FRONT_ASSETS?>img/IPAD_3.png">
		        		<h4>COMPLILE AND SHARE</h4>
		        		<ul>
		        			<li>Build, save, and share custom reports directly in the platform.</li>
		        			<li>Customize filters and data visualization.</li>
		        		</ul>
		        	</div>
		        	<div class="method">
		        	<img src="<?=FRONT_ASSETS?>img/IPAD_5.png">
		        		<h4>COMPETITIVE RESEARCH</h4>
		        		<ul>
		        			<li>Search brand activation visuals across TV and social.</li>
		        			<li>Social campaign tagging functionality with ability to extract visuals.</li>
		        		</ul>
		        	</div>
		        </div>
			</div>


			<!-- Power section -->
			<div class="power">
				<h2>THE POWER OF NIELSEN SPORTS</h2>
				<h5>Comprehensive</h5>
				<p>The leading source of data and insights around the world.</p>

				<h5>Committed</h5>
				<p>Unrivaled expertise in a rapidly changing marketplace.</p>

				<h5>Cutting Edge</h5>
				<p>Innovative technology and groundbreaking research.</p>

				<h5>Connected</h5>
				<p>The global standard for sponsorship valuation across media platforms.</p>
		        <img src="<?=FRONT_ASSETS?>img/IPAD_2.png">
			</div>


			<!-- Contact section -->
			<div class="footer">
				<h3>Tired of fragmented measurement? Learn more about the power of Sports Connect.</h3>
				<a href="contact" class="button click_action">CONTACT</a>
			</div>
</section>

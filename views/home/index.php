<main>
	<header>
		<!-- <a class='home_link' href="/"><img src="<?=FRONT_ASSETS?>img/home.png"></a> -->
		<p class='nielsen_text'>NIELSEN<br>MUSIC</p>
		<img class='banner' src="<?=FRONT_ASSETS?>img/banner.png">
	</header>

	<h1>NIELSEN MUSIC</h1>

	<section class="home">
	  <div class="choices">
	    <a href='/trivia' id='trivia' class="choice click_action">
	      <img src="<?=FRONT_ASSETS?>img/trivia.png">
	      <h2>TRIVIA</h2>
	    </a>
	    <a href='/connect' id='stadium' class="choice click_action">
	      <img src="<?=FRONT_ASSETS?>img/connect.png">
	      <h2>EXPLORE MUSIC CONNECT</h2>
	    </a>
	    <a href="/global" id='info' class="choice click_action">
	      <img src="<?=FRONT_ASSETS?>img/global.png">
	      <h2>NIELSEN MUSIC EXPANDS GLOBALLY</h2>
	    </a>
	    <a href='/festival' id='contact' class="choice click_action">
	      <img src="<?=FRONT_ASSETS?>img/festival.png">
	      <h2>FESTIVAL FACTS</h2>
	    </a>
	    <a href='/contact' id='contact' class="choice click_action">
	      <img src="<?=FRONT_ASSETS?>img/phone.png">
	      <h2>CONTACT</h2>
	    </a>
	  </div>
	</section>
</main>

<script type="text/javascript">
	// opening animations
    $('main h1').fadeIn();

    setTimeout(function(){
        $('main h1').fadeOut(1500);
        $('main').css('background-position', 'center 1180px');
        $('header').fadeIn(1500);
    }, 1500);

    setTimeout(function(){
        $('.choices').fadeIn(2000);
        $('.choices').css('display', 'flex');
        $('.choices').css('margin-top', '257px');
    }, 3000);
</script>
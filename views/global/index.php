<main class='global_page'>
	<header>
		<a class='home_link' href="/"><img src="<?=FRONT_ASSETS?>img/home_w.png"></a>
		<p class='nielsen_text'>NIELSEN<br>MUSIC</p>
		<img class='banner' src="<?=FRONT_ASSETS?>img/banner.png">
	</header>

	<img class='globe' src="<?=FRONT_ASSETS?>img/globe.png">
	<p class='opening'>The Nielsen Music 360 reports include the industry's most important fan data & insights</p>

	<section class='content step2'>
		<h1>NIELSEN MUSIC 360 REPORTS</h1>
		<div class='map_holder map_holder1'>
			<p class='smallhead'>2017</p>
			<img class='map' src="<?=FRONT_ASSETS?>img/map.png">
			<img src="<?=FRONT_ASSETS?>img/canada.png">
			<img src="<?=FRONT_ASSETS?>img/us.png">
			<img src="<?=FRONT_ASSETS?>img/mexico.png">
			<img src="<?=FRONT_ASSETS?>img/brazil.png">
			<img src="<?=FRONT_ASSETS?>img/argentina.png">
			<img src="<?=FRONT_ASSETS?>img/us.png">
			<img src="<?=FRONT_ASSETS?>img/skorea.png">
			<img src="<?=FRONT_ASSETS?>img/japan.png">
			<img src="<?=FRONT_ASSETS?>img/taiwan.png">
			<img src="<?=FRONT_ASSETS?>img/australia.png">
			<img class='countries' src="<?=FRONT_ASSETS?>img/countries.png">
		</div>
		<div class='map_holder map_holder2'>
			<p class='smallhead'>2018</p>
			<img class='map' src="<?=FRONT_ASSETS?>img/map2.png">
			<img src="<?=FRONT_ASSETS?>img/uk.png">
			<img src="<?=FRONT_ASSETS?>img/germany.png">
			<img src="<?=FRONT_ASSETS?>img/china.png">
			<img src="<?=FRONT_ASSETS?>img/india.png">
			<img class='countries' src="<?=FRONT_ASSETS?>img/countries2.png">
		</div>
		<div class='map_holder map_holder3'>
			<p class='smallhead'>AND BEYOND</p>
			<img class='beyond' src="<?=FRONT_ASSETS?>img/globe_grey.png">
			<img class='beyond' src="<?=FRONT_ASSETS?>img/globe_blue.png">
		</div>
	</section>

	<div class='popup'>
		<p class='close'><img src="<?=FRONT_ASSETS?>img/next.png"></p>
		<div class='text'>
			<p>Through the report, you can understand:</p>
			<li><img src="<?=FRONT_ASSETS?>img/hear.png">Music listening attitudes</li>
			<li><img src="<?=FRONT_ASSETS?>img/price.png">Time and money spent on music</li>
			<li><img src="<?=FRONT_ASSETS?>img/audience.png">Streaming behaviors</li>
			<li><img src="<?=FRONT_ASSETS?>img/festival.png">Live music event attendance</li>
			<li><img src="<?=FRONT_ASSETS?>img/stadium.png">Fan behaviors</li>
			<li><img src="<?=FRONT_ASSETS?>img/billboard.png">Sponsorship valuation solutions</li>
		</div>
	</div>

<script type="text/javascript">
	$(document).ready(function(){

		setTimeout(function() {
			$('.globe').fadeIn(2000);
			$('.globe').css('margin-top', '0px');
		}, 1000);

		var clicked = false

		function animate() {
			clicked = true;
			$('.globe').fadeOut();
			$('.opening').fadeOut();
			$('.popup').fadeIn(1000);
			$('.popup').css('display', 'flex');
			setTimeout(function(){
				$('.home_link img').attr('src', '<?=FRONT_ASSETS?>img/home.png');
				$('.global_page .nielsen_text, .global_page .banner').fadeIn();
				$('.map').fadeIn(1000);
			}, 1000);


		$('.close').click(function(){
			$('.popup').fadeOut(1000);
			$('.step2').fadeIn(1000);

			setTimeout(function(){
				$('.home_link img').attr('src', '<?=FRONT_ASSETS?>img/home.png');
				$('.global_page .nielsen_text, .global_page .banner').fadeIn();
				$('.map').fadeIn(1000);
			}, 1000);

			setTimeout(function(){
				$('.map_holder1 img:nth-child(2)').delay(200).fadeIn(500);
				$('.map_holder1 img:nth-child(3)').delay(400).fadeIn(500);
				$('.map_holder1 img:nth-child(4)').delay(600).fadeIn(500);
				$('.map_holder1 img:nth-child(5)').delay(800).fadeIn(500);
				$('.map_holder1 img:nth-child(6)').delay(1000).fadeIn(500);
				$('.map_holder1 img:nth-child(7)').delay(1200).fadeIn(500);
				$('.map_holder1 img:nth-child(8)').delay(1400).fadeIn(500);
				$('.map_holder1 img:nth-child(9)').delay(1600).fadeIn(500);
				$('.map_holder1 img:nth-child(10)').delay(1800).fadeIn(500);
				$('.map_holder1 img:nth-child(11)').delay(2000).fadeIn(500);
				$('.map_holder1 img:nth-child(12)').delay(2200).fadeIn(500);
				$('.map_holder1 .countries').delay(3000).fadeIn(500);
				$('.map_holder1').delay(6000).fadeOut(500);
			}, 2000);

			setTimeout(function(){
				$('.map_holder2').fadeIn(1000);
				$('.map_holder2 img:nth-child(3)').delay(1200).fadeIn(500);
				$('.map_holder2 img:nth-child(4)').delay(1400).fadeIn(500);
				$('.map_holder2 img:nth-child(5)').delay(1600).fadeIn(500);
				$('.map_holder2 img:nth-child(6)').delay(1800).fadeIn(500);
				$('.map_holder2 .countries').delay(2600).fadeIn(500);
			}, 9000);

			setTimeout(function(){
				$('.map_holder2 .smallhead').css('opacity', '0');
				$('.map_holder3').fadeIn(1000);
				$('.map_holder3').css('display', 'flex');
				$('.map_holder3 img:nth-child(2)').delay(1200).fadeIn(500);
				$('.map_holder3 img:nth-child(3)').delay(2700).fadeIn(2000);
				setTimeout(function(){
					$('.beyond').css({"transform": "rotateY(180deg) 1s",
"-ms-transform": "rotateY(180deg)", "-webkit-transform": "rotateY(180deg)"});
					$('.white').fadeIn();
					setTimeout(function(){
						window.location.replace("/");
					}, 500);
				},10000);
			}, 15000);

			setTimeout(function(){
				$('.map_holder3 img.beyond').css({"transform": "rotateY(-180deg)",
"-ms-transform": "rotateY(-180deg)", "-webkit-transform": "rotateY(-180deg)"});
			}, 18000 );
		});

		};

		$('.globe').click(function(){
			animate();
			$('.globe').css({"transform": "rotateY(-180deg) 2s",
"-ms-transform": "rotateY(-180deg)", "-webkit-transform": "rotateY(-180deg)"});
		});

		setTimeout(function(){
			if (!clicked) {
				animate();
				$('.globe').css({"transform": "rotateY(-180deg) 2s",
"-ms-transform": "rotateY(-180deg)", "-webkit-transform": "rotateY(-180deg)"});
			}
		}, 10000);

		$('.beyond').click(function(){
			$('.white').fadeIn();
			$('.beyond').css({"transform": "rotateY(180deg) 1s",
"-ms-transform": "rotateY(180deg)", "-webkit-transform": "rotateY(180deg)"});
			setTimeout(function(){
				window.location.replace("/");
				$('.beyond').css({"transform": "rotateY(180deg) 1s",
"-ms-transform": "rotateY(180deg)", "-webkit-transform": "rotateY(180deg)"});
			}, 500);
		});


	});
</script>
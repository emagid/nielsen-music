<main class="panoramic">
    <img class='logo' src="<?=FRONT_ASSETS?>img/logo.png">
    <div class="panoramic_holder">
      <section class="panoramic_choices">
        <a href='/' class="button click_action back_home">HOME<img src="<?=FRONT_ASSETS?>img/back.png"></a>  
        <div class="title_holder stadium">
          <h1>STADIUM VIEW</h1>
          <p>CHOOSE A SPORT TO LEARN MORE</p>
        </div>  
      </section>

      <section class="choices">
        <div id='soccer' class="choice click_action">
          <h2>FOOTBALL</h2>
          <img src="<?=FRONT_ASSETS?>img/soccer.png">
        </div>
        <div id='rugby' class="choice click_action">
          <h2>RUGBY</h2>
          <img src="<?=FRONT_ASSETS?>img/rugby.png">
        </div>
      </section>
    </div>

    <section class="soccer">
      <div class="button click_action back_sports">BACK<img src="<?=FRONT_ASSETS?>img/back.png"></div>
        <div class="title_holder">
          <h1>FOOTBALL</h1>
          <p class="button">TAP THE ICONS TO LEARN MORE</p>
        </div>
        <div class="click_points">
          <div id='soccer_jersey' class="locator_box click_action">
            <img src="<?=FRONT_ASSETS?>img/jersey_bub.svg">
          </div>
          <div id='soccer_bench' class="locator_box click_action">
            <img src="<?=FRONT_ASSETS?>img/bench.svg">
          </div>
          <div id='audience3' class="locator_box click_action">
            <img src="<?=FRONT_ASSETS?>img/fans_left.svg">
          </div>
          <div id='audience4' class="locator_box click_action">
            <img src="<?=FRONT_ASSETS?>img/fans.svg">
          </div>
          <div id='soccer_banner' class="locator_box click_action">
            <img src="<?=FRONT_ASSETS?>img/LED.svg">
          </div>
          <div id='scoreboard' class="locator_box click_action">
            <img src="<?=FRONT_ASSETS?>img/soccer_billboard.svg">
          </div>
        </div>
<!--         <a class="button rugby_link"><img src="<?=FRONT_ASSETS?>img/rugby_white_tag.png"><img src="<?=FRONT_ASSETS?>img/back.png"></a>
 -->    </section>

    <section class="rugby">
      <div class="button click_action back_sports">BACK<img src="<?=FRONT_ASSETS?>img/back.png"></div>
        <div class="title_holder">
          <h1>RUGBY</h1>
          <p class="button">TAP THE ICONS TO LEARN MORE</p>
        </div>
        <div class="click_points">
            <div id='interview' class="locator_box click_action">
              <img src="<?=FRONT_ASSETS?>img/interview.svg">
            </div>
            <div id='audience1' class="locator_box click_action">
              <img src="<?=FRONT_ASSETS?>img/fans.svg">
            </div>
            <div id='audience2' class="locator_box click_action">
              <img src="<?=FRONT_ASSETS?>img/fans_left.svg">
            </div>
            <div id='ref' class="locator_box click_action">
              <img src="<?=FRONT_ASSETS?>img/ref.svg">
            </div>
            <div id='jersey' class="locator_box click_action">
              <img src="<?=FRONT_ASSETS?>img/jersey_bub.svg">
            </div>
            <div id='adds' class="locator_box click_action">
              <img src="<?=FRONT_ASSETS?>img/billboard.svg">
            </div>
          <!-- <div id='interview' class="locator_box click_action">
            <div class="locator"></div>
          </div>
          <div id='audience1' class="locator_box click_action">
            <div class="locator"></div>
          </div>
          <div id='audience2' class="locator_box click_action">
            <div class="locator"></div>
          </div>
          <div id='ref' class="locator_box click_action">
            <div class="locator"></div>
          </div>
          <div id='jersey' class="locator_box click_action">
            <div class="locator"></div>
          </div>
          <div id='adds' class="locator_box click_action">
            <div class="locator"></div>
          </div> -->
        </div>
<!--         <a class="button soccer_link"><img src="<?=FRONT_ASSETS?>img/soccer_white.png"><img src="<?=FRONT_ASSETS?>img/back.png"></a>
 -->    </section>

      <!-- RUGBY info boxes -->
      <div id='jersey_fact' class="rugby_overlay overlay">
        <div class="x_box">
          <span class="x">X</span>
        </div>
        <p>During an average UK rugby league match, the front-of-jersey sponsor position generates <span class='blue_text'>8min 30sec</span> of brand exposure.</p>
      </div>

      <div id='adds_fact' class="rugby_overlay overlay">
        <div class="x_box">
          <span class="x">X</span>
        </div>
        <p>Field level static boards receive <span class='blue_text'>2x</span> more exposure than goal post pads in the average UK rugby league match.</p>
      </div>

      <div id='ref_fact' class="rugby_overlay overlay">
        <div class="x_box">
          <span class="x">X</span>
        </div>
        <p>Referee branding generates over <span class='blue_text'>1min 30sec</span> of exposure per game in the average UK rugby league match.</p>
      </div>

      <div id='interview_fact' class="rugby_overlay overlay">
        <div class="x_box">
          <span class="x">X</span>
        </div>
        <p>The <span class='blue_text'>interview backdrop</span> generates the highest cumulative media value of all sponsor locations within UK rugby league matches.</p>
      </div>

      <div id='audience1_fact' class="rugby_overlay overlay">
        <div class="x_box">
          <span class="x">X</span>
        </div>
        <p>There are <span class='blue_text'>14.3M</span> rugby league fans in the UK with an average age of 47 and average household income of £34k.</p>
      </div>

      <div id='audience2_fact' class="rugby_overlay overlay">
        <div class="x_box">
          <span class="x">X</span>
        </div>
        <p><span class='blue_text'>45%</span> of UK rugby league fans are more likely to buy products of brands that are sponsoring the sport.</p>
      </div>


      <!-- SOCCER info boxes -->
      <div id='soccer_jersey_fact' class="rugby_overlay overlay">
        <div class="x_box">
          <span class="x">X</span>
        </div>
        <p>On average, the front-of-jersey sponsor generated <span class='blue_text'>£13.1M</span> per team in media value in the EPL in the 2016-17 season.</p>
      </div>

      <div id='soccer_bench_fact' class="rugby_overlay overlay">
        <div class="x_box">
          <span class="x">X</span>
        </div>
        <p>The bench area, including player seatbacks, generated <span class='blue_text'>£16.7M</span> in sponsor media value in the 2016-17 EPL season.</p>
      </div>

      <div id='audience3_fact' class="rugby_overlay overlay">
        <div class="x_box">
          <span class="x">X</span>
        </div>
        <p>Instagram engagements increased by <span class='blue_text'>55%</span> year-over-year for the clubs in Europe's five largest football leagues between 1st June - 1st September in 2017.</p>
      </div>

      <div id='audience4_fact' class="rugby_overlay overlay">
        <div class="x_box">
          <span class="x">X</span>
        </div>
        <p>Globally, there are over <span class='blue_text'>900 million</span> football fans.</p>
      </div>

      <div id='soccer_banner_fact' class="rugby_overlay overlay">
        <div class="x_box">
          <span class="x">X</span>
        </div>
        <p>LED pitchside boards generate the most value for EPL clubs, the average sponsor garnered <span class='blue_text'>1min 14sec</span> of exposure per average live home match in the 2016-17 season.</p>
      </div>

      <div id='scoreboard_fact' class="rugby_overlay overlay">
        <div class="x_box">
          <span class="x">X</span>
        </div>
        <p>The static highboards across all EPL matches generated over <span class='blue_text'>£574M</span> in sponsor media value in the 2016-17 season.</p>
      </div>

  </main>
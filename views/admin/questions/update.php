  <div role="tabpanel">
    <ul class="nav nav-tabs" role="tablist">
      <li role="presentation" class="active"><a href="#general" aria-controls="general" role="tab" data-toggle="tab">General</a></li>
      <li role="presentation"><a href="#answers" aria-controls="answers" role="tab" data-toggle="tab">Answers</a></li>

    </ul>
    
    <div class="tab-content">
      <div role="tabpanel" class="tab-pane active" id="general">

        <div class="row">
            <div class="col-md-24">
                <div class="box">
                    <form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
                        <input type="hidden" name="id" value="<?php echo $model->question->id; ?>" />
                        <h4>General</h4>
                        <div class="form-group">
                            <label>Question</label>
                            <?php echo $model->form->editorFor("text"); ?>
                        </div>
                        <div class="form-group">
                            <label>Source of Question</label>
                            <?php echo $model->form->editorFor("source"); ?>
                        </div>
                        <div class="form-group">
                            <label>Display Order</label>
                            <input name="display_order" type="number" value="<?=$model->question->display_order?>">
                        </div>
                        <div class="form-group">
                            <label>Correct Answer</label>
                            <? if(count($model->answers) > 0){ ?>
                                <?php echo $model->form->dropDownListFor("correct_answer_id",$model->c_answers); ?>
                            <? } else {?>
                                <p>Add answers in the answer tab to set a correct answer.</p>
                            <? } ?>
                        </div>
                        <div class="form-group">
                            <label>Failure Text (displayed on wrong answer)</label>
                            <?php echo $model->form->editorFor("failure_text"); ?>
                        </div>

                        <div class="form-group">
                            <label>Image</label>

                            <p>
                                <small>(ideal featured image size is ___ x ___)</small>
                            </p>
                            <p><input type="file" name="image" class='image'/></p>

                            <div style="display:inline-block">
                                <?php
                                $img_path = "";
                                if ($model->question->image != "" && file_exists(UPLOAD_PATH . 'questions' . DS . $model->question->image)) {
                                    $img_path = UPLOAD_URL . 'questions/' . $model->question->image;
                                    ?>
                                    <div class="well well-sm pull-left">
                                        <img src="<?php echo $img_path; ?>" width="100"/>
                                        <br/>
                                        <a href="<?= ADMIN_URL . 'questions/delete_image/' . $model->question->id; ?>"
                                           onclick="return confirm('Are you sure?');"
                                           class="btn btn-default btn-xs">Delete</a>
                                        <input type="hidden" name="image"
                                               value="<?= $model->question->image ?>"/>
                                    </div>
                                <?php } ?>
                                <div class='preview-container'></div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-save">Save</button>
                    </form>
                </div>
            </div>
        </div>
      </div>
      <div role="tabpanel" class="tab-pane" id="answers">
          <div class="col-md-24">
              <div class="box">
                  <h4>Answers</h4>
                      <table class="table">
                          <thead>
                              <tr>
                                  <th width="25%">Answer</th>
                                  <th width="15%">Image</th>
                                  <th width="15%" class="text-center">Edit</th>
                                  <th width="15%" class="text-center">Delete</th>
                              </tr>
                          </thead>
                          <tbody>
                              <? foreach ($model->answers as $answer) { ?>
                                  <tr id="answer<?=$answer->id?>">
                                    <td><?=$answer->text?></td>
                                    <td>
                                        <? if($answer->featured_image != ''){?>
                                            <img src="<?php echo UPLOAD_URL."answers/".$answer->featured_image; ?>" width="50"/>
                                        <?}?>
                                    </td>
                                    <td>
                                        <a class="btn-actions edit_answer" data-answer="<?= $answer->id ?>" href="">
                                            <i class="icon-pencil"></i>
                                       </a>
                                    </td>
                                    <td>
                                        <a class="btn-actions del_answer" data-answer="<?= $answer->id ?>" href="">
                                            <i class="icon-cancel-circled"></i>
                                       </a>
                                    </td>
                                  </tr>
                                  <tr id="edit_answer<?=$answer->id?>" style="display: none">
                                      <form action="/admin/questions/update_answer" method="post" enctype="multipart/form-data">
                                          <input name="id" type="hidden" value="<?=$answer->id?>">
                                          <input name="question_id" type="hidden" value="<?=$model->question->id?>">
                                          <td colspan="1">
                                              <label>Text</label>
                                              <input type="text" name="text" placeholder="Answer" value="<?=$answer->text?>">
                                          </td>
                                          <td>
                                              <p>
                                                  <small>(ideal featured image size is ___ x ___)</small>
                                              </p>
                                              <p><input type="file" name="featured_image" class='image'/></p>

                                              <div style="display:inline-block">
                                                  <?php
                                                  $img_path = "";
                                                  if ($answer->featured_image != "" && file_exists(UPLOAD_PATH . 'answers' . DS . $answer->featured_image)) {
                                                      $img_path = UPLOAD_URL . 'answers/' . $answer->featured_image;
                                                      ?>
                                                      <div id="fi<?=$answer->id?>" class="well well-sm pull-left">
                                                          <img src="<?php echo $img_path; ?>" width="100"/>
                                                          <br/>
                                                          <a onclick="if(confirm('Are you sure?')){$('#fi<?=$answer->id?>').remove();};"
                                                             class="btn btn-default btn-xs">Delete</a>
                                                          <input type="hidden" name="featured_image"
                                                                 value="<?= $answer->featured_image ?>"/>
                                                      </div>
                                                  <?php } ?>
                                                  <div class='preview-container'></div>
                                              </div>
                                          </td>
                                          <td colspan="2">
                                              <button type="submit" class="btn btn-save">Save</button>
                                          </td>
                                      </form>
                                  </tr>
                              <? } ?>
                              <tr>
                                  <form action="/admin/questions/update_answer" method="post" enctype="multipart/form-data">
                                      <input name="question_id" type="hidden" value="<?=$model->question->id?>">
                                      <td >
                                          <label>New Answer Option</label>
                                          <input type="text" name="text" placeholder="Answer">
                                      </td>
                                      <td>
                                          <label>Answer Image</label>
                                          <p>
                                              <small>(ideal featured image size is ___ x ___)</small>
                                          </p>

                                          <div style="display:inline-block">
                                              <p><input type="file" name="featured_image" class='image'/></p>
                                              <div class='preview-container'></div>
                                          </div>
                                      </td>
                                      <td colspan="2">
                                          <button type="submit" class="btn btn-save">Save</button>
                                      </td>
                                  </form>
                              </tr>
                          </tbody>
                      </table>

              </div>
          </div>
      </div>
    
    </div>
  </div>

<?php echo footer(); ?>
<script type='text/javascript'>
    $(document).ready(function() {
        $(".edit_answer").click( function(e) {
            e.preventDefault();
            $("#answer" + $(this).data('answer')).toggle();
            $("#edit_answer" + $(this).data('answer')).toggle();
        });
        $(".del_answer").click( function(e) {
            e.preventDefault();
            $("#answer"+$(this).data('answer')).remove();
            $.post('/admin/questions/delete_answer', {id: $(this).data('answer')}, function (response) {
                console.log(response);
            });
        });

    });
</script>
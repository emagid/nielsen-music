<?php
 if(count($model->offices)>0): ?>
  <div class="box box-table">
    <table class="table">
      <thead>
        <tr>
           <th width="20%">Practice</th>
            <th width="20%">Office</th>
            <th width="20%">Email</th>
            <th width="20%">Phone Number</th>
            <th width="20%">Address</th>
            <th width="10%">City, State</th>
        </tr>
      </thead>
      <tbody>
      <?php if(false)/*foreach($model->ch_result as $obj)*/{ ?>
        <tr>
      <td>
         <a href="<?php echo ADMIN_URL; ?>offices/update/<?php echo $obj->O; ?>">
           <?php echo $obj->OFN;?>
         </a>
      </td>
      <td>
         <a href="<?php echo ADMIN_URL; ?>offices/update/<?php echo $obj->O; ?>">
           <?php echo $obj->EM;?>
         </a>
      </td>
      <td>
        <a href="<?php echo ADMIN_URL; ?>offices/update/<?php echo $obj->O; ?>">
           <?php echo $obj->PH;?>
        </a>
      </td>
        <td>
            <a href="<?php echo ADMIN_URL; ?>offices/update/<?php echo $obj->O; ?>">
                <?php echo $obj->AD." ".$obj->AD2;?>
            </a>
        </td>
        <td>
            <a href="<?php echo ADMIN_URL; ?>offices/update/<?php echo $obj->O; ?>">
                <?php echo $obj->CT.", ".$obj->ST;?>
            </a>
        </td>
       </tr>
       <?php } ?>
      <?php foreach($model->offices as $obj){ ?>
      <tr>
      <td>
         <a href="<?php echo ADMIN_URL; ?>offices/update/<?php echo $obj->id; ?>">
             <?php echo $obj->practice()->name;?>
         </a>
      </td>
      <td>
         <a href="<?php echo ADMIN_URL; ?>offices/update/<?php echo $obj->id; ?>">
           <?php echo $obj->label;?>
         </a>
      </td>
      <td>
         <a href="<?php echo ADMIN_URL; ?>offices/update/<?php echo $obj->id; ?>">
           <?php echo $obj->email;?>
         </a>
      </td>
      <td>
        <a href="<?php echo ADMIN_URL; ?>offices/update/<?php echo $obj->id; ?>">
           <?php echo $obj->phone;?>
        </a>
      </td>
        <td>
            <a href="<?php echo ADMIN_URL; ?>offices/update/<?php echo $obj->id; ?>">
                <?php echo $obj->address." ".$obj->address2;?>
            </a>
        </td>
        <td>
            <a href="<?php echo ADMIN_URL; ?>offices/update/<?php echo $obj->id; ?>">
                <?php echo $obj->city.", ".$obj->state;?>
            </a>
        </td>
       </tr>
       <?php } ?>
   </tbody>
 </table>
 <div class="box-footer clearfix">
  <div class='paginationContent'></div>
</div>
</div>
<?php endif; ?>

<?php echo footer(); ?>
 <script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'offices';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>
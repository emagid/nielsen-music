<?
function setDefaultValue($model, $inputName, $relations = null, $field = null)
{
    if(isset($_SESSION['checkoutPost']) && isset($_SESSION['checkoutPost'][$inputName])){
        return $_SESSION['checkoutPost'][$inputName];
    } elseif($user = $model->user){
        if($inputName == 'email'){
            return $user->email;
        }
        if($relations && $field && $user->$relations()){
            return $user->$relations()->$field;
        }
    } else {
        return "";
    }
}

$user = $model->user;
?>
<div class="content">
    <form method="post" id="activate-form" action="/account/activate">
        <div class="shipAddr" >
            <input type="hidden" name="id" value="<?=$user->id?>">
            <input type="hidden" name="wholesale_id" value="<?=$user->wholesale_id?>">
            <input type="hidden" name="email" value="<?=$user->email?>">
            <label for="username">Username:</label>
            <input type="text" id="username" name="username" required>
            <label for="password">Password<span>*</span></label>
            <input type="password" id="password" name="password" required/>
            <label for="confirm_password">Confirm Password<span>*</span></label>
            <input type="password" id="confirm_password" name="confirm_password" required/>
            <input type="submit" id="form-submit" name="form-submit" value="Save"/>
        </div>
    </form>
</div>

<script>
    $('#form-submit').on('click',function(e){
        e.preventDefault();
        var data = {
            username:$('#username').val(),
            password:$('#password').val(),
            confirm_password:$('#confirm_password').val(),
            'password,#confirm_password': $('#password').val() == $('#confirm_password').val()
        };
        var submit = true;
        $.each(data,function(key,value) {
            if (!value) {
                submit = false;
                $('#' + key).css('border', '1px solid red');
            } else if(value){
                $('#' + key).css('border', '1px solid #eaeaea');
            }
        });

        if(submit){
            $("#activate-form").submit();
        }

    });
</script>
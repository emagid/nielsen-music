<?
function setDefaultValue($model, $inputName, $relations = null, $field = null)
{
	if(isset($_SESSION['checkoutPost']) && isset($_SESSION['checkoutPost'][$inputName])){
		return $_SESSION['checkoutPost'][$inputName];
	} elseif($user = $model->user){
		if($inputName == 'email'){
			return $user->email;
		}
		if($relations && $field && $user->$relations()){
			return $user->$relations()->$field;
		}
	} else {
		return "";
	}
}
?>
<div class="content_wrapper_97">
	<div class="account_view_header_row">
		<div class="account_view_header_top">
			<div class="account_hero">
			</div>
			<h4>My Account</h4>
			<h2>Hello <?=$model->user->first_name?></h2>
			<p>View your order history, manage your shipping and billing addresses, and reflect on your wishlist.</p>
		</div>
		<div class="account_header_tab_row">
			<a id="account_tab_hearder_summary" class="account_header_tab tile_fade_on active_account_header_tab" data-account_header_tab_type="summary">
				<div class="account_header_tab_block">
				</div>
				<p class="account_header_tab_text">Summary</p>
			</a>
			<a id="account_tab_hearder_items" class="account_header_tab tile_fade_on" data-account_header_tab_type="items">
				<div class="account_header_tab_block">
				</div>
				<p class="account_header_tab_text">Items</p>
			</a>
			<a id="account_tab_hearder_orders" class="account_header_tab tile_fade_on" data-account_header_tab_type="orders">
				<div class="account_header_tab_block">
				</div>
				<p class="account_header_tab_text">Orders</p>
			</a>
			<a id="account_tab_hearder_settings" class="account_header_tab tile_fade_on" data-account_header_tab_type="password">
				<div class="account_header_tab_block">
				</div>
				<p class="account_header_tab_text">Settings</p>
			</a>
		</div>
	</div>
	<div class="account_tab_content_section">
		<div class="account_tab_content account_tab_content_active" style="display:block;" id="account_tab_content_summary">
			<div class="account_tab_content_tile">
				<div class="tab_content_tile_header">
					<p>Home Address</p>
					<a class="shipBtn">Edit</a>
				</div>
				<div class="tab_content_tile_data">

					<?php if($address = $model->user->address()){ ?>
						<div class="shipAddr">
							<p><?=$address->label?></p>
							<p><?=$address->first_name." ".$address->last_name?></p>
							<p><?=$address->address?></p>
							<p><?=$address->address2?></p>
							<p><?=$address->phone?></p>
							<p><?=$address->city?></p>
							<p><?=$address->state.' '.$address->zip?></p>
						</div>
					<form method="post" action="/account/shipping">
						<div class="shipAddr" style="display:none">
							<input type="text" name="id" style="width:100%; display:none" value="<?=$address->id?>">
							<input type="text" name="user_id" style="width:100%; display:none" value="<?=$address->user_id?>">
							<label for="label">Label: </label>
							<input type="text" id="label" name="label" style="width:100%" value="<?=$address->label?>">
							<label for="firstName">First Name:</label>
							<input type="text" id="firstName" name="first_name" style="width:100%" value="<?=$address->first_name?>">
							<label for="lastName">Last Name: </label>
							<input type="text" id="lastName" name="last_name" style="width:100%" value="<?=$address->last_name?>">
							<label for="address1">Address 1: </label>
							<input type="text" id="address1" name="address" style="width:100%" value="<?=$address->address?>">
							<label for="address2">Address 2: </label>
							<input type="text" id="address2" name="address2" style="width:100%" value="<?=$address->address2?>">
							<label for="address2">Phone: </label>
							<input type="text" id="phone" name="phone" style="width:100%" value="<?=$address->phone?>">
							<label for="city">City: </label>
							<input type="text" id="city" name="city" style="width:100%" value="<?=$address->city?>">
							<label for="state">State: </label>
							<select name="state" style="width:100%">
								<?php foreach (get_states() as $key=>$value) {?>
									<option value="<?=$key?>" <?if($key == $address->state){echo 'selected';}?>><?=$value?></option>
								<?}?>
							</select>
<!--							<input type="text"  id="state" name="state" style="width:100%" value="--><?//=$address->state?><!--">-->
							<label for="zip">Zip: </label>
							<input type="text" id="zip" name="zip" style="width:100%" value="<?=$address->zip?>">
							<input type="submit" name="submit" value="Save"/>
						</div>
					</form>
					<?php } else { ?>
						<p class="shipAddr">None<br/>Press Edit to add a new address</p>
						<form method="post" action="/account/shipping">
							<div class="shipAddr" style="display:none">
								<input type="text" name="user_id" style="width:100%; display:none" value="<?=$model->user->id?>">
								<label for="label">Label: </label>
								<input type="text" id="label" name="label" style="width:100%" >
								<label for="firstName">First Name:</label>
								<input type="text" id="firstName" name="first_name" style="width:100%">
								<label for="lastName">Last Name: </label>
								<input type="text" id="lastName" name="last_name" style="width:100%">
								<label for="address1">Address 1: </label>
								<input type="text" id="address1" name="address" style="width:100%"><br/>
								<label for="address2">Address 2: </label>
								<input type="text" id="address2" name="address2" style="width:100%">
								<label for="city">City: </label>
								<input type="text" id="city" name="city" style="width:100%">
								<label for="state">State: </label>
								<select name="state" style="width:100%">
									<?php foreach (get_states() as $key=>$value) {?>
										<option value="<?=$key?>"><?=$value?></option>
									<?}?>
								</select>
								<label for="zip">Zip: </label>
								<input type="text" id="zip" name="zip" style="width:100%">
								<input type="submit" name="submit" value="Save"/>
							</div>
						</form>
					<?php } ?>
				</div>
			</div>
			<div class="account_tab_content_tile">
				<div class="tab_content_tile_header">
					<p>Billing Address</p>
					<a class="billBtn">Edit</a>
				</div>
				<div class="tab_content_tile_data">
					<?php if($address = $model->user->payment_profile()){ ?>
						<div class="billAddr">
							<p><?=$address->label?></p>
							<p><?=$address->first_name.' '.$address->last_name?></p>
							<p><?=$address->address?></p>
							<p><?=$address->address2?></p>
							<p><?=$address->phone?></p>
							<p><?=$address->city?></p>
							<p><?=$address->state.' '.$address->zip?></p>
						</div>
						<form method="post" action="/account/billing">
							<div class="billAddr" style="display:none">
								<input type="text" name="id" style="width:100%; display:none" value="<?=$address->id?>">
								<input type="text" name="user_id" style="width:100%; display:none" value="<?=$address->user_id?>">
								<label for="label">Label: </label>
								<input type="text" id="label" name="label" style="width:100%" value="<?=$address->label?>">
								<label for="firstName">First Name:</label>
								<input type="text" id="firstName" name="first_name" style="width:100%" value="<?=$address->first_name?>">
								<label for="lastName">Last Name: </label>
								<input type="text" id="lastName" name="last_name" style="width:100%" value="<?=$address->last_name?>">
								<label for="address1">Address 1: </label>
								<input type="text" id="address1" name="address" style="width:100%" value="<?=$address->address?>">
								<label for="address2">Address 2: </label>
								<input type="text" id="address2" name="address2" style="width:100%" value="<?=$address->address2?>">
								<label for="address2">Phone: </label>
								<input type="text" id="phone" name="phone" style="width:100%" value="<?=$address->phone?>">
								<label for="city">City: </label>
								<input type="text" id="city" name="city" style="width:100%" value="<?=$address->city?>">
								<label for="state">State: </label>
								<select name="state" style="width:100%">
									<?php foreach (get_states() as $key=>$value) {?>
										<option value="<?=$key?>" <?if($key == $address->state){echo 'selected';}?>><?=$value?></option>
									<?}?>
								</select>
								<label for="zip">Zip: </label>
								<input type="text" id="zip" name="zip" style="width:100%" value="<?=$address->zip?>">
								<input type="submit" name="submit" value="Save"/>
							</div>
						</form>
							<?php } else { ?>
						<p class="billAddr">None<br/>Press Edit to add a new address</p>
						<form method="post" action="/account/billing">
							<div class="billAddr" style="display:none">
								<input type="text" name="user_id" style="width:100%; display:none" value="<?=$model->user->id?>">
								<label for="label">Label: </label>
								<input type="text" id="label" name="label" style="width:100%" >
								<label for="firstName">First Name:</label>
								<input type="text" id="firstName" name="first_name" style="width:100%">
								<label for="lastName">Last Name: </label>
								<input type="text" id="lastName" name="last_name" style="width:100%">
								<label for="address1">Address 1: </label>
								<input type="text" id="address1" name="address" style="width:100%"><br/>
								<label for="address2">Address 2: </label>
								<input type="text" id="address2" name="address2" style="width:100%">
								<label for="city">City: </label>
								<input type="text" id="city" name="city" style="width:100%">
								<label for="state">State: </label>
								<select name="state" style="width:100%">
									<?php foreach (get_states() as $key=>$value) {?>
										<option value="<?=$key?>"><?=$value?></option>
									<?}?>
								</select>
								<label for="zip">Zip: </label>
								<input type="text" id="zip" name="zip" style="width:100%">
								<input type="submit" name="submit" value="Save"/>
							</div>
						</form>
							<?php } ?>
				</div>
			</div>
		</div>
		<div class="account_tab_content" id="account_tab_content_items" style="display:none;">
			<h2>Your Products</h2>
			<? print_r($model->wholesale_items) ?>
			<? print_r($model->user) ?>
			<ul class = "products_order_summary items">
				<?php if($wholesale_items = $model->wholesale_items){
					foreach($wholesale_items as $product){ ?>
						<li>
							<a class = "remove_product remove_product_wishlist" data-wishlist_id = "<?=$wishlist->id?>">
								<svg class="modal_svg" xmlns="http://www.w3.org/2000/svg" version="1.1" width="32" height="32" viewBox="0 0 32 32">
									<path class="modal_svg_path" d="M31.158 32.118c-.246 0-.492-.093-.678-.282L.163 1.52c-.375-.375-.375-.983 0-1.358s.983-.375 1.358 0l30.317 30.316c.375.375.375.983 0 1.358-.187.188-.433.282-.678.282zM.842 32.118c-.246 0-.492-.093-.678-.282-.375-.375-.375-.983 0-1.358L30.48.162c.375-.375.983-.375 1.358 0s.375.983 0 1.358L1.52 31.836c-.186.188-.432.282-.677.282z" fill="#fff"></path>
								</svg>
							</a>
							<div class = "product_image"><img style="width: inherit;" src="<?=$product->featuredImage()?>"></div>
							<div class = "product_data">
								<h6><?=$product->name?></h6>
								<p class = "price">$<?=number_format($product->price, 1)?></p>
								<p><label>Qty:</label>1</p>
							</div>
							<?php  $reflect = new ReflectionClass($product); $category = $reflect->getShortName();?>
							<a class="view_product green_btn" href="<?=$product->getUrl()?>">
								<p>View Product</p>
							</a>
						</li>
					<? } ?>
				<? } else { ?>
				<h3>None</h3>
				<? } ?>
			</ul>		
		</div>
		<div class="account_tab_content" id="account_tab_content_orders" style="display:none;">
			<h2>Your Orders</h2>
			<ul class = "order_summary_data">
				<?
				$orders = $model->user->order();
				$orderProducts = [];
				foreach($orders as $order){
					$orderProducts[] = ['products' => $order->orderProducts(), 'order' => $order];
				}
				?>

				<?php if($orderProducts){
					foreach($orderProducts as $products){ $order = $products['order'];?>
						<li>Order Id: <?=$order->id?>
						<div>Insert Time: <?=$order->insert_time?></div>
						<?foreach($products['products'] as $product) {
							$product_map = \Model\Product_Map::getItem($product->product_map_id)?>
								<div>Order-Product Id: <?=$product->id?></div>
								<div><?switch($product_map->product_type_id){
										case 1: echo 'Diamond ';break;
										case 2: echo 'Wedding Band ';break;
										case 3: echo 'Ring ';break;}
									?>Id: <?=$product_map->product_id?>
								</div>
								<div>Name: <?=$product_map->getProduct($product_map->product_id, $product_map->product_type_id)->name?></div>
<!--								<div>Product Image: <img src="--><?//=$product_map->getProduct($product_map->product_id, $product_map->product_type_id)->featuredImage()?><!--"/></div>-->
								<div>Unit Price: <?=$product->unit_price?></div>
						<?php } ?>
						</li>
						<br/>
					<?}?>
				<?} else {?>
					<h3>None</h3>
				<?}?>
<!--				--><?php //if($orders = $model->user->order()){
//					foreach($model->user->order() as $order){?>
<!--						<li>-->
<!--							<a class = "remove_product " data-order_id = "--><?//=$order->id?><!--">-->
<!--								<svg class="modal_svg" xmlns="http://www.w3.org/2000/svg" version="1.1" width="32" height="32" viewBox="0 0 32 32">-->
<!--									<path class="modal_svg_path" d="M31.158 32.118c-.246 0-.492-.093-.678-.282L.163 1.52c-.375-.375-.375-.983 0-1.358s.983-.375 1.358 0l30.317 30.316c.375.375.375.983 0 1.358-.187.188-.433.282-.678.282zM.842 32.118c-.246 0-.492-.093-.678-.282-.375-.375-.375-.983 0-1.358L30.48.162c.375-.375.983-.375 1.358 0s.375.983 0 1.358L1.52 31.836c-.186.188-.432.282-.677.282z" fill="#fff"></path>-->
<!--								</svg>-->
<!--							</a>-->
<!--							<div>Order_Id: --><?//=$order->id?><!--</div>-->
<!--							<div>Tracking_Number: --><?//=$order->tracking_number?><!--</div>-->
<!--							<div>Email: --><?//=$order->email?><!--</div>-->
<!--							<div>Shipping Address: --><?//=$order->ship_address?><!--</div>-->
<!--						</li>-->
<!--					--><?//}?>
<!--				--><?// } else { ?>
<!--					<h3>None</h3>-->
<!--				--><?//}?>

			</ul>

			<br>
			<br>
			<br>
		</div>
		<div class="account_tab_content" id="account_tab_content_password" style="display:none;">
			<div>
				<form method="post" action="/account/password">
					<div>Change Password</div>
					<label for="password_change"></label>
					<input name="id" style="display:none" value="<?=$model->user->id?>">
					<input name="email" style="display:none" value="<?=$model->user->email?>">
					<input name="first_name" style="display:none" value="<?=$model->user->first_name?>">
					<input name="last_name" style="display:none" value="<?=$model->user->last_name?>">
					<input id="password_change" name="password" type="text">
					<input type="submit" value="Update">
				</form>
			</div>

			
			<form method="post" class="newsletter_settings_section" action="/account/newsletter">
				<div>Email Newsletter</div>
				<input name="email" style="display:none" value="<?=$model->user->email?>">
				<?php
					$selectedCategories = $model->user->newsletter();
					$categoriesArray = [];
					foreach($selectedCategories as $category){
						$categoriesArray[$category->category] = $category->is_subscribe;
					}
				?>
				<?php foreach (\Model\Newsletter::getCategories() as $category) { ?>
					<div class="mdl_radio_wrapper">
					<?=$category?>:<br/>

					<label class="mdl_settings_radio mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect <?php if(isset($categoriesArray[$category]) && $categoriesArray[$category] == 1) echo 'is_checked';?>" for="<?=$category?>Yes">
						<span class="mdl-checkbox__label">Yes</span>
						<input type="checkbox" id="<?=$category?>Yes" name="category[<?=$category?>]" class="mdl-checkbox__input" <?php if(isset($categoriesArray[$category]) && $categoriesArray[$category] == 1) echo "checked";?> value="on"/>
					</label>

					<label class="mdl_settings_radio mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect <?php if(!isset($categoriesArray[$category]) || $categoriesArray[$category] == 0) echo 'is_checked';?>" for="<?=$category?>No">
						<span class="mdl-checkbox__label">No</span>
						<input type="checkbox" id="<?=$category?>No" name="category[<?=$category?>]" class="mdl-checkbox__input" <?php if(!isset($categoriesArray[$category]) || $categoriesArray[$category] == 0) echo "checked";?> value="on"/>
					</label>
					</div>
				<?}?>
				<input type="submit" value="Update">
			</form>
		</div>
	</div>
</div>

<script>
	$(document).ready(function(){
		function gup( name, url ) {
			if (!url) url = location.href;
			name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
			var regexS = "[\\?&]"+name+"=([^&#]*)";
			var regex = new RegExp( regexS );
			var results = regex.exec( url );
			return results == null ? null : results[1];
		}
		var tab = gup('tab', window.location.href);
		$("#account_tab_hearder_"+tab).trigger('click');
		$('.shipBtn').click(function(){
			$('.shipAddr').toggle();
			});
		$('.billBtn').click(function(){
			$('.billAddr').toggle();
			});
		$('.addAddr').click(function(){
			$('.inputAddrName').toggle();
		});
	})
</script>
--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.2
-- Dumped by pg_dump version 9.4.2
-- Started on 2015-10-28 16:32:58 EDT

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 206 (class 3079 OID 12123)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2491 (class 0 OID 0)
-- Dependencies: 206
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 172 (class 1259 OID 116555)
-- Name: admin_roles; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE admin_roles (
    id integer NOT NULL,
    active smallint DEFAULT 1 NOT NULL,
    insert_time timestamp without time zone DEFAULT now() NOT NULL,
    admin_id integer NOT NULL,
    role_id integer NOT NULL
);


ALTER TABLE admin_roles OWNER TO postgres;

--
-- TOC entry 173 (class 1259 OID 116560)
-- Name: admin_roles_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE admin_roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE admin_roles_id_seq OWNER TO postgres;

--
-- TOC entry 2492 (class 0 OID 0)
-- Dependencies: 173
-- Name: admin_roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE admin_roles_id_seq OWNED BY admin_roles.id;


--
-- TOC entry 174 (class 1259 OID 116562)
-- Name: administrator; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE administrator (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    first_name character varying,
    last_name character varying,
    email character varying,
    username character varying,
    password character varying,
    hash character varying,
    permissions character varying,
    signup_ip character varying,
    update_time character varying
);


ALTER TABLE administrator OWNER TO postgres;

--
-- TOC entry 175 (class 1259 OID 116570)
-- Name: administrator_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE administrator_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE administrator_id_seq OWNER TO postgres;

--
-- TOC entry 2493 (class 0 OID 0)
-- Dependencies: 175
-- Name: administrator_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE administrator_id_seq OWNED BY administrator.id;


--
-- TOC entry 176 (class 1259 OID 116572)
-- Name: category; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE banner (
    id SERIAL PRIMARY KEY,
    active SMALLINT DEFAULT 1,
    insert_time TIMESTAMP WITHOUT TIME ZONE DEFAULT now(),
    title VARCHAR,
    image VARCHAR,
    link VARCHAR,
    date_modified VARCHAR,
    is_deal BOOLEAN DEFAULT FALSE,
    featured_id INTEGER DEFAULT 0
) WITH (oids = false);


CREATE TABLE category (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    name character varying,
    subtitle character varying,
    parent_category integer,
    description character varying,
    slug character varying,
    meta_title character varying,
    meta_keywords character varying,
    meta_description character varying,
    display_order integer,
    in_menu boolean,
    image character varying
);


ALTER TABLE category OWNER TO postgres;

--
-- TOC entry 177 (class 1259 OID 116580)
-- Name: category_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE category_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE category_id_seq OWNER TO postgres;

--
-- TOC entry 2494 (class 0 OID 0)
-- Dependencies: 177
-- Name: category_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE category_id_seq OWNED BY category.id;


--
-- TOC entry 178 (class 1259 OID 116582)
-- Name: config; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE config (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    name character varying,
    value character varying
);


ALTER TABLE config OWNER TO postgres;

--
-- TOC entry 179 (class 1259 OID 116590)
-- Name: config_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE config_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE config_id_seq OWNER TO postgres;

--
-- TOC entry 2495 (class 0 OID 0)
-- Dependencies: 179
-- Name: config_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE config_id_seq OWNED BY config.id;


--
-- TOC entry 180 (class 1259 OID 116592)
-- Name: coupon; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE coupon (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    name character varying,
    code character varying,
    discount_type character varying,
    discount_amount character varying,
    min_amount character varying,
    num_uses_all character varying,
    uses_per_user character varying,
    start_time character varying,
    end_time character varying
);


ALTER TABLE coupon OWNER TO postgres;

--
-- TOC entry 181 (class 1259 OID 116600)
-- Name: coupon_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE coupon_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE coupon_id_seq OWNER TO postgres;

--
-- TOC entry 2496 (class 0 OID 0)
-- Dependencies: 181
-- Name: coupon_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE coupon_id_seq OWNED BY coupon.id;


--
-- TOC entry 182 (class 1259 OID 116602)
-- Name: credit; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE credit (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    name character varying,
    description character varying,
    price character varying,
    value character varying
);


ALTER TABLE credit OWNER TO postgres;

--
-- TOC entry 183 (class 1259 OID 116610)
-- Name: credit_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE credit_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE credit_id_seq OWNER TO postgres;

--
-- TOC entry 2497 (class 0 OID 0)
-- Dependencies: 183
-- Name: credit_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE credit_id_seq OWNED BY credit.id;


--
-- TOC entry 203 (class 1259 OID 116741)
-- Name: newsletter; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE newsletter (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    email character varying,
    is_subscribe integer DEFAULT 1
);


ALTER TABLE newsletter OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 116739)
-- Name: newsletter_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE newsletter_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE newsletter_id_seq OWNER TO postgres;

--
-- TOC entry 2498 (class 0 OID 0)
-- Dependencies: 202
-- Name: newsletter_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE newsletter_id_seq OWNED BY newsletter.id;


--
-- TOC entry 184 (class 1259 OID 116612)
-- Name: order; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "order" (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    bill_first_name character varying,
    bill_last_name character varying,
    bill_address character varying,
    bill_address2 character varying,
    bill_city character varying,
    bill_state character varying,
    bill_country character varying,
    bill_zip character varying,
    ship_first_name character varying,
    ship_last_name character varying,
    ship_address character varying,
    ship_address2 character varying,
    ship_city character varying,
    ship_state character varying,
    ship_country character varying,
    ship_zip character varying,
    cc_number character varying,
    cc_expiration_month character varying,
    cc_expiration_year character varying,
    cc_ccv character varying,
    tax character varying,
    tax_rate character varying,
    status character varying,
    tracking_number character varying,
    shipping_method character varying,
    user_id integer,
    coupon_code character varying,
    gift_card character varying,
    email character varying,
    shipping_cost real,
    payment_method integer,
    subtotal real,
    total real,
    viewed boolean,
    phone character varying,
    coupon_type integer,
    coupon_amount numeric,
    note character varying,
    user_role character varying
);


ALTER TABLE "order" OWNER TO postgres;

--
-- TOC entry 185 (class 1259 OID 116620)
-- Name: order_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE order_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE order_id_seq OWNER TO postgres;

--
-- TOC entry 2499 (class 0 OID 0)
-- Dependencies: 185
-- Name: order_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE order_id_seq OWNED BY "order".id;


--
-- TOC entry 186 (class 1259 OID 116622)
-- Name: order_products; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE order_products (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    order_id integer,
    product_id integer,
    quantity integer,
    unit_price real,
    size character varying,
    color character varying,
    type_product real,
    provider_id integer,
    date_of_pack character varying
);


ALTER TABLE order_products OWNER TO postgres;

--
-- TOC entry 187 (class 1259 OID 116630)
-- Name: order_products_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE order_products_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE order_products_id_seq OWNER TO postgres;

--
-- TOC entry 2500 (class 0 OID 0)
-- Dependencies: 187
-- Name: order_products_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE order_products_id_seq OWNED BY order_products.id;


--
-- TOC entry 188 (class 1259 OID 116632)
-- Name: page; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE page (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    title character varying,
    slug character varying,
    description character varying,
    featured_image character varying,
    meta_title character varying,
    meta_keywords character varying,
    meta_description character varying,
    date_modified character varying
);


ALTER TABLE page OWNER TO postgres;

--
-- TOC entry 189 (class 1259 OID 116640)
-- Name: page_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE page_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE page_id_seq OWNER TO postgres;

--
-- TOC entry 2501 (class 0 OID 0)
-- Dependencies: 189
-- Name: page_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE page_id_seq OWNED BY page.id;


--
-- TOC entry 190 (class 1259 OID 116642)
-- Name: product; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE product (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    name character varying,
    sku character varying,
    price character varying,
    meta_title character varying,
    meta_keywords character varying,
    meta_description character varying,
    featured_image character varying,
    colors character varying,
    featured smallint,
    shape character varying,
    clarity character varying,
    weight character varying,
    lab character varying,
    cut_grade character varying,
    polish character varying,
    symmetry character varying,
    fluor character varying,
    rapaport_price character varying,
    total character varying,
    certificate character varying,
    length character varying,
    width character varying,
    depth character varying,
    depth_percent character varying,
    table_percent character varying,
    girdle character varying,
    culet character varying,
    description character varying,
    origin character varying,
    memo_status character varying,
    inscription character varying,
    certificate_file character varying,
    slug character varying
);


ALTER TABLE product OWNER TO postgres;

--
-- TOC entry 191 (class 1259 OID 116650)
-- Name: product_categories; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE product_categories (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    product_id integer,
    category_id integer
);


ALTER TABLE product_categories OWNER TO postgres;

--
-- TOC entry 192 (class 1259 OID 116655)
-- Name: product_categories_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE product_categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE product_categories_id_seq OWNER TO postgres;

--
-- TOC entry 2502 (class 0 OID 0)
-- Dependencies: 192
-- Name: product_categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE product_categories_id_seq OWNED BY product_categories.id;


--
-- TOC entry 193 (class 1259 OID 116657)
-- Name: product_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE product_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE product_id_seq OWNER TO postgres;

--
-- TOC entry 2503 (class 0 OID 0)
-- Dependencies: 193
-- Name: product_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE product_id_seq OWNED BY product.id;


--
-- TOC entry 205 (class 1259 OID 116754)
-- Name: product_images; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE product_images (
    id integer NOT NULL,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    product_id integer,
    image character varying,
    display_order integer
);


ALTER TABLE product_images OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 116752)
-- Name: product_images_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE product_images_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE product_images_id_seq OWNER TO postgres;

--
-- TOC entry 2504 (class 0 OID 0)
-- Dependencies: 204
-- Name: product_images_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE product_images_id_seq OWNED BY product_images.id;


--
-- TOC entry 194 (class 1259 OID 116659)
-- Name: role; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE role (
    id integer NOT NULL,
    active smallint DEFAULT 1 NOT NULL,
    insert_time timestamp without time zone DEFAULT now() NOT NULL,
    name character varying NOT NULL
);


ALTER TABLE role OWNER TO postgres;

--
-- TOC entry 195 (class 1259 OID 116667)
-- Name: role_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE role_id_seq OWNER TO postgres;

--
-- TOC entry 2505 (class 0 OID 0)
-- Dependencies: 195
-- Name: role_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE role_id_seq OWNED BY role.id;


--
-- TOC entry 196 (class 1259 OID 116669)
-- Name: user; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "user" (
    id integer NOT NULL,
    active smallint DEFAULT 1 NOT NULL,
    insert_time timestamp without time zone DEFAULT now() NOT NULL,
    email character varying NOT NULL,
    password character varying,
    hash character varying,
    first_name character varying,
    last_name character varying,
    zip numeric,
    phone character varying,
    dob timestamp without time zone,
    bank_name character varying,
    routing_number character varying,
    bank_address character varying,
    bank_city character varying,
    voided_check character varying,
    photo character varying
);


ALTER TABLE "user" OWNER TO postgres;

--
-- TOC entry 197 (class 1259 OID 116677)
-- Name: user_favorite; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE user_favorite (
    id integer NOT NULL,
    active smallint DEFAULT 1 NOT NULL,
    insert_time timestamp without time zone DEFAULT now() NOT NULL,
    user_id integer,
    provider_id integer
);


ALTER TABLE user_favorite OWNER TO postgres;

--
-- TOC entry 198 (class 1259 OID 116682)
-- Name: user_favorite_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE user_favorite_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE user_favorite_id_seq OWNER TO postgres;

--
-- TOC entry 2506 (class 0 OID 0)
-- Dependencies: 198
-- Name: user_favorite_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE user_favorite_id_seq OWNED BY user_favorite.id;


--
-- TOC entry 199 (class 1259 OID 116684)
-- Name: user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE user_id_seq OWNER TO postgres;

--
-- TOC entry 2507 (class 0 OID 0)
-- Dependencies: 199
-- Name: user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE user_id_seq OWNED BY "user".id;


--
-- TOC entry 200 (class 1259 OID 116686)
-- Name: user_roles; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE user_roles (
    id integer NOT NULL,
    active smallint DEFAULT 1 NOT NULL,
    insert_time timestamp without time zone DEFAULT now() NOT NULL,
    user_id integer NOT NULL,
    role_id integer NOT NULL
);


ALTER TABLE user_roles OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 116691)
-- Name: user_roles_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE user_roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE user_roles_id_seq OWNER TO postgres;

--
-- TOC entry 2508 (class 0 OID 0)
-- Dependencies: 201
-- Name: user_roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE user_roles_id_seq OWNED BY user_roles.id;


--
-- TOC entry 2258 (class 2604 OID 116693)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY admin_roles ALTER COLUMN id SET DEFAULT nextval('admin_roles_id_seq'::regclass);


--
-- TOC entry 2261 (class 2604 OID 116694)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY administrator ALTER COLUMN id SET DEFAULT nextval('administrator_id_seq'::regclass);


--
-- TOC entry 2264 (class 2604 OID 116695)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY category ALTER COLUMN id SET DEFAULT nextval('category_id_seq'::regclass);


--
-- TOC entry 2267 (class 2604 OID 116696)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY config ALTER COLUMN id SET DEFAULT nextval('config_id_seq'::regclass);


--
-- TOC entry 2270 (class 2604 OID 116697)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY coupon ALTER COLUMN id SET DEFAULT nextval('coupon_id_seq'::regclass);


--
-- TOC entry 2273 (class 2604 OID 116698)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY credit ALTER COLUMN id SET DEFAULT nextval('credit_id_seq'::regclass);


--
-- TOC entry 2301 (class 2604 OID 116744)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY newsletter ALTER COLUMN id SET DEFAULT nextval('newsletter_id_seq'::regclass);


--
-- TOC entry 2276 (class 2604 OID 116699)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "order" ALTER COLUMN id SET DEFAULT nextval('order_id_seq'::regclass);


--
-- TOC entry 2279 (class 2604 OID 116700)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY order_products ALTER COLUMN id SET DEFAULT nextval('order_products_id_seq'::regclass);


--
-- TOC entry 2282 (class 2604 OID 116701)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY page ALTER COLUMN id SET DEFAULT nextval('page_id_seq'::regclass);


--
-- TOC entry 2285 (class 2604 OID 116702)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product ALTER COLUMN id SET DEFAULT nextval('product_id_seq'::regclass);


--
-- TOC entry 2288 (class 2604 OID 116703)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product_categories ALTER COLUMN id SET DEFAULT nextval('product_categories_id_seq'::regclass);


--
-- TOC entry 2304 (class 2604 OID 116757)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY product_images ALTER COLUMN id SET DEFAULT nextval('product_images_id_seq'::regclass);


--
-- TOC entry 2291 (class 2604 OID 116704)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY role ALTER COLUMN id SET DEFAULT nextval('role_id_seq'::regclass);


--
-- TOC entry 2294 (class 2604 OID 116705)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "user" ALTER COLUMN id SET DEFAULT nextval('user_id_seq'::regclass);


--
-- TOC entry 2297 (class 2604 OID 116706)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY user_favorite ALTER COLUMN id SET DEFAULT nextval('user_favorite_id_seq'::regclass);


--
-- TOC entry 2300 (class 2604 OID 116707)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY user_roles ALTER COLUMN id SET DEFAULT nextval('user_roles_id_seq'::regclass);


--
-- TOC entry 2450 (class 0 OID 116555)
-- Dependencies: 172
-- Data for Name: admin_roles; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO admin_roles VALUES (1, 1, '2015-08-21 12:40:22.392', 1, 1);


--
-- TOC entry 2509 (class 0 OID 0)
-- Dependencies: 173
-- Name: admin_roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('admin_roles_id_seq', 1, true);


--
-- TOC entry 2452 (class 0 OID 116562)
-- Dependencies: 174
-- Data for Name: administrator; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO administrator VALUES (1, 1, '2015-08-21 12:40:22.254', 'Master', 'User', 'master@user.com', 'emagid', '02d0d69b58490c81a83e5397db8cb3f9e061106692bd259f0aa1d96f67422270', '88a63e6a48e06121b858913886ed8c2a', 'Content,Pages,Blogs,Posts,Newsletter,Catalog,Categories,Products,Orders,Customers,Vendors,Banners,System,Email List,Configs,Administrators', NULL, NULL);


--
-- TOC entry 2510 (class 0 OID 0)
-- Dependencies: 175
-- Name: administrator_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('administrator_id_seq', 1, true);


--
-- TOC entry 2454 (class 0 OID 116572)
-- Dependencies: 176
-- Data for Name: category; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO category VALUES (2, 0, '2015-09-23 13:13:07.744', 'Testio', 'Testio 222', 0, '<p>Testio</p>

', 'testio', '', '', '', NULL, NULL, NULL);
INSERT INTO category VALUES (1, 0, '2015-08-31 13:41:31.003', 'NAme', '', 0, '', 'name', '', '', '', NULL, NULL, NULL);
INSERT INTO category VALUES (4, 1, '2015-10-01 18:44:52.709', 'Engagement', '', 0, '', 'engagement', '', '', '', NULL, true, NULL);
INSERT INTO category VALUES (8, 1, '2015-10-01 18:45:33.045', 'Jewelry', '', 0, '', 'jewelry', '', '', '', NULL, true, NULL);
INSERT INTO category VALUES (7, 1, '2015-10-01 18:45:24.244', 'Precious Gems', '', 0, '', 'precious-gems', '', '', '', NULL, true, NULL);
INSERT INTO category VALUES (5, 1, '2015-10-01 18:45:03.148', 'Weddings', '', 0, '', 'weddings', '', '', '', NULL, true, NULL);
INSERT INTO category VALUES (3, 1, '2015-10-01 18:44:32.053', 'Sale', '', 0, '', 'sale', '', '', '', 0, true, NULL);
INSERT INTO category VALUES (6, 1, '2015-10-01 18:45:13.027', 'postgres', '', 0, '', 'postgres', '', '', '', NULL, true, '560ef7f55bc35_wedding_ring_hero.png');
INSERT INTO category VALUES (9, 1, '2015-10-02 17:42:04.408', 'Wedding Rings', '', 6, '', 'wedding-rings', '', '', '', NULL, NULL, NULL);
INSERT INTO category VALUES (10, 1, '2015-10-02 17:42:30.243', 'Bridal Jewelry', '', 6, '', 'bridal-jewelry', '', '', '', NULL, NULL, NULL);
INSERT INTO category VALUES (11, 0, '2015-10-02 17:42:41.415', 'Gifts for the Bride', '', 6, '', 'gifts-for-the-bride', '', '', '', NULL, NULL, NULL);
INSERT INTO category VALUES (12, 0, '2015-10-02 17:42:54.417', 'Gifts for the Groom', '', 6, '', 'gifts-for-the-groom', '', '', '', NULL, NULL, NULL);


--
-- TOC entry 2511 (class 0 OID 0)
-- Dependencies: 177
-- Name: category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('category_id_seq', 12, true);


--
-- TOC entry 2456 (class 0 OID 116582)
-- Dependencies: 178
-- Data for Name: config; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO config VALUES (1, 1, '2015-08-21 12:04:19.995', 'Meta Title', NULL);
INSERT INTO config VALUES (2, 1, '2015-08-21 12:04:19.995', 'Meta Description', NULL);
INSERT INTO config VALUES (3, 1, '2015-08-21 12:04:19.995', 'Meta Keywords', NULL);


--
-- TOC entry 2512 (class 0 OID 0)
-- Dependencies: 179
-- Name: config_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('config_id_seq', 1, false);


--
-- TOC entry 2458 (class 0 OID 116592)
-- Dependencies: 180
-- Data for Name: coupon; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO coupon VALUES (1, 1, '2015-08-21 14:52:09.922', 'Test', '23423423423432423423423423', '1', '100', '10.00', '10', NULL, '1439330400', '1440108000');


--
-- TOC entry 2513 (class 0 OID 0)
-- Dependencies: 181
-- Name: coupon_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('coupon_id_seq', 1, true);


--
-- TOC entry 2460 (class 0 OID 116602)
-- Dependencies: 182
-- Data for Name: credit; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO credit VALUES (1, 1, '2015-08-21 16:23:31.588', '20 credits', '<p>Desc</p>

', '100', '75');


--
-- TOC entry 2514 (class 0 OID 0)
-- Dependencies: 183
-- Name: credit_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('credit_id_seq', 1, true);


--
-- TOC entry 2481 (class 0 OID 116741)
-- Dependencies: 203
-- Data for Name: newsletter; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO newsletter VALUES (1, 1, '2015-10-23 18:41:51.170288', 'sean@emagid.com');
INSERT INTO newsletter VALUES (2, 1, '2015-10-23 18:42:31.84288', 'sean@emagid.com');
INSERT INTO newsletter VALUES (3, 1, '2015-10-23 18:42:32.718904', 'sean@emagid.com');
INSERT INTO newsletter VALUES (4, 1, '2015-10-23 18:42:33.899157', 'sean@emagid.com');
INSERT INTO newsletter VALUES (5, 1, '2015-10-23 18:42:34.119224', 'sean@emagid.com');
INSERT INTO newsletter VALUES (6, 1, '2015-10-23 18:42:34.475881', 'sean@emagid.com');
INSERT INTO newsletter VALUES (7, 1, '2015-10-23 18:42:34.700025', 'sean@emagid.com');
INSERT INTO newsletter VALUES (8, 1, '2015-10-23 18:42:34.898417', 'sean@emagid.com');
INSERT INTO newsletter VALUES (9, 1, '2015-10-23 18:42:35.100269', 'sean@emagid.com');


--
-- TOC entry 2515 (class 0 OID 0)
-- Dependencies: 202
-- Name: newsletter_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('newsletter_id_seq', 9, true);


--
-- TOC entry 2462 (class 0 OID 116612)
-- Dependencies: 184
-- Data for Name: order; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO "order" VALUES (109, 1, '2015-09-24 20:54:06', 'Evgeny', 'Emagid', 'gdfg', NULL, 'dfdsfd', 'AS', NULL, '4324', 'Evgeny', 'Emagid', 'gdfg', NULL, 'dfdsfd', 'AS', NULL, '4324', '23423423423', '01', '2016', '234', NULL, NULL, NULL, NULL, NULL, 75, NULL, NULL, 'dgdgd@fd.rtr', NULL, NULL, NULL, 149, false, '+1(233)333-3333', NULL, NULL, NULL, 'customer');
INSERT INTO "order" VALUES (110, 1, '2015-09-24 21:53:20', 'Evgeny', 'Emagid', 'dsfdsf', NULL, 'dsadsad', 'AK', NULL, '2323', 'Evgeny', 'Emagid', 'dsfdsf', NULL, 'dsadsad', 'AK', NULL, '2323', '231', '02', '2016', NULL, NULL, NULL, NULL, NULL, NULL, 75, NULL, NULL, 'xx.tj.zz@gmail.com', NULL, NULL, NULL, 149, false, '+1(222)222-2222', NULL, NULL, NULL, 'customer');
INSERT INTO "order" VALUES (111, 1, '2015-09-24 22:10:09', 'Evgeny', 'Emagid', '2111 albemarle', NULL, 'sadsad', 'AK', NULL, '23123', 'Evgeny', 'Emagid', '2111 albemarle', NULL, 'sadsad', 'AK', NULL, '23123', '123123', '02', '2016', '12321', NULL, NULL, NULL, NULL, NULL, 75, NULL, NULL, 'xx.tj.zz@gmail.com', NULL, NULL, NULL, 149, false, '+1(213)222-2222', NULL, NULL, NULL, 'customer');
INSERT INTO "order" VALUES (112, 1, '2015-09-24 23:16:30', 'Evgeny', 'Emagid', '2111 albemarle', NULL, 'sadsad', 'AK', NULL, '2323', 'Evgeny', 'Emagid', '2111 albemarle', NULL, 'sadsad', 'AK', NULL, '2323', '2322', '01', '2016', '232', NULL, NULL, NULL, NULL, NULL, 75, NULL, NULL, 'xx.tj.zz@gmail.com', NULL, NULL, NULL, NULL, false, '+1(222)222-2222', NULL, NULL, NULL, 'customer');
INSERT INTO "order" VALUES (113, 1, '2015-09-24 23:18:30', 'Evgeny', 'Emagid', '2111 albemarle', NULL, 'sadsad', 'AK', NULL, '2323', 'Evgeny', 'Emagid', '2111 albemarle', NULL, 'sadsad', 'AK', NULL, '2323', '2322', '01', '2016', '232', NULL, NULL, NULL, NULL, NULL, 75, NULL, NULL, 'xx.tj.zz@gmail.com', NULL, NULL, NULL, 4950, false, '+1(222)222-2222', NULL, NULL, NULL, 'customer');


--
-- TOC entry 2516 (class 0 OID 0)
-- Dependencies: 185
-- Name: order_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('order_id_seq', 113, true);


--
-- TOC entry 2464 (class 0 OID 116622)
-- Dependencies: 186
-- Data for Name: order_products; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO order_products VALUES (503, 1, '2015-09-24 14:54:06.691', 109, 6, NULL, 149, NULL, NULL, 3, 30, 'September 25  2015 at 10:00am');
INSERT INTO order_products VALUES (504, 1, '2015-09-24 15:53:20.992', 110, 6, NULL, 149, NULL, NULL, 3, 30, 'September 24  2015 at 3:00pm');
INSERT INTO order_products VALUES (505, 1, '2015-09-24 16:10:09.342', 111, 6, NULL, 149, NULL, NULL, 3, 30, 'September 24  2015 at 6:00pm');
INSERT INTO order_products VALUES (506, 1, '2015-09-24 17:16:30.688', 112, 11, NULL, 4950, NULL, NULL, 2, 30, 'September 24  2015 at 4:00pm');
INSERT INTO order_products VALUES (507, 1, '2015-09-24 17:18:30.097', 113, 11, NULL, 4950, NULL, NULL, 2, 30, 'September 24  2015 at 4:00pm');
INSERT INTO order_products VALUES (508, 1, '2015-09-24 19:03:00.281', NULL, 2, NULL, 200, '2XL', '#cedbcf', 1, NULL, NULL);
INSERT INTO order_products VALUES (509, 1, '2015-09-24 19:03:00.293', NULL, 2, NULL, 200, 'XL', 'heather gray', 1, NULL, NULL);


--
-- TOC entry 2517 (class 0 OID 0)
-- Dependencies: 187
-- Name: order_products_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('order_products_id_seq', 509, true);


--
-- TOC entry 2466 (class 0 OID 116632)
-- Dependencies: 188
-- Data for Name: page; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO page VALUES (1, 1, '2015-09-01 13:05:32.499', 'Our Company', 'our-company', '<p>&quot;Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&quot;</p>
', NULL, 'sad', 'asd', 'asd', NULL);
INSERT INTO page VALUES (2, 1, '2015-10-23 16:18:03.795351', 'Careers', 'careers', '<p>&quot;Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&quot;</p>
', NULL, '', '', '', NULL);
INSERT INTO page VALUES (3, 1, '2015-10-23 16:18:39.060969', 'Support', 'support', '<p>&quot;Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&quot;</p>
', NULL, '', '', '', NULL);
INSERT INTO page VALUES (4, 1, '2015-10-23 16:18:47.401568', 'Shipping', 'shipping', '<p>&quot;Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&quot;</p>
', NULL, '', '', '', NULL);
INSERT INTO page VALUES (5, 1, '2015-10-23 16:18:54.910587', 'Tracking', 'tracking', '<p>&quot;Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&quot;</p>
', NULL, '', '', '', NULL);
INSERT INTO page VALUES (6, 1, '2015-10-23 16:19:01.18121', 'Returns', 'returns', '<p>&quot;Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&quot;</p>
', NULL, '', '', '', NULL);
INSERT INTO page VALUES (7, 1, '2015-10-23 16:19:16.290312', 'Engagements', 'engagements', '<p>&quot;Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&quot;</p>
', NULL, '', '', '', NULL);
INSERT INTO page VALUES (8, 1, '2015-10-23 16:19:27.966844', 'Collections', 'collections', '<p>&quot;Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&quot;</p>
', NULL, '', '', '', NULL);
INSERT INTO page VALUES (9, 1, '2015-10-23 16:19:38.854992', 'On Sale', 'on-sale', '<p>&quot;Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&quot;</p>
', NULL, '', '', '', NULL);
INSERT INTO page VALUES (10, 1, '2015-10-23 16:19:59.401989', 'Wholesale', 'wholesale', '<p>&quot;Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&quot;</p>
', NULL, '', '', '', NULL);


--
-- TOC entry 2518 (class 0 OID 0)
-- Dependencies: 189
-- Name: page_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('page_id_seq', 10, true);


--
-- TOC entry 2468 (class 0 OID 116642)
-- Dependencies: 190
-- Data for Name: product; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO product VALUES (115, 1, '2015-10-15 18:09:06.099', '17', '17', '2250', NULL, NULL, NULL, NULL, 'K', NULL, 'RB', 'VVS2', '1.32', 'IGI', 'Ideal', 'Excellent', 'Very Good', 'NONE', '5000', '2970', '10127701', '7.13', '7.08', '4.33', '0.611', '0.56', 'MEDIUM, FACETED', 'NONE', NULL, 'Lab Created', '', '10127701', 'http://www.washingtonpostgrescorp.com/certificates/LG10127701.pdf', NULL);
INSERT INTO product VALUES (116, 1, '2015-10-15 18:09:06.105', '146', '146', '2925', NULL, NULL, NULL, NULL, 'H', NULL, 'RB', 'SI1', '1.02', 'IGI', 'Very Good', 'Excellent', 'Very Good', 'NONE', '6500', '2983.5', '10151415', '6.62', '6.56', '3.92', '0.596', '0.575', 'VERY THIN TO THIN, FACETED', 'NONE', NULL, 'Lab Created', '', '10151415', 'http://www.washingtonpostgrescorp.com/certificates/LG10151415.pdf', NULL);
INSERT INTO product VALUES (117, 1, '2015-10-15 18:09:06.11', '1637', '1637', '3015', NULL, NULL, NULL, NULL, 'I', NULL, 'RB', 'VS1', '1.04', 'IGI', 'Excellent', 'Excellent', 'Very Good', 'NONE', '6700', '3135.6', '10133909', '6.62', '6.6', '3.9', '0.591', '0.62', 'MEDIUM, FACETED', 'NONE', NULL, 'Lab Created', '', '10133909', 'http://www.washingtonpostgrescorp.com/certificates/LG10133909.pdf', NULL);
INSERT INTO product VALUES (118, 1, '2015-10-15 18:09:06.119', '1701', '1701', '3015', NULL, NULL, NULL, NULL, 'I', NULL, 'RB', 'VS1', '1.08', 'IGI', 'Excellent', 'Excellent', 'Very Good', 'NONE', '6700', '3256.2', '10135503', '6.72', '6.65', '3.96', '0.592', '0.61', 'MEDIUM, FACETED', 'NONE', NULL, 'Lab Created', '', '10135503', 'http://www.washingtonpostgrescorp.com/certificates/LG10135503.pdf', NULL);
INSERT INTO product VALUES (119, 1, '2015-10-15 18:09:06.125', '1713', '1713', '2520', NULL, NULL, NULL, NULL, 'H', NULL, 'RB', 'SI2', '1.1', 'IGI', 'Excellent', 'Very Good', 'Very Good', 'NONE', '5600', '2772', '10135611', '6.6', '6.57', '4.15', '0.631', '0.535', 'MEDIUM, FACETED', 'NONE', NULL, 'Lab Created', '', '10135611', 'http://www.washingtonpostgrescorp.com/certificates/LG10135611.pdf', NULL);
INSERT INTO product VALUES (120, 1, '2015-10-15 18:09:06.131', '1746', '1746', '3420', NULL, NULL, NULL, NULL, 'H', NULL, 'RB', 'VS1', '1.06', 'IGI', 'Ideal', 'Excellent', 'Very Good', 'NONE', '7600', '3625.2', '10136118', '6.59', '6.55', '3.99', '0.608', '0.58', 'MEDIUM, FACETED', 'VERY SMALL', NULL, 'Lab Created', '', '10136118', 'http://www.washingtonpostgrescorp.com/certificates/LG10136118.pdf', NULL);
INSERT INTO product VALUES (121, 1, '2015-10-15 18:09:06.136', '1760', '1760', '3015', NULL, NULL, NULL, NULL, 'I', NULL, 'RB', 'VS1', '1.03', 'IGI', 'Ideal', 'Excellent', 'Very Good', 'NONE', '6700', '3105.45', '10137004', '6.6', '6.56', '3.91', '0.594', '0.595', 'MEDIUM, FACETED', 'NONE', NULL, 'Lab Created', '', '10137004', 'http://www.washingtonpostgrescorp.com/certificates/LG10137004.pdf', NULL);
INSERT INTO product VALUES (122, 1, '2015-10-15 18:09:06.144', '1767', '1767', '2880', NULL, NULL, NULL, NULL, 'I', NULL, 'RB', 'VS2', '1.1', 'IGI', 'Ideal', 'Excellent', 'Excellent', 'NONE', '6400', '3168', '10135604', '6.72', '6.69', '4', '0.597', '0.585', 'MEDIUM, FACETED', 'NONE', NULL, 'Lab Created', '', '10135604', 'http://www.washingtonpostgrescorp.com/certificates/LG10135604.pdf', NULL);
INSERT INTO product VALUES (123, 1, '2015-10-15 18:09:06.154', '1451', '1451', '2250', NULL, NULL, NULL, NULL, 'K', NULL, 'RB', 'VVS2', '1.13', 'IGI', 'Very Good', 'Good', 'Very Good', 'NONE', '5000', '2542.5', '10160203', '6.69', '6.67', '4.14', '0.62', '0.585', 'MEDIUM', 'NONE', NULL, 'Lab Created', '', '10160203', 'http://www.washingtonpostgrescorp.com/certificates/LG10160203.pdf', NULL);
INSERT INTO product VALUES (124, 1, '2015-10-15 18:09:06.164', '1509', '1509', '2520', NULL, NULL, NULL, NULL, 'H', NULL, 'RB', 'SI2', '1.09', 'IGI', 'Excellent', 'Very Good', 'Very Good', 'NONE', '5600', '2746.8', '10130315', '6.75', '6.69', '3.96', '0.59', '0.605', 'MEDIUM TO SLIGHTLY THICK, FACETED', 'NONE', NULL, 'Lab Created', '', '10130315', 'http://www.washingtonpostgrescorp.com/certificates/LG10130315.pdf', NULL);
INSERT INTO product VALUES (125, 1, '2015-10-15 18:09:06.173', '1506', '1506', '2880', NULL, NULL, NULL, NULL, 'I', NULL, 'RB', 'VS2', '1.07', 'IGI', 'Ideal', 'Excellent', 'Excellent', 'NONE', '6400', '3081.6', '10132418', '6.61', '6.56', '4.02', '0.611', '0.585', 'MEDIUM, FACETED', 'NONE', NULL, 'Lab Created', '', '10132418', 'http://www.washingtonpostgrescorp.com/certificates/LG10132418.pdf', NULL);
INSERT INTO product VALUES (126, 1, '2015-10-15 18:09:06.183', '1507', '1507', '3105', NULL, NULL, NULL, NULL, 'I', NULL, 'RB', 'VVS2', '1.07', 'IGI', 'Very Good', 'Excellent', 'Excellent', 'NONE', '6900', '3322.35', '10132420', '6.57', '6.51', '4.03', '0.616', '0.57', 'MEDIUM TO THICK, FACETED', 'NONE', NULL, 'Lab Created', '', '10132420', 'http://www.washingtonpostgrescorp.com/certificates/LG10132420.pdf', NULL);
INSERT INTO product VALUES (127, 1, '2015-10-15 18:09:06.189', '1538', '1538', '3105', NULL, NULL, NULL, NULL, 'I', NULL, 'RB', 'VVS2', '1.1', 'IGI', 'Ideal', 'Excellent', 'Very Good', 'NONE', '6900', '3415.5', '10133707', '6.66', '6.6', '4.06', '0.61', '0.58', 'MEDIUM, FACETED', 'NONE', NULL, 'Lab Created', '', '10133707', 'http://www.washingtonpostgrescorp.com/certificates/LG10133707.pdf', NULL);
INSERT INTO product VALUES (128, 1, '2015-10-15 18:09:06.197', '1557', '1557', '2880', NULL, NULL, NULL, NULL, 'I', NULL, 'RB', 'VS2', '1.11', 'IGI', 'Ideal', 'Excellent', 'Excellent', 'NONE', '6400', '3196.8', '10132405', '6.71', '6.69', '4.02', '0.6', '0.585', 'MEDIUM TO SLIGHTLY THICK, FACETED', 'NONE', NULL, 'Lab Created', '', '10132405', 'http://www.washingtonpostgrescorp.com/certificates/LG10132405.pdf', NULL);
INSERT INTO product VALUES (129, 1, '2015-10-15 18:09:06.203', '1934', '1934', '3420', NULL, NULL, NULL, NULL, 'H', NULL, 'RB', 'VS1', '1.06', 'IGI', 'Ideal', 'Excellent', 'Very Good', 'NONE', '7600', '3625.2', '10139614', '6.55', '6.51', '4.04', '0.618', '0.56', 'MEDIUM, FACETED', 'NONE', NULL, 'Lab Created', '', '10139614', 'http://www.washingtonpostgrescorp.com/certificates/LG10139614.pdf', NULL);
INSERT INTO product VALUES (130, 1, '2015-10-15 18:09:06.21', '2058', '2058', '3015', NULL, NULL, NULL, NULL, 'I', NULL, 'RB', 'VS1', '1.09', 'IGI', 'Ideal', 'Excellent', 'Excellent', 'NONE', '6700', '3286.35', '10141104', '6.73', '6.69', '4.05', '0.604', '0.565', 'MEDIUM, FACETED', 'NONE', NULL, 'Lab Created', '', '10141104', 'http://www.washingtonpostgrescorp.com/certificates/LG10141104.pdf', NULL);
INSERT INTO product VALUES (131, 1, '2015-10-15 18:09:06.216', '2188', '2188', '3015', NULL, NULL, NULL, NULL, 'I', NULL, 'RB', 'VS1', '1.1', 'IGI', 'Ideal', 'Very Good', 'Very Good', 'NONE', '6700', '3316.5', '10143205', '6.67', '6.62', '4.04', '0.607', '0.59', 'SLIGHTLY THICK, FACETED', 'NONE', NULL, 'Lab Created', '', '10143205', 'http://www.washingtonpostgrescorp.com/certificates/LG10143205.pdf', NULL);
INSERT INTO product VALUES (132, 1, '2015-10-15 18:09:06.221', '2194', '2194', '2520', NULL, NULL, NULL, NULL, 'H', NULL, 'RB', 'SI2', '1.11', 'IGI', 'Ideal', 'Excellent', 'Excellent', 'NONE', '5600', '2797.2', '10143708', '6.64', '6.59', '4.12', '0.622', '0.565', 'MEDIUM TO SLIGHTLY THICK, FACETED', 'NONE', NULL, 'Lab Created', '', '10143708', 'http://www.washingtonpostgrescorp.com/certificates/LG10143708.pdf', NULL);
INSERT INTO product VALUES (133, 1, '2015-10-15 18:09:06.227', '2308', '2308', '3105', NULL, NULL, NULL, NULL, 'I', NULL, 'RB', 'VVS2', '1.01', 'IGI', 'Excellent', 'Excellent', 'Excellent', 'NONE', '6900', '3136.05', '10146809', '6.5', '6.44', '3.86', '0.597', '0.605', 'MEDIUM TO SLIGHTLY THICK, FACETED', 'NONE', NULL, 'Lab Created', '', '10146809', 'http://www.washingtonpostgrescorp.com/certificates/LG10146809.pdf', NULL);
INSERT INTO product VALUES (134, 1, '2015-10-15 18:09:06.234', '2418', '2418', '2160', NULL, NULL, NULL, NULL, 'K', NULL, 'RB', 'VS1', '1.21', 'IGI', 'Excellent', 'Excellent', 'Excellent', 'NONE', '4800', '2613.6', '10151502', '6.97', '6.91', '4.13', '0.594', '0.61', 'MEDIUM, FACETED', 'NONE', NULL, 'Lab Created', '', '10151502', 'http://www.washingtonpostgrescorp.com/certificates/LG10151502.pdf', NULL);
INSERT INTO product VALUES (135, 1, '2015-10-15 18:09:06.241', '2432', '2432', '2295', NULL, NULL, NULL, NULL, 'I', NULL, 'RAD', 'VVS2', '1.26', 'IGI', '', 'Excellent', 'Very Good', 'NONE', '5100', '2891.7', '10151409', '6.25', '6.26', '3.73', '0.597', '0.685', 'MEDIUM', 'NONE', NULL, 'Lab Created', '', '10151409', 'http://www.washingtonpostgrescorp.com/certificates/LG10151409.pdf', NULL);
INSERT INTO product VALUES (136, 1, '2015-10-15 18:09:06.246', '2465', '2465', '2610', NULL, NULL, NULL, NULL, 'J', NULL, 'RB', 'VS1', '1.25', 'IGI', 'Ideal', 'Excellent', 'Very Good', 'NONE', '5800', '3262.5', '10155503', '7.04', '7', '4.18', '0.596', '0.59', 'MEDIUM, FACETED', 'NONE', NULL, 'Lab Created', '', '10155503', 'http://www.washingtonpostgrescorp.com/certificates/LG10155503.pdf', NULL);
INSERT INTO product VALUES (137, 1, '2015-10-15 18:09:06.251', '2495', '2495', '2475', NULL, NULL, NULL, NULL, 'J', NULL, 'RB', 'VS2', '1.23', 'IGI', 'Very Good', 'Excellent', 'Excellent', 'NONE', '5500', '3044.25', '10156405', '7.06', '6.99', '4.19', '0.597', '0.59', 'VERY THIN TO THIN, FACETED', 'NONE', NULL, 'Lab Created', '', '10156405', 'http://www.washingtonpostgrescorp.com/certificates/LG10156405.pdf', NULL);
INSERT INTO product VALUES (139, 1, '2015-10-15 18:09:06.261', '2507', NULL, '2610', '', '', '', '562ad225ee037_diamond_6.jpg', 'J', NULL, 'RB', 'VS1', '1.25', 'IGI', 'Very Good', 'Excellent', 'Excellent', 'NONE', '5800', '3262.5', '10156404', '7.05', '7.02', '4.24', NULL, '0.56', 'VERY THIN TO THIN', 'NONE', '', 'Lab Created', '', '10156404', 'http://www.washingtonpostgrescorp.com/certificates/LG10156404.pdf', 'newtest');
INSERT INTO product VALUES (140, 1, '2015-10-15 18:09:06.268', '2512', NULL, '9999', '', '', '', '562d2fc58f37e_diamond_6.jpg', 'J', NULL, 'RB', 'VVS2', '1.13', 'IGI', 'Ideal', 'Excellent', 'Very Good', 'NONE', '6000', '3051', '10155513', '6.83', '6.79', '4.04', NULL, '0.585', 'THIN, FACETED', 'NONE', '<p>cms ninja</p>
', 'Lab Created', '', '10155513', 'http://www.washingtonpostgrescorp.com/certificates/LG10155513.pdf', 'test123');
INSERT INTO product VALUES (138, 1, '2015-10-15 18:09:06.256', '2504', NULL, '2250', '', '', '', '562d2fd9a460a_diamond_6.jpg', 'K', NULL, 'RB', 'VVS2', '1.12', 'IGI', 'Ideal', 'Excellent', 'Excellent', 'NONE', '5000', '2520', '10155520', '6.71', '6.67', '4.08', NULL, '0.55', 'THIN TO MEDIUM, FACETED', 'NONE', '', 'Lab Created', '', '10155520', 'http://www.washingtonpostgrescorp.com/certificates/LG10155520.pdf', 'diamond');


--
-- TOC entry 2469 (class 0 OID 116650)
-- Dependencies: 191
-- Data for Name: product_categories; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO product_categories VALUES (5, 1, '2015-10-02 16:32:14.966', 2, 3);
INSERT INTO product_categories VALUES (6, 1, '2015-10-02 16:32:21.649', 1, 3);
INSERT INTO product_categories VALUES (7, 1, '2015-10-05 14:08:29.394', 3, 6);
INSERT INTO product_categories VALUES (2, 0, '2015-09-23 13:12:50.916', 2, 1);
INSERT INTO product_categories VALUES (1, 0, '2015-08-31 15:45:48.267', 1, 1);
INSERT INTO product_categories VALUES (8, 1, '2015-10-05 16:50:58.572', 9, 6);
INSERT INTO product_categories VALUES (9, 1, '2015-10-05 16:58:47.31', 8, 6);
INSERT INTO product_categories VALUES (10, 1, '2015-10-05 16:58:53.176', 7, 6);
INSERT INTO product_categories VALUES (11, 1, '2015-10-05 16:59:00.526', 6, 6);
INSERT INTO product_categories VALUES (12, 1, '2015-10-05 16:59:08.211', 5, 6);
INSERT INTO product_categories VALUES (13, 1, '2015-10-05 16:59:14.826', 4, 6);
INSERT INTO product_categories VALUES (3, 0, '2015-09-23 13:13:38.059', 3, 2);
INSERT INTO product_categories VALUES (4, 0, '2015-10-02 16:32:07.307', 3, 3);
INSERT INTO product_categories VALUES (14, 1, '2015-10-13 15:09:45.7', 111, 10);
INSERT INTO product_categories VALUES (15, 1, '2015-10-23 18:49:43.273898', 140, 10);
INSERT INTO product_categories VALUES (16, 1, '2015-10-23 18:49:43.275284', 140, 9);
INSERT INTO product_categories VALUES (17, 1, '2015-10-23 18:49:43.275868', 140, 8);
INSERT INTO product_categories VALUES (18, 1, '2015-10-23 18:49:43.276475', 140, 7);
INSERT INTO product_categories VALUES (19, 1, '2015-10-23 18:49:43.277057', 140, 6);
INSERT INTO product_categories VALUES (20, 1, '2015-10-23 18:49:43.277624', 140, 5);
INSERT INTO product_categories VALUES (21, 1, '2015-10-23 18:49:43.278196', 140, 4);
INSERT INTO product_categories VALUES (22, 1, '2015-10-23 18:49:43.27877', 140, 3);
INSERT INTO product_categories VALUES (23, 1, '2015-10-23 20:35:26.182048', 139, 10);
INSERT INTO product_categories VALUES (24, 1, '2015-10-23 20:35:26.183089', 139, 9);
INSERT INTO product_categories VALUES (25, 1, '2015-10-23 20:35:26.183677', 139, 8);
INSERT INTO product_categories VALUES (26, 1, '2015-10-23 20:35:26.184252', 139, 7);
INSERT INTO product_categories VALUES (27, 1, '2015-10-23 20:35:26.184819', 139, 6);
INSERT INTO product_categories VALUES (28, 1, '2015-10-23 20:35:26.185385', 139, 5);
INSERT INTO product_categories VALUES (29, 1, '2015-10-23 20:35:26.185954', 139, 4);
INSERT INTO product_categories VALUES (30, 1, '2015-10-23 20:35:26.18651', 139, 3);
INSERT INTO product_categories VALUES (31, 1, '2015-10-25 15:39:05.678397', 138, 10);
INSERT INTO product_categories VALUES (32, 1, '2015-10-25 15:39:05.681184', 138, 9);
INSERT INTO product_categories VALUES (33, 1, '2015-10-25 15:39:05.681806', 138, 8);
INSERT INTO product_categories VALUES (34, 1, '2015-10-25 15:39:05.682386', 138, 7);
INSERT INTO product_categories VALUES (35, 1, '2015-10-25 15:39:05.682975', 138, 6);
INSERT INTO product_categories VALUES (36, 1, '2015-10-25 15:39:05.683589', 138, 5);
INSERT INTO product_categories VALUES (37, 1, '2015-10-25 15:39:05.68449', 138, 4);
INSERT INTO product_categories VALUES (38, 1, '2015-10-25 15:39:05.685211', 138, 3);


--
-- TOC entry 2519 (class 0 OID 0)
-- Dependencies: 192
-- Name: product_categories_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('product_categories_id_seq', 38, true);


--
-- TOC entry 2520 (class 0 OID 0)
-- Dependencies: 193
-- Name: product_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('product_id_seq', 140, true);


--
-- TOC entry 2483 (class 0 OID 116754)
-- Dependencies: 205
-- Data for Name: product_images; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO product_images VALUES (3, 1, '2015-10-25 17:03:34.317983', 140, '562d43a64d11b_Apollo_synthetic_diamond.jpg', 2);
INSERT INTO product_images VALUES (4, 1, '2015-10-25 17:03:43.314697', 140, '562d43af4c8cf_diamondthree.png', 2);
INSERT INTO product_images VALUES (5, 1, '2015-10-25 17:03:59.129077', 140, '562d43bf1f52e_heart_shaped_diamond.jpg', 3);
INSERT INTO product_images VALUES (6, 1, '2015-10-25 20:42:46.503506', 138, '562d77067abf2_Apollo_synthetic_diamond.jpg', 1);
INSERT INTO product_images VALUES (7, 1, '2015-10-25 20:42:52.665181', 138, '562d770ca2185_diamond_6.jpg', 2);
INSERT INTO product_images VALUES (8, 1, '2015-10-25 20:43:22.795089', 139, '562d772ac1c2c_Apollo_synthetic_diamond.jpg', 1);
INSERT INTO product_images VALUES (9, 1, '2015-10-25 20:43:30.817568', 139, '562d7732c7535_heart_shaped_diamond.jpg', 2);
INSERT INTO product_images VALUES (10, 1, '2015-10-25 20:43:41.843178', 139, '562d773dcd8a9_diamond_6.jpg', 2);


--
-- TOC entry 2521 (class 0 OID 0)
-- Dependencies: 204
-- Name: product_images_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('product_images_id_seq', 10, true);


--
-- TOC entry 2472 (class 0 OID 116659)
-- Dependencies: 194
-- Data for Name: role; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO role VALUES (1, 1, '2015-08-21 12:05:07.458', 'admin');
INSERT INTO role VALUES (3, 1, '2015-08-21 12:05:07.458', 'provider');
INSERT INTO role VALUES (2, 1, '2015-08-21 12:05:07.458', 'customer');


--
-- TOC entry 2522 (class 0 OID 0)
-- Dependencies: 195
-- Name: role_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('role_id_seq', 1, false);


--
-- TOC entry 2474 (class 0 OID 116669)
-- Dependencies: 196
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO "user" VALUES (74, 0, '2015-09-15 16:01:06.146', 'xx.tj.zz@gmail.com', '978a36d080378115eff8dd1b4fa3e1c570b81f880ebf6b9ecb1bfb738f0736de', 'cd0cad83d9337bcc535e75eeeec6d4b6', 'Evgeny', 'Emagid', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "user" VALUES (75, 1, '2015-09-16 11:24:19.002', 'test@test.ru', '2df2e946372e25867e88c66ab1e17a754a15e0a0a09bdbec0ce49ea13d6fca30', '440bdbc12160b2ba1c16674c14a8150f', 'Evgeny', 'Emagid', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '560069c553408_rNHGcHwGYXM.jpg');


--
-- TOC entry 2475 (class 0 OID 116677)
-- Dependencies: 197
-- Data for Name: user_favorite; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2523 (class 0 OID 0)
-- Dependencies: 198
-- Name: user_favorite_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('user_favorite_id_seq', 126, true);


--
-- TOC entry 2524 (class 0 OID 0)
-- Dependencies: 199
-- Name: user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('user_id_seq', 75, true);


--
-- TOC entry 2478 (class 0 OID 116686)
-- Dependencies: 200
-- Data for Name: user_roles; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO user_roles VALUES (51, 1, '2015-09-15 16:01:06.154', 74, 2);
INSERT INTO user_roles VALUES (52, 1, '2015-09-16 11:24:19.274', 75, 2);


--
-- TOC entry 2525 (class 0 OID 0)
-- Dependencies: 201
-- Name: user_roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('user_roles_id_seq', 52, true);


--
-- TOC entry 2308 (class 2606 OID 116709)
-- Name: admin_roles_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY admin_roles
    ADD CONSTRAINT admin_roles_pkey PRIMARY KEY (id);


--
-- TOC entry 2310 (class 2606 OID 116711)
-- Name: administrator_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY administrator
    ADD CONSTRAINT administrator_pkey PRIMARY KEY (id);


--
-- TOC entry 2312 (class 2606 OID 116713)
-- Name: category_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY category
    ADD CONSTRAINT category_pkey PRIMARY KEY (id);


--
-- TOC entry 2314 (class 2606 OID 116715)
-- Name: config_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY config
    ADD CONSTRAINT config_pkey PRIMARY KEY (id);


--
-- TOC entry 2316 (class 2606 OID 116717)
-- Name: coupon_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY coupon
    ADD CONSTRAINT coupon_pkey PRIMARY KEY (id);


--
-- TOC entry 2318 (class 2606 OID 116719)
-- Name: credit_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY credit
    ADD CONSTRAINT credit_pkey PRIMARY KEY (id);


--
-- TOC entry 2338 (class 2606 OID 116751)
-- Name: newsletter_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY newsletter
    ADD CONSTRAINT newsletter_pkey PRIMARY KEY (id);


--
-- TOC entry 2320 (class 2606 OID 116721)
-- Name: order_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "order"
    ADD CONSTRAINT order_pkey PRIMARY KEY (id);


--
-- TOC entry 2322 (class 2606 OID 116723)
-- Name: order_products_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY order_products
    ADD CONSTRAINT order_products_pkey PRIMARY KEY (id);


--
-- TOC entry 2324 (class 2606 OID 116725)
-- Name: page_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY page
    ADD CONSTRAINT page_pkey PRIMARY KEY (id);


--
-- TOC entry 2328 (class 2606 OID 116727)
-- Name: product_categories_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY product_categories
    ADD CONSTRAINT product_categories_pkey PRIMARY KEY (id);


--
-- TOC entry 2340 (class 2606 OID 116764)
-- Name: product_images_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY product_images
    ADD CONSTRAINT product_images_pkey PRIMARY KEY (id);


--
-- TOC entry 2326 (class 2606 OID 116729)
-- Name: product_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY product
    ADD CONSTRAINT product_pkey PRIMARY KEY (id);


--
-- TOC entry 2330 (class 2606 OID 116731)
-- Name: role_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY role
    ADD CONSTRAINT role_pkey PRIMARY KEY (id);


--
-- TOC entry 2334 (class 2606 OID 116733)
-- Name: user_favorite_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY user_favorite
    ADD CONSTRAINT user_favorite_pkey PRIMARY KEY (id);


--
-- TOC entry 2332 (class 2606 OID 116735)
-- Name: user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);


--
-- TOC entry 2336 (class 2606 OID 116737)
-- Name: user_roles_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY user_roles
    ADD CONSTRAINT user_roles_pkey PRIMARY KEY (id);


--
-- TOC entry 2490 (class 0 OID 0)
-- Dependencies: 5
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2015-10-28 16:32:58 EDT

--
-- PostgreSQL database dump complete
--


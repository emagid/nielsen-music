<?php

namespace Emagid\Mvc\Views;

class Json extends \Emagid\Mvc\View
{
  
	protected function _render(){
		header("Content-type: application/json");

		die (json_encode($this->_params)); // Hack/ Patch for deficiency in framework.... needs a better way

	}
}

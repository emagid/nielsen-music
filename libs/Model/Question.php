<?php
/**
 * Created by PhpStorm.
 * User: Garrett
 * Date: 9/19/17
 * Time: 4:22 PM
 */

namespace Model;

class Question extends \Emagid\Core\Model {
    public static $tablename = "question";

    public static $fields = [
        'text',
        'failure_text',
        'image',
        'correct_answer_id',
        'display_order' => ['type'=>'numeric'],
        'source',
    ];

    public function get_answer(){

        $answer = Answer::getItem($this->correct_answer_id);
        if($answer->question_id != $this->id)
            return null;
        if($answer != null)
            return $answer->text;
        else
            return null;
    }

    public function get_answers(){
        return Answer::getList(['where'=>"question_id = {$this->id}"]);
    }
}
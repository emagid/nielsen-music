<?php

namespace Model;

class User_Roles extends \Emagid\Core\Model {
  
    static $tablename = "user_roles";
    public static $fields = ['user_id','role_id'];

}

<?php
namespace Model;

class Transaction extends \Emagid\Core\Model {
 
    static $tablename = "transaction";

    public static $fields = [
    	'authorize_net_data', //blob
    	'order_id',
    	'ref_trans_id'
    ];
  
}
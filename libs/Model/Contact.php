<?php

namespace Model;

class Contact extends \Emagid\Core\Model {
    static $tablename = "public.contact";

    public static $fields  =  [
        'first_name',
        'last_name',
        'email',
        'company',
        'message'
    ];

    function full_name() {
        return $this->first_name.' '.$this->last_name;
    }
}

























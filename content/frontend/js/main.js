$(document).ready(function(){

    // opening animations
    $('header').fadeIn(1500);
    $('.white').fadeOut(1500);





    // No Right clicks
    document.addEventListener('contextmenu', event => event.preventDefault());


    // Notification disapear
    setTimeout(function(){
        $('body #in_page_alert_wrapper_nofitication').fadeOut('slow');
    }, 2000);

    // custom valid email statement
    // $(function(){
    //     $("input.contact_form[name=email]")[0].oninvalid = function () {
    //         this.setCustomValidity("Please enter an email name@domain.");
    //         this.setCustomValidity("");
    //     };
    // });

    
    // Choice click
    var timer;
    $(".click_action").on({
         'click': function clickAction() {
             var self = this;
                $(self).css('transform', 'scale(.9)');
              timer = setTimeout(function () {
                  $(self).css('transform', 'scale(1)');
              }, 100);
         }
    });

    // Image alignmnt
    $('.answer').on('click', function(){
        $(this).children('img').css('right', '0');
    });

    $('.answer_outcome').on('click', function(){
        $(this).children('img').css('right', '0');
    });

    $(document).on('click', '.trivia_content .question_holder:nth-of-type(6) .answer', function(){
        $(this).children('img').css('right', '23px');
    });

    $(document).on('click', '.trivia_content .question_holder:nth-of-type(6) .answer_outcome', function(){
        $(this).children('img').css('right', '23px');
    });

    // Trivia click delay
    $('#trivia').on('click', function(e){
        var link = $(this).attr("href");
        e.preventDefault()
        setTimeout(function(){
            $('.home').css('padding-top', '200px');
            $('.home').css('opacity', '0');
            $('.home').delay(500).fadeOut(); 
            $('.white').fadeIn();
        }, 500);
        setTimeout(function(){
            window.location.href = link;
        }, 1000);
    });

    // stadium click delay
    $('#stadium').on('click', function(e){
        var link = $(this).attr("href");
        e.preventDefault()
        setTimeout(function(){
            $('.home').css('padding-top', '200px');
            $('.home').css('opacity', '0');
            $('.home').delay(500).fadeOut(); 
            $('.white').fadeIn();
        }, 500);
        setTimeout(function(){
            window.location.href = link;
        }, 1000);
    });

    // Contact click delay
     $('#contact').on('click', function(e){
        var link = $(this).attr("href");
        e.preventDefault()
        setTimeout(function(){
            $('.home').css('padding-top', '200px');
            $('.home').css('opacity', '0');
            $('.home').delay(500).fadeOut(); 
            $('.white').fadeIn();
        }, 500);
        setTimeout(function(){
            window.location.href = link;
        }, 1000);
    });

    // Info click delay
     $('#info').on('click', function(e){
        var link = $(this).attr("href");
        e.preventDefault()
        setTimeout(function(){
            $('.home').css('padding-top', '200px');
            $('.home').css('opacity', '0');
            $('.home').delay(500).fadeOut(); 
            $('.white').fadeIn();
        }, 500);
        setTimeout(function(){
            window.location.href = link;
        }, 1000);
    });

    // Home click delay
     $('.back_home').on('click', function(e){
        var link = $(this).attr("href");
        e.preventDefault()
        setTimeout(function(){
            $('.white').fadeIn();
        }, 500)
        setTimeout(function(){
            window.location.href = link;
        }, 1000);
    });


    // Page starts
    $('.home').css('opacity', '1');
    $('.trivia_content').fadeIn('fast');
    $('.trivia_content').css('padding-top', '0px');
    $('.panoramic_holder').fadeIn('fast');
    $('.panoramic_holder').css('padding-top', '0px');
    $('.contact').fadeIn('slow');
    $('.contact').css('padding-top', '0px');
    $('.contact_form').fadeIn('slow');
    $('.contact_form').css('padding-top', '0px');
    $('.future_of_sport').fadeIn('slow');


    // Answer click
    $(document).on('click', '.answer', function(){
        $('.answer').css('background-color', 'rgba(255, 255, 255, .80)');
        $(this).css('background-color', 'white');
        // $('.answer').removeClass('chosen');
        // $(this).addClass('chosen');
    });


    // Home animation
    $('.back_home').click(function(){
        $('.back_home img').css('margin-left', '-58px');
    });



    // Rugby Click
    function rugbyClick(){
        $('#soccer').addClass('no_click');
        $('.panoramic').css('background-size', '550%');
        $('.panoramic').css('background-position', 'right -673px bottom');
        setTimeout(function(){
            $('.panoramic_holder').fadeOut('slow');
            $('.click_points').fadeOut('slow');
        }, 1000);
        setTimeout(function(){
            $('.soccer').css('display', 'none');
            $('.rugby').fadeIn('slow');
            $('.rugby').addClass('active');
            $('.soccer').removeClass('active');
            $('.locator').css('transform','scale(3)');
            setTimeout(function(){
                $('.locator').css('transform','scale(1)');
            }, 1000);
        }, 2000);
        setTimeout(function(){
            $('.click_points').fadeIn('slow');
        }, 3500);
    }

    $('#rugby').click(rugbyClick);

    // Soccer Click
    function soccerClick(){
        $('#rugby').addClass('no_click');
        $('.panoramic').css('background-size', '550%');
        $('.panoramic').css('background-position', '-1012px bottom');
        setTimeout(function(){
            $('.panoramic_holder').fadeOut('slow');
            $('.click_points').fadeOut('slow');
        }, 1000);
        setTimeout(function(){
            $('.rugby').css('display', 'none');
            $('.soccer').fadeIn('slow');
            $('.soccer').addClass('active');
            $('.rugby').removeClass('active');
            $('.locator').css('transform','scale(3)');
            setTimeout(function(){
                $('.locator').css('transform','scale(1)');
            }, 1000);
        }, 2000);
        setTimeout(function(){
            $('.click_points').fadeIn('slow');
        }, 3500);
    }

    $('#soccer').click(soccerClick);


    // Back Click
    $('.back_sports').click(function(){
        $('.panoramic').css('background-size', '300%');
        $('.panoramic').css('background-position', 'center'); 
        $('#soccer').removeClass('no_click');  
        $('#rugby').removeClass('no_click');       
        $('.active').fadeOut('slow');
        setTimeout(function(){
            $('.panoramic_holder').fadeIn('slow');
        }, 2000);
    });


    // Scroll indicator
    function loop() {
        $('#scroll').animate({'bottom': '370'}, {
            duration: 1000, 
            complete: function() {
                $('#scroll').animate({bottom: 337}, {
                    duration: 1000, 
                    complete: loop});
            }});        
    }

    loop()


    
    // Arrow fade on info page
    $(window).on('scroll', function() {
       if ( $(window).scrollTop() < 300 ) {
            $('#scroll').fadeIn();
       }else {
            $('#scroll').fadeOut();
       }
   });


    $('#scroll').on('click', function(){
        $('html, body').animate({
                scrollTop: $(".icon_section").offset().top
        }, 2000);
    });


     // Go home after trivia completion
     $('.button.submit').on('click', function(){
        setTimeout(function(){
            if ( $('.trivia_completion').css('display') == 'block' ) {
                $('.trivia_completion').css('padding-top', '200px').fadeOut();
                $('.white').fadeIn();
                setTimeout(function(){
                    window.location.replace("/");
                }, 500);
             }
         }, 5000);
     });


     // initial timeout redirect homepage
        var initial = null;

        function invoke() {
            initial = window.setTimeout(
                function() {
                    $('.timeout1').fadeIn();
                }, 60000);
            initial2 = window.setTimeout(
                function() {
                    $('.timeout1').fadeIn();
                }, 600000);
        }


        invoke();
        // invoke2();

        $('body').on('click mousemove', function(){
            window.clearTimeout(initial);
            window.clearTimeout(initial2);
            invoke();
            // invoke2();
        })
         



    // Keyboard input click
    $('.jQKeyboard').on('click', function(){
        $('.jQKeyboard').removeClass('focus');
        $(this).addClass('focus');
    });

    

});



